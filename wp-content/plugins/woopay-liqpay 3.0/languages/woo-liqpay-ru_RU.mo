��            )         �  P   �               :  D   Z     �     �     �     �     �     �  	   	       Y   "     |     �  O   �     �     �                #     7  &   N     u  N   �      �  v   �  �   h     �  �  �  �   �  )   �	  )   �	  F   �	  �   &
  0   �
  3   �
          /  -   G  #   u     �     �  �   �  &   f     �  �   �  "     "   =     `     |     �     �  3   �       �      9   �  �   �  �   �     �                                                                                      	                     
                             Add custom return URI. Customer will be returned there after payment completed.) Add your LiqPay public key. Add your LiqPay secret key. Cancel order &amp; restore cart Choose the language of payment gateway page where user will be send. Custom order description Customer return URI Description Enable LiqPay Enable sandbox mode Enable/Disable Error: %s Failed payment In sandbox mode all the payments will have status "sandbox". Useful for testing purposes. Language LiqPay LiqPay payment gateway redirects user to http://liqpay.com for making payments. LiqPay public key LiqPay secret key Paid with LiqPay.com Pay for order Pay with LiqPay.com Payment for order # %s Payment is waiting for secure reasons. Refunded %s Sorry, this payment gateway doesn't support the currency of your online store. Test mode: paid with LiqPay.com! Text in order description field (Standart is: "Payment for order # $iOrderId" (the variable $iOrderId is an order ID)) Thank you - your order is now pending payment. You can press "Pay" button and then you will be redirected on LiqPay to make payment. Title Project-Id-Version: Woo Liqpay
POT-Creation-Date: 2014-09-16 11:06+0200
PO-Revision-Date: 2017-11-21 16:20+0200
Last-Translator: Vitaliy Kiyko <mr.psiho@gmail.com>
Language-Team: MooMoo Web Studio <sales@moomoo.com.ua>
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.2
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Добавить кастомный URI возврата. Покупатель будет возвращен туда после завершения оплаты. Ваш публичный ключ Liqpay Ваш секретный ключ Liqpay Отменить заказ и восстановить корзину Укажите язык страницы оплаты платежного шлюза куда покупатель будет направлен. Кастомное описание заказа URL возвращения пользователя Описание Включить LiqPay Включить тестовый режим? Включить/выключить Ошибка: %s Неудачная оплата В тестовом режиме все платежи будут иметь статус "sandbox". Полезно с целью тестирования. Язык страницы оплаты LiqPay Шлюз оплаты LiqPay направляет покупателя к http://liqpay.com для совершения оплаты. Публичный ключ Liqpay Секретный ключ Liqpay Оплачено LiqPay.com Оплатить заказ Оплатить LiqPay.com Оплата заказа # %s Идет процес проверки оплаты Возвращено %s  Извините, этот платежный шлюз не поддерживает валюту вашего магазина. Тестовый режим: оплачено LiqPay.com! Текст в поле описания заказа (Стандартный: "Оплата заказа # $iOrderId" (переменная $iOrderId это номер заказа)) Спасибо, теперь ваш заказ ожидает оплаты. Вы можете нажать кнопку "Оплатить" и далее будете перенаправлены на страницу оплаты LiqPay. Заголовок 