��            )         �  P   �               :  D   Z     �     �     �     �     �     �  	   	       Y   "     |     �  O   �     �     �                #     7  &   N     u  N   �      �  v   �  �   h     �  �  �  �   �  )   �	  )   �	  6   �	  V   
  0   c
  '   �
     �
     �
  /   �
  #        3     F  �   b  &   �          %  "   �  "   �     �  %        -  &   L  -   s     �  �   �  9   <  �   v  $  5     Z                                                                                      	                     
                             Add custom return URI. Customer will be returned there after payment completed.) Add your LiqPay public key. Add your LiqPay secret key. Cancel order &amp; restore cart Choose the language of payment gateway page where user will be send. Custom order description Customer return URI Description Enable LiqPay Enable sandbox mode Enable/Disable Error: %s Failed payment In sandbox mode all the payments will have status "sandbox". Useful for testing purposes. Language LiqPay LiqPay payment gateway redirects user to http://liqpay.com for making payments. LiqPay public key LiqPay secret key Paid with LiqPay.com Pay for order Pay with LiqPay.com Payment for order # %s Payment is waiting for secure reasons. Refunded %s Sorry, this payment gateway doesn't support the currency of your online store. Test mode: paid with LiqPay.com! Text in order description field (Standart is: "Payment for order # $iOrderId" (the variable $iOrderId is an order ID)) Thank you - your order is now pending payment. You can press "Pay" button and then you will be redirected on LiqPay to make payment. Title Project-Id-Version: Woo Liqpay
POT-Creation-Date: 2014-09-16 11:13+0200
PO-Revision-Date: 2014-09-16 11:13+0200
Last-Translator: Vitaliy Kiyko <mr.psiho@gmail.com>
Language-Team: MooMoo Web Studio <sales@moomoo.com.ua>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.9
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Додайте власний URI повернення. Покупця буде повернуто туди після завершення оплати. Ваш публічний ключ Liqpay Ваш секретний ключ Liqpay Скасувати &amp; відновити кошик Вкажіть мову сторінки оплати платіжного шлюза. Кастомний опис замовлення URL повернення покупця Опис Увімкнути LiqPay Увімкнути тестовий режим? Увімкнути/Вимкнути Помилка: %s Невдала оплата В тестовому режимі всі платежі матимуть статус "sandbox". Корисно в цілях тестування. Мова сторінки оплати LiqPay Шлюз оплати LiqPay направляє покупця до http://liqpay.com для здійснення оплати. Публічний ключ Liqpay Секретний ключ Liqpay Оплачено LiqPay.com Оплатити замовлення Оплатити з LiqPay.com Оплата замовлення # %s Платіж очікує перевірки. Повернуто %s  Вибачте, цей платіжний шлюз не підтримує валюту вашого онлайн магазину. Тестовый режим: оплачено LiqPay.com! Текст в полі опису замовлення (Стандартний: "Оплата замовлення # $iOrderId" (змінна $iOrderId це номер замовлення)) Дякуємо, тепер ваше замовлення очікує оплати. Ви можете натиснути кнопку "Оплатити" і далі будете перенаправлені на сторінку оплати LiqPay для здійснення платежу. Заголовок 