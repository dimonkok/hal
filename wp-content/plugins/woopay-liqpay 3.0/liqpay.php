<?php

/*
Plugin Name: Woocommerce - LiqPay 3.0 Payment Gateway
Plugin URI: http://google.com.ua
Description: Payment gateway LiqPay 3.0 
Author: NoNaMe
Version: 3.0
Author URI: http://google.com.ua
*/

if ( ! defined( 'WOO_LIQPAY_DOMAIN' ) )
    define( 'WOO_LIQPAY_DOMAIN', 'woo-liqpay' );

if( !load_plugin_textdomain( WOO_LIQPAY_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' ) )
    load_plugin_textdomain( WOO_LIQPAY_DOMAIN, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

add_action('plugins_loaded', 'LiqpayWoocommerceGateway', 0 );

function LiqpayWoocommerceGateway() {

if (!class_exists('WC_Payment_Gateway')) return;

    class WC_Gateway_Liqpay extends WC_Payment_Gateway {

	var $notify_url;

    /**
     * Constructor for the gateway.
     *
     * @access public
     * @return void
     */
	public function __construct() {
		global $woocommerce;

        $this->id               = 'liqpay';
        $this->icon             = plugins_url() . '/' . dirname( plugin_basename( __FILE__ ) ) . '/images/liqpay-icon.png';
        $this->has_fields       = false;
        $this->method_title     = __( 'LiqPay', WOO_IK_DOMAIN );
        $this->notify_url       = WC()->api_request_url( 'WC_Gateway_Liqpay' );
		$this->supports 		= array(
			'products',
			//'refunds'
		);

		// Load the settings.
		$this->init_form_fields();
		$this->init_settings();

		// Define user set variables
		$this->title 			= $this->get_option( 'title' );
		$this->description 		= $this->get_option( 'description' );
		$this->public_key		= $this->get_option( 'public_key' );
        $this->secret_key	    = $this->get_option( 'secret_key' );
        $this->language	        = $this->get_option( 'language' );
        $this->order_desc	    = $this->get_option( 'order_desc' );
        $this->return_url	    = $this->get_option( 'return_url' );
        $this->test	            = $this->get_option( 'test' );

		// Logs
		if ( 'yes' == $this->debug ) {
			$this->log = new WC_Logger();
		}

		// Actions
        add_action( 'woocommerce_receipt_liqpay', array($this, 'receipt_page'));
        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

	    // Payment listener/API hook
        add_action( 'woocommerce_api_wc_gateway_liqpay', array( $this, 'check_liqpay_response' ) );

    }

	/**
	 * Admin Panel Options
	 */
	public function admin_options() {

		?>
		<h3><?php _e( 'LiqPay', WOO_LIQPAY_DOMAIN ); ?></h3>
		<p><?php _e( 'LiqPay payment gateway redirects user to http://liqpay.com for making payments.', WOO_LIQPAY_DOMAIN ); ?></p>

        <?php if ( $this->is_valid_for_use() ) { ?>

			<table class="form-table">
			<?php
    			// Generate the HTML For the settings form.
    			$this->generate_settings_html();
			?>
			</table><!--/.form-table-->

        <?php } else { ?>
            <p><?php _e('Sorry, this payment gateway doesn\'t support the currency of your online store.', WOO_LIQPAY_DOMAIN); ?></p>
        <?php } ?>

		<?php
	}

    public function is_valid_for_use() {

            if (!in_array(get_option('woocommerce_currency'), array('USD', 'EUR', 'RUB', 'UAH', 'GEL'))) {
                return false;
            }

            return true;
    }


    /**
     * Initialise Gateway Settings Form Fields
     *
     */
    function init_form_fields() {

    	$this->form_fields = array(
			'enabled' => array(
							'title' => __( 'Enable/Disable', 'woocommerce' ),
							'type' => 'checkbox',
							'label' => __( 'Enable LiqPay', WOO_LIQPAY_DOMAIN ),
							'default' => 'yes'
						),
			'title' => array(
							'title' => __( 'Title', 'woocommerce' ),
							'type' => 'text',
							'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
							'default' => __( 'LiqPay', WOO_LIQPAY_DOMAIN ),
							'desc_tip'      => true,
						),
			'description' => array(
							'title' => __( 'Description', 'woocommerce' ),
							'type' => 'textarea',
							'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce' ),
							'default' => __( 'Pay with LiqPay.com', WOO_LIQPAY_DOMAIN )
						),
			'public_key' => array(
							'title' => __( 'LiqPay public key', WOO_LIQPAY_DOMAIN ),
							'type' 			=> 'text',
							'description' => __( 'Add your LiqPay public key.', WOO_LIQPAY_DOMAIN ),
							'default' => '',
							'desc_tip'      => true,
							'placeholder'	=> ''
						),
			'secret_key' => array(
							'title' => __( 'LiqPay secret key', WOO_LIQPAY_DOMAIN ),
							'type' 			=> 'text',
							'description' => __( 'Add your LiqPay secret key.', WOO_LIQPAY_DOMAIN ),
							'default' => '',
							'desc_tip'      => true,
							'placeholder'	=> ''
						),
			'language' => array(
							'title' => __( 'Language', WOO_LIQPAY_DOMAIN ),
							'type' 			=> 'select',
							'description' => __( 'Choose the language of payment gateway page where user will be send.', WOO_LIQPAY_DOMAIN ),
							'default' => 'en',
                            'options' => array(
                                    'en' => 'EN',
                                    'ru' => 'RU'
                                    )
						),
			'order_desc' => array(
							'title' => __( 'Custom order description', WOO_LIQPAY_DOMAIN ),
							'type' 			=> 'text',
							'description' => __( 'Text in order description field (Standart is: "Payment for order # $iOrderId" (the variable $iOrderId is an order ID))', WOO_LIQPAY_DOMAIN ),
							'default' => 'Payment for order # %s',
							'desc_tip'      => true,
							'placeholder'	=> ''
						),
			'return_url' => array(
							'title' => __( 'Customer return URI', WOO_LIQPAY_DOMAIN ),
							'type' 			=> 'text',
							'description' => __( 'Add custom return URI. Customer will be returned there after payment completed.)', WOO_LIQPAY_DOMAIN ),
							'default' => '',
							'desc_tip'      => true,
							'placeholder'	=> ''
						),
			'test' => array(
							'title' => __( 'Enable sandbox mode', WOO_LIQPAY_DOMAIN ),
							'type' => 'checkbox',
							'label' => __( 'In sandbox mode all the payments will have status "sandbox". Useful for testing purposes.', WOO_LIQPAY_DOMAIN ),
							'default' => ''
						)
			);

    }

		/**
		 * Generate form
		 **/
		public function generate_liqpay_form( $order_id ) {
            global $woocommerce;

			$order = new WC_Order( $order_id );

			$sPublicKey         = $this->public_key;
			$sSecretKey         = $this->secret_key;
            $sLanguage          = $this->language;
			$sCustomOrderDesc   = $this->order_desc;
            $sReturnUrl         = $this->return_url;
            $sTestMode          = $this->test;

            $sAmount                    = $order->order_total;
            $sCurrency                  = get_option('woocommerce_currency');
            if ( $sCustomOrderDesc ) {
                $sCustomOrderDesc	        = str_replace('$iOrderId', $order_id, $sCustomOrderDesc);
                $sDescription           = $sCustomOrderDesc;
            } else {
                $sDescription           = sprintf( __( 'Payment for order # %s', WOO_LIQPAY_DOMAIN ), $order_id );
            }
            if ( $sTestMode == 'yes' ) {
                $sMode = '1';
            } else {
                $sMode = '';
            }
            $sOrderID                   = $order_id.'_'.time();
            // save unique order id that is created by plugin; may be used if refund is initialized
            add_post_meta( $order->id, '_uni_gateway_order_id', $sOrderID, true );
            $sServerUrl                 = $this->notify_url;
            $sSignature 	            = base64_encode( sha1($sSecretKey.$sAmount.$sCurrency.$sPublicKey.$sOrderID.'buy'
                                                        .$sDescription.$sReturnUrl.$sServerUrl, 1 ));

		    $sOutput .= '
                    <form class="uni_liqpay_payment_form" method="POST" action="https://www.liqpay.com/api/pay" accept-charset="utf-8">
                        <input type="hidden" name="public_key" value="'.$sPublicKey.'" />
                        <input type="hidden" name="amount" value="'.$sAmount.'" />
                        <input type="hidden" name="currency" value="'.$sCurrency.'" />
                        <input type="hidden" name="description" value="'.$sDescription.'" />
                        <input type="hidden" name="order_id" value="'.$sOrderID.'" />
                        <input type="hidden" name="result_url" value="'.$sReturnUrl.'" />
                        <input type="hidden" name="server_url" value="'.$sServerUrl.'" />
                        <input type="hidden" name="type" value="buy" />
                        <input type="hidden" name="signature" value="'.$sSignature.'" />
                        <input type="hidden" name="language" value="'.$sLanguage.'" />
                        <input type="hidden" name="sandbox" value="'.$sMode.'" />
                        <input type="submit" class="button alt el-btn mod-grad mod-arr" id="uni_submit_liqpay_payment_form" value="'.__('Pay for order', 'woocommerce').'" /></br> <a class="button cancel" href="'.esc_url($order->get_cancel_order_url()).'">'.__('Cancel order &amp; restore cart', 'woocommerce').'</a>
                    </form>';

            return $sOutput;

		}

		/**
		 * Process the payment and return the result
		 **/
		function process_payment( $order_id ) {
            global $woocommerce;

			$order = new WC_Order( $order_id );

			return array(
				'result' 	=> 'success',
                'redirect'	=> add_query_arg('order', $order->id, add_query_arg('key', $order->order_key, get_permalink(woocommerce_get_page_id('pay'))))
			);

		}

		/**
		 * receipt_page
		 **/
		function receipt_page( $order ) {
            echo '<div class="liqpay_modal remodal">';
            echo '<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>';
            echo '<p class="uni_liqpay_receipt_paragraph">' . __( 'Thank you - your order is now pending payment. You can press "Pay" button and then you will be redirected on LiqPay to make payment.', WOO_LIQPAY_DOMAIN ) . '</p>';

			echo $this->generate_liqpay_form( $order );
            echo '</div>';
		}

	    /**
	    * Process a refund if supported
	    * @param  int $order_id
	    * @param  float $amount
	    * @param  string $reason
	    * @return  bool|wp_error True or false based on success, or a WP_Error object
	    */
        public function process_refund( $order_id, $amount = null, $reason = '' ) {

			$sPublicKey         = $this->public_key;
			$sSecretKey         = $this->secret_key;
            $order = wc_get_order( $order_id );

		    if ( ! $order || ! $order->get_transaction_id() || ! $this->public_key || ! $this->secret_key ) {
			    return false;
		    }

            $sOrderID = get_post_meta( $order_id, '_uni_gateway_order_id', true );

		    $aPostData = array(
			    'order_id'      => $sOrderID,
			    'server_url'    => $this->notify_url,
                'public_key'    => $sPublicKey
		    );

            // Nope, sorry, LiqPay 2.0 API doesn't support partial refund... maybe next time ;)
            /*
		    if ( ! is_null( $amount ) ) {
			    $aPostData['amount']            = number_format( $amount, 2, '.', '' );
			    $post_data['currency']          = $order->get_order_currency();
		    } */

            $sPostData = json_encode($aPostData);
            $sSignature = base64_encode(sha1($sSecretKey.$sPostData.$sSecretKey, 1));

		    $response = wp_remote_post( 'https://www.liqpay.com/api/payment/refund', array(
			        'method'      => 'POST',
			        'body'        => array( 'data' => $sPostData, 'signature' => $sSignature ),
			        'timeout'     => 70,
			        'sslverify'   => false,
			        'user-agent'  => 'WooCommerce',
			        'httpversion' => '1.1'
			    )
		    );

		    if ( is_wp_error( $response ) ) {
			    return $response;
		    }

		    if ( empty( $response['body'] ) ) {
			    return new WP_Error( 'liqpay-error', __( 'Empty Liqpay response.', WOO_LIQPAY_DOMAIN ) );
		    }

            $parsed_response = json_decode($response['body'], true);
            //$order->add_order_note( sprintf( __( '$parsed_response: %s', WOO_LIQPAY_DOMAIN ), print_r($parsed_response, 1)  ) );
		    switch ( strtolower( $parsed_response['result'] ) ) {
			    case 'ok':
				    $order->add_order_note( sprintf( __( 'Refunded %s', WOO_LIQPAY_DOMAIN ), $order_id ) );
				    return true;
                    break;
                case 'error':
				    $order->add_order_note( sprintf( __( 'Error: %s', WOO_LIQPAY_DOMAIN ), $parsed_response['description'] ) );
				    return false;
			        break;
		    }

		    return false;

        }

		/**
		 * check_liqpay_response
		 **/
	    public function check_liqpay_response() {

            global $woocommerce;

			    $sPublicKey         = $this->public_key;
			    $sSecretKey         = $this->secret_key;
                $sLanguage          = $this->language;
			    $sCustomOrderDesc   = $this->order_desc;
                $sReturnUrl         = $this->return_url;
                $sTestMode          = $this->test;

                $sPublicKey     = $_POST['public_key'];
                $sAmount        = $_POST['amount'];
                $sCurrency      = $_POST['currency'];
                $sDecription    = $_POST['description'];
                $sType          = $_POST['type'];
                $sOrderIdRaw    = $_POST['order_id'];
                $sStatus        = $_POST['status'];
                $sTransactionId = $_POST['transaction_id'];
                $sSenderPhone   = $_POST['sender_phone'];
                $sSignature     = $_POST['signature'];

                $sMySignature 	= base64_encode( sha1($sSecretKey.$sAmount.$sCurrency.
                                $sPublicKey.$sOrderIdRaw.$sType.$sDecription.$sStatus.$sTransactionId.$sSenderPhone, 1 ));

                $aOrderId       = $pieces = explode('_', $sOrderIdRaw);
                $sOrderId       = $aOrderId[0];

				if ( $sStatus == 'success' && $sSignature == $sMySignature ) {

					$order = new WC_Order( $sOrderId );

					if ( $order->status !== 'completed' ) {
                        if ( version_compare( WOOCOMMERCE_VERSION, "2.2" ) >= 0 ) {
                            if ( $sTransactionId ) $order->payment_complete( $sTransactionId );
                        } else {
						    $order->update_status('completed', __( 'Paid with LiqPay.com', WOO_LIQPAY_DOMAIN ));
                        }
					}

				} elseif ( $sStatus == 'failure' && $sSignature == $sMySignature ) {

					$order = new WC_Order( $sOrderId );

					if ( $order->status !== 'completed' ) {
						$order->update_status('failed', __( 'Failed payment', WOO_LIQPAY_DOMAIN ));
					}

				} elseif ( $sStatus == 'wait_secure' && $sSignature == $sMySignature ) {

					$order = new WC_Order( $sOrderId );

					if ( $order->status !== 'completed' ) {
						$order->update_status('on-hold', __( 'Payment is waiting for secure reasons.', WOO_LIQPAY_DOMAIN ));
					}

				} elseif ( $sStatus == 'sandbox' && $sSignature == $sMySignature ) {

					$order = new WC_Order( $sOrderId );

					if ( $order->status !== 'completed' ) {
                        if ( version_compare( WOOCOMMERCE_VERSION, "2.2" ) >= 0 ) {
                            if ( $sTransactionId ) $order->payment_complete( $sTransactionId );
                        } else {
						    $order->update_status('completed', __( 'Test mode: paid with LiqPay.com!', WOO_LIQPAY_DOMAIN ));
                        }
					}

                }

	    }

    }

	/**
    * Add the Gateway to WooCommerce
    **/
    function woocommerce_add_gateway_liqpay_gateway($methods) {
        $methods[] = 'WC_Gateway_Liqpay';
        return $methods;
    }
    add_filter('woocommerce_payment_gateways', 'woocommerce_add_gateway_liqpay_gateway' );

}

    /**
	*   plugin acivation hook
	**/
    function woo_liqpay_activation() {
    }

    register_activation_hook( __FILE__, 'woo_liqpay_activation');
