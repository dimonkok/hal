=== Custom Payment Gateways for WooCommerce ===
Contributors: algoritmika,anbinder
Tags: woocommerce,custom payment gateways,custom payment gateway,payment gateways,payment gateway
Requires at least: 4.4
Tested up to: 4.8
Stable tag: 1.1.0
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Custom payment gateways for WooCommerce.

== Description ==

**Custom Payment Gateways for WooCommerce** plugin lets you add custom payment gateways to WooCommerce.

= Feedback =
* We are open to your suggestions and feedback. Thank you for using or trying out one of our plugins!

== Installation ==

1. Upload the entire 'custom-payment-gateways-for-woocommerce' folder to the '/wp-content/plugins/' directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Start by visiting plugin settings at WooCommerce > Settings > Custom Payment Gateways.

== Changelog ==

= 1.1.0 - 26/07/2017 =
* Dev - WooCommerce v3 compatibility - `reduce_order_stock()` replaced with `wc_reduce_stock_levels()`.
* Dev - WooCommerce v3 compatibility - Order status, payment method and shipping method - Getting with functions instead of accessing properties directly.
* Dev - Autoloading plugin options.
* Dev - Link updated from http://coder.fm to https://wpcodefactory.com.
* Dev - Plugin header ("Text Domain" etc.) updated.
* Dev - POT file added.

= 1.0.0 - 17/02/2017 =
* Initial Release.

== Upgrade Notice ==

= 1.0.0 =
This is the first release of the plugin.
