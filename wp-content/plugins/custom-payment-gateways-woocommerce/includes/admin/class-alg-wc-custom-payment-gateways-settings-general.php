<?php
/**
 * Custom Payment Gateways for WooCommerce - General Section Settings
 *
 * @version 1.1.0
 * @since   1.0.0
 * @author  Algoritmika Ltd.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Alg_WC_Custom_Payment_Gateways_Settings_General' ) ) :

class Alg_WC_Custom_Payment_Gateways_Settings_General extends Alg_WC_Custom_Payment_Gateways_Settings_Section {

	/**
	 * Constructor.
	 *
	 * @version 1.0.0
	 * @since   1.0.0
	 */
	function __construct() {
		$this->id   = '';
		$this->desc = __( 'General', 'custom-payment-gateways-for-woocommerce' );
		parent::__construct();
	}

	/**
	 * get_section_settings.
	 *
	 * @version 1.1.0
	 * @since   1.0.0
	 * @todo    (maybe) alg_wc_custom_payment_gateways_custom_number for alg_wc_custom_payment_gateways_number
	 */
	function get_section_settings() {
		$wocommerce_checkout_settings = '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout' ) . '">' .
			__( 'WooCommerce > Settings > Checkout', 'custom-payment-gateways-for-woocommerce' ) . '</a>';
		$settings = array(
			array(
				'title'    => __( 'Custom Payment Gateways Options', 'custom-payment-gateways-for-woocommerce' ),
				'type'     => 'title',
				'id'       => 'alg_wc_custom_payment_gateways_options',
				'desc'     => __( 'Here you can set number of custom payment gateways to add.', 'custom-payment-gateways-for-woocommerce' )
					. ' ' . sprintf( __( 'After setting the number, visit %s to set each gateway options.', 'custom-payment-gateways-for-woocommerce' ), $wocommerce_checkout_settings ),
			),
			array(
				'title'    => __( 'Custom Payment Gateways', 'custom-payment-gateways-for-woocommerce' ),
				'desc'     => '<strong>' . __( 'Enable', 'custom-payment-gateways-for-woocommerce' ) . '</strong>',
				'desc_tip' => __( 'WooCommerce Custom Payment Gateways.', 'custom-payment-gateways-for-woocommerce' ),
				'id'       => 'alg_wc_custom_payment_gateways_enabled',
				'default'  => 'yes',
				'type'     => 'checkbox',
			),
			array(
				'title'    => __( 'Number of Gateways', 'custom-payment-gateways-for-woocommerce' ),
				'desc'     => apply_filters( 'alg_wc_custom_payment_gateways', sprintf( __( 'You will need <a href="%s">Custom Payment Gateways for WooCommerce Pro plugin</a> to add more than one custom payment gateway.', 'custom-payment-gateways-for-woocommerce' ), 'https://wpcodefactory.com/item/custom-payment-gateways-woocommerce/' ), 'settings' ),
				'desc_tip' => __( 'Number of custom payments gateways to be added.', 'custom-payment-gateways-for-woocommerce' ) . ' ' .
					__( 'Press Save changes after changing this number.', 'custom-payment-gateways-for-woocommerce' ),
				'id'       => 'alg_wc_custom_payment_gateways_number',
				'default'  => 1,
				'type'     => 'number',
				'custom_attributes' => apply_filters( 'alg_wc_custom_payment_gateways', array( 'readonly' => 'readonly' ), 'settings_array' ),
			),
		);
		for ( $i = 1; $i <= apply_filters( 'alg_wc_custom_payment_gateways', 1, 'value_total_gateways' ); $i++ ) {
			$settings[] = array(
				'title'    => __( 'Admin Title for Custom Gateway', 'custom-payment-gateways-for-woocommerce' ) . ' #' . $i,
				'id'       => 'alg_wc_custom_payment_gateways_admin_title_' . $i,
				'default'  => __( 'Custom Gateway', 'custom-payment-gateways-for-woocommerce' ) . ' #' . $i,
				'type'     => 'text',
			);
		}
		$settings = array_merge( $settings, array(
			array(
				'type'     => 'sectionend',
				'id'       => 'alg_wc_custom_payment_gateways_options',
			),
		) );
		return $settings;
	}

}

endif;

return new Alg_WC_Custom_Payment_Gateways_Settings_General();
