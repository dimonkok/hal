<?php
/**
 * Content wrappers
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/wrapper-end.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$template = get_option( 'template' );

switch ( $template ) {
	case 'twentyeleven' :
		echo '</div>';
		get_sidebar( 'shop' );
		echo '</div>';
		break;
	case 'twentytwelve' :
		echo '</div></div>';
		break;
	case 'twentythirteen' :
		echo '</div></div>';
		break;
	case 'twentyfourteen' :
		echo '</div></div></div>';
		get_sidebar( 'content' );
		break;
	case 'twentyfifteen' :
		echo '</div></div>';
		break;
	case 'twentysixteen' :
		echo '</main></div>';
		break;
}

?>
<?php if(is_product()) : ?>
	</div>
	</div>
    <?php endif; ?>
<?php if (is_shop() || is_product_category()) : ?>
</section><div class="el-banner banner-section">
<?php //if (ICL_LANGUAGE_CODE == "ua") {
//        $ids_need = 152;
//    } else {
//        $ids_need = 44;
//    } ?>
 
</div></div>
<section class="cat-articles mod-mtop js-bg-cover" data-img="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/gbg2.jpg">
	<div class="container cat-articles-inner">
        <?php if (ICL_LANGUAGE_CODE == "ua") {
            $cats = 31;
        } else {
            $cats = 30;
        }
       $args = array(
           'cat' => $cats,
           'order' => 'DESC',
           'posts_per_page' => 4
       );
       $query = new WP_Query( $args );
       while ($query->have_posts()) : $query->the_post();  // launching a series of blog bypass materials
        ?>
		<!-- item -->
		<div class="cat-article col-xs-12 col-sm-6 col-md-3">
			<div class="cat-article-inner">
				<a href="<?php echo get_permalink(); ?>" class="js-bg-cover cat-article-pic" data-img="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"></a>
				<a href="<?php echo get_permalink(); ?>" class="cat-article-title">
					<?php the_title(); ?>
				</a>
				<div class="cat-article-date">
                    <?php echo get_the_date('d F Y'); ?>
				</div>
			</div>
		</div>
				<!-- end item -->
        <?php
    endwhile; // Complete the cycle.
wp_reset_query();
?>
	</div>
	<div class="text-center">
        <?php if (ICL_LANGUAGE_CODE == "ua") {
            $pereiti = 'Читати Ще';
            $linck_news = get_home_url().'/novini/';
        } else {
            $pereiti = 'Читать Еще';
            $linck_news = get_home_url().'/novosti/';
        } ?>
		<a href="<?php echo $linck_news;?>" class="el-btn mod-nobg mod-arr">
			<?php echo $pereiti; ?>
		</a>
	</div>
</section>
<section class="catalog-txt">
	<div class="container">
		<div class="article-heading">
			<?php $value = get_field( "texsettt", 'option' );
			echo $value; ?>
		</div>
		<div class="ctext text-center">
            <?php $value = get_field( "discript_content", 'option' );
            echo $value; ?>
		</div>
	</div>
</section>
</div>
<?php endif; ?>
