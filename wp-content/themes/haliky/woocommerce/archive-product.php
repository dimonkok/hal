<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>


<?php if(is_product_category()): ?>

    <div class="pagebanner js-bg-cover" data-img="<?php
    $current_category = get_queried_object();
     echo get_field('cat_main_baner_golovnui', 'product_cat_'.$current_category->term_id);
    ?>">
        <div class="pagebanner-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-4 col-md-offset-4">
                        <div class="pagebanner-h">
                            <?php woocommerce_page_title(); ?>
                        </div>
                        <div class="pagebanner-txt">
                            <?php echo $current_category->description; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a href="#categories-info_my" class="el-scrolldown js-scrolldown mod-blue"></a>
    <?php woocommerce_breadcrumb(); ?>
    <section class="categories-info" id="categories-info_my">
        <div class="container">
            <div class="row">
                <div class="categories-info-pic col-xs-12 col-md-4 valign">
                    <?php
                        $image_cats = get_field('cartinka_cat', 'product_cat_'.$current_category->term_id);
                        $descriptions_cats = get_field('opisanie_categorii', 'product_cat_'.$current_category->term_id);
                    ?>
                    <img src="<?php echo $image_cats; ?>" alt="">
                </div>
                <div class="categories-info-txt ctext  col-xs-12 col-md-8 valign">
                    <?php echo $descriptions_cats; ?>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <section class="itemlist">

            <?php if ( have_posts() ) : ?>

                <?php
                /**
                 * woocommerce_before_shop_loop hook.
                 *
                 * @hooked wc_print_notices - 10
                 * @hooked woocommerce_result_count - 20
                 * @hooked woocommerce_catalog_ordering - 30
                 */
                do_action( 'woocommerce_before_shop_loop' );
                ?>

                <?php woocommerce_product_loop_start(); ?>
         

                <?php woocommerce_product_subcategories(); ?>

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php
                    /**
                     * woocommerce_shop_loop hook.
                     *
                     * @hooked WC_Structured_Data::generate_product_data() - 10
                     */
                    do_action( 'woocommerce_shop_loop' );
                    ?>

                    <?php wc_get_template_part( 'content', 'product' ); ?>

                <?php endwhile; // end of the loop. ?>

                <?php woocommerce_product_loop_end(); ?>

                <?php
                /**
                 * woocommerce_after_shop_loop hook.
                 *
                 * @hooked woocommerce_pagination - 10
                 */
                do_action( 'woocommerce_after_shop_loop' );
                ?>

            <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

                <?php
                /**
                 * woocommerce_no_products_found hook.
                 *
                 * @hooked wc_no_products_found - 10
                 */
                do_action( 'woocommerce_no_products_found' );
                ?>

            <?php endif; ?>

            <?php
            /**
             * woocommerce_after_main_content hook.
             *
             * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
             */
            do_action( 'woocommerce_after_main_content' );
            ?>
        </section>
    </div>
<?php get_footer( 'shop' ); ?>
<?php else : ?>

    <div class="page-heading">

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<div class="page-heading-title"><?php woocommerce_page_title(); ?></div>

		<?php endif; ?>

		<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
		?>
        <a href="#categories" class="el-scrolldown js-scrolldown"></a>

    </div>
<section id="categories" class="categories mod-nobottom">
    <div class="catlist row">
        <?php

        $taxonomy     = 'product_cat';
        $orderby      = 'name';
        $show_count   = 0;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no
        $title        = '';
        $empty        = 0;
        $exclude      = array(43,44);

        $args = array(
            'taxonomy'     => $taxonomy,
            'orderby'      => $orderby,
            'show_count'   => $show_count,
            'pad_counts'   => $pad_counts,
            'hierarchical' => $hierarchical,
            'title_li'     => $title,
            'hide_empty'   => $empty,
            'exclude'      => $exclude,
        );
        global $wp_query, $woocommerce;
        $all_categories = get_categories( $args );
        foreach ($all_categories as $cat) :
            if($cat->category_parent == 0) {
                $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
                // get the image URL
                $image = wp_get_attachment_url( $thumbnail_id );
                // print the IMG HTML
                echo '<a href="'.esc_url(get_category_link( $cat->term_id )).'" class="catitem js-bg-cover col-xs-12 col-sm-4" data-img="'.$image.'">';
                echo '<div class="catitem-title">'.$cat->name.'</div>';
                echo '<div class="catitem-descr" style="white-space: pre;">'.$cat->description.'</div></a>';
            }
            ?>
        <?php endforeach; ?>

    </div>
</section>
<section class="itemlist">
<?php

/**
 * woocommerce_before_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>
		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked wc_print_notices - 10
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php
						/**
						 * woocommerce_shop_loop hook.
						 *
						 * @hooked WC_Structured_Data::generate_product_data() - 10
						 */
						do_action( 'woocommerce_shop_loop' );
					?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php
				/**
				 * woocommerce_no_products_found hook.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				do_action( 'woocommerce_no_products_found' );
			?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>
</section>
	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

<?php get_footer( 'shop' ); ?>
<?php endif; ?>
