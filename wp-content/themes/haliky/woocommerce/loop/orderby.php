<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="itemlist-filters">
  <ul>
      <?php if (ICL_LANGUAGE_CODE == "ua") {
          $showe = 'ПОКАЗАТИ:';
          $vid_product = 'ВИГЛЯД:';
      } else {
          $showe = 'ПОКАЗАТЬ:';
          $vid_product = 'ВИД:';
      } ?>
      <?php //var_dump($catalog_orderby_options); ?>
      <form class="woocommerce-ordering" method="get"><li><label><?php echo $showe; ?></label>
    <select name="orderby" class="orderby">
        <?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
            <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
        <?php endforeach; ?>
    </select>
    <?php wc_query_string_form_fields( null, array( 'orderby', 'submit' ) ); ?>
          </li></form>
    <?php if ( is_active_sidebar( 'left-sidebar' ) ) : ?>
        <?php if ( ! dynamic_sidebar('left-sidebar') ) : ?>
            <li>{static sidebar item 1}</li>
            <li>{static sidebar item 2}</li>
        <?php endif; ?>
    <?php endif; ?>
      <li class="mod-toggleview"><label><?php echo $vid_product; ?></label>
          <div class="itemlist-filters-view gridlist-toggle">
              <a href="#" id="grid">
                  <i class="icon-v1"></i>
              </a>
              <a href="#" id="list">
                  <i class="icon-v2"></i>
              </a>
          </div>
      </li>
    </ul>
</div>
