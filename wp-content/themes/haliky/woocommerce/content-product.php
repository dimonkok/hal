<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
    return;
}
?>
<!-- item -->
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 item">
    <?php

        $postdate = get_the_time ( 'Y-m-d' );
        $postdatestamp = strtotime ( $postdate );
        $newness = '20';

        if ((time () - (60 * 60 * 24 * $newness)) < $postdatestamp) {
            $class = 'mod-new';
        }

    ?>
    <?php if ( $product->is_on_sale() ) :
        $class = 'mod-action';
    ?>

    <?php endif; ?>

    <a href="<?php echo get_permalink($product->id); ?>" title="<?php echo $product->name; ?>">
    <div class="item-inner <?php echo $class; ?>">

    <?php


    /**
     * woocommerce_before_shop_loop_item_title hook.
     *
     * @hooked woocommerce_show_product_loop_sale_flash - 10
     * @hooked woocommerce_template_loop_product_thumbnail - 10
     */
    echo '<div class="item-pic ">';
    do_action( 'woocommerce_before_shop_loop_item_title' );
    echo '</div>';


/**
 * woocommerce_before_shop_loop_item hook.
 *
 * @hooked woocommerce_template_loop_product_link_open - 10
 */

    /**
     * woocommerce_shop_loop_item_title hook.
     *
     * @hooked woocommerce_template_loop_product_title - 10
     */
    echo '<div class="list-1">';
    do_action( 'woocommerce_before_shop_loop_item' );
    do_action( 'woocommerce_shop_loop_item_title' );
/**
 * woocommerce_after_shop_loop_item hook.
 *
 * @hooked woocommerce_template_loop_product_link_close - 5
 * @hooked woocommerce_template_loop_add_to_cart - 10
 */
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
    add_action ( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 10 );
    do_action( 'woocommerce_after_shop_loop_item' );
    //add_action ( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
    //wc_get_template( 'single-product/short-description.php' );
    //the_excerpt();
    $rating_count = $product->get_rating_count();
    $review_count = $product->get_review_count();
    $average      = $product->get_average_rating();
    $idss = $product->id;
    global $wpdb;
    $count = $wpdb->get_var("SELECT COUNT(*) FROM `rating` WHERE `product_id` =".$idss);
    $result = $wpdb->get_results('SELECT sum(rate) as result_value FROM `rating` WHERE `product_id` ='.$idss);
    $end_result = $result[0]->result_value;
    if ($count>0 ) :
        $ratess = $end_result/$count;
    endif;
    ?>
        <div class="tovar-slider-info-top">
            <div class="rate-wrap">
                <div class="rate">
                    <div class="rate_row">
                        <?php
                        for($i=1; $i <= 5; $i++) {
                            ((int) $ratess >= $i) ? $active="active" : $active="";
                            echo '<span class="rate_star '.$active.'" data-value="'.$i.'"></span>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    echo '<p>';
    do_excerpt(get_the_excerpt(), 30);
    echo '</p>';
        if (ICL_LANGUAGE_CODE == "ua") {
            $button_cart_small = 'у кошик';
            $button_cart_big = 'У КОШИК';
        } else {
            $button_cart_small = 'в корзину';
            $button_cart_big = 'В КОРЗИНУ';
        }
    ?><a rel="nofollow" href="/magazin/?add-to-cart=<?php echo $product->id; ?>" data-quantity="1" data-product_id="<?php echo $product->id; ?>" data-product_sku="" class="button product_type_simple add_to_cart_button ajax_add_to_cart item-addbasket"><span> <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/cart.png" alt=""> </span><?php echo $button_cart_small; ?></a>
        <a rel="nofollow" data-quantity="1" data-product_id="<?php echo $product->id; ?>" data-product_sku="" href="/magazin/?add-to-cart=<?php echo $product->id; ?>"  class="el-btn mod-grad tocart product_type_simple add_to_cart_button ajax_add_to_cart">
            <span>
        <i class="icon-shopping-cart"></i>
      </span><span><?php echo $button_cart_big; ?></span>
        </a>
        <?php



/**
     * woocommerce_after_shop_loop_item_title hook.
     *
     * @hooked woocommerce_template_loop_rating - 5
     * @hooked woocommerce_template_loop_price - 10
     */
    remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
    echo '<div class="item-price">';
    do_action( 'woocommerce_after_shop_loop_item_title' );
    echo '</div>';
echo '</div>';



    ?>
        <div class="list-2">

                    <div class="item-price">
                    <div class="name_chena">Цена</div>
                <?php
                //echo $product->get_price();
                echo $product->get_price_html();
               // do_action( 'woocommerce_after_shop_loop_item_title' );
                do_action( 'woocommerce_after_shop_loop_item' );
                add_action ( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
                ?>
                    </div>
                    <a href="#" class="el-like like ajax" data-id="<?php echo $product->get_id(); ?>">
                            <i class="icon-like"></i>
                    </a>
        </div>
    </a>
    </div>
</div>
<!-- end item -->
