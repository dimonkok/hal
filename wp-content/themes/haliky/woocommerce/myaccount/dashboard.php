<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $wpdb;
global $woocommerce ;
global $order;
?>
    <div class="avatar_change remodal" data-remodal-id="avatar_change">
    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
        <?php echo do_shortcode('[avatar_upload]'); ?>
    </div>
<div class="container personal-container">
    <div class="row">
        <div class="col-xs-12 col-md-4 userdata">
            <div class="userdata-inner">
                <?php $user = wp_get_current_user();
                $avtarimg =  scrapeImage(get_avatar($user->ID ));
                ?>
                <div class="userdata-inner-ava js-bg-contain" data-img="<?php echo $avtarimg ?>"></div>

                <div class="userdata-inner-hello">
                    <?php echo get_field( "привет!", 'option' ); ?><br>
                    <?php echo $user->user_firstname; ?>
                </div>
                <ul class="userdata-inner-stats">
                    <li>
                        <span><?php echo get_field( "имя", 'option' ); ?>:</span>
                        <?php echo $user->user_firstname;
                       // var_dump($user);
                        ?>
                    </li>
                    <li>
                        <span><?php echo get_field( "email", 'option' ); ?>:</span>
                        <?php echo get_user_meta( $user->ID, 'billing_email', true ); ?>
                    </li>
                    <li>
                        <span><?php echo get_field( "телефон", 'option' ); ?>:</span>
                        <?php echo get_user_meta( $user->ID, 'billing_phone', true ); ?>
                    </li>
                    <li>
                        <span><?php echo get_field( "город", 'option' ); ?>:</span>
                        <?php echo get_user_meta( $user->ID, 'billing_address_1', true ); ?>
                    </li>
                </ul>
                <a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-account' )); ?>" class="userdata-inner-edit">
                    <?php echo get_field( "редактировать_профиль", 'option' ); ?>
                </a>
            </div>
        </div>
        <div class="userdata-stats col-xs-12 col-md-8">
            <div class="userdata-stats-block">
                <div class="userdata-stats-h">
                    <?php echo get_field( "последние_заказы", 'option' ); ?>
                </div>

                <?php
                $customer_orders = get_posts( array(
                    'numberposts' => 3,
                    'meta_key'    => '_customer_user',
                    'meta_value'  => get_current_user_id(),
                    'post_type'   => wc_get_order_types(),
                    'post_status' => array_keys( wc_get_order_statuses() ),
                ) );
                $customer = wp_get_current_user();
                //var_dump($customer_orders);
                if ( count( $customer_orders ) >= 1 ) : ?>
                <!-- hist -->
                <div class="hist">
                    <div class="cart-list ">
                    <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table cart-table">
                        <tr>
                            <th>
                                <?php echo get_field( "№_заказа", 'option' ); ?>
                            </th>
                            <th>
                                <?php echo get_field( "дата", 'option' ); ?>
                            </th>
                            <th>
                                <?php echo get_field( "статус", 'option' ); ?>
                            </th>
                            <th>
                                <?php echo get_field( "сумма_таблица", 'option' ); ?>
                            </th>
                            <th>
                                <?php echo get_field( "подробнее", 'option' ); ?>
                            </th>
                        </tr>
                        <!--                        <tr>-->
<!--                            --><?php //foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
<!--                                <th class="woocommerce-orders-table__header woocommerce-orders-table__header---><?php //echo esc_attr( $column_id ); ?><!--"><span class="nobr">--><?php //echo esc_html( $column_name ); ?><!--</span></th>-->
<!--                            --><?php //endforeach; ?>
<!--                        </tr>-->

                        <?php foreach ( $customer_orders as $customer_order ) :
                            $order      = wc_get_order( $customer_order );
                            $item_count = $order->get_item_count();
                            ?>
                            <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr( $order->get_status() ); ?> order">
                                <?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
                                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr( $column_id ); ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
                                        <?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
                                            <?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

                                        <?php elseif ( 'order-number' === $column_id ) : ?>
                                            <a href="<?php echo esc_url( $order->get_view_order_url() ); ?>">
                                                <?php echo '#' . $order->get_order_number(); ?>
                                            </a>

                                        <?php elseif ( 'order-date' === $column_id ) : ?>
                                            <time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'd M Y' ) ); ?>"><?php echo esc_html( $order->get_date_created()->date( 'd M Y' )  ); ?></time>

                                        <?php elseif ( 'order-status' === $column_id ) : ?>
                                            <?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

                                        <?php elseif ( 'order-total' === $column_id ) : ?>
                                            <?php
                                            /* translators: 1: formatted order total 2: total order items */
                                            printf( _n( '%1$s', '%1$s', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count );
                                            ?>

                                        <?php elseif ( 'order-actions' === $column_id ) : ?>
                                            <?php
                                            $actions = array(
                                                'pay'    => array(
                                                    'url'  => $order->get_checkout_payment_url(),
                                                    'name' => __( 'Pay', 'woocommerce' ),
                                                ),
                                                'view'   => array(
                                                    'url'  => $order->get_view_order_url(),
                                                    'name' => __( 'View', 'woocommerce' ),
                                                ),
                                                'cancel' => array(
                                                    'url'  => $order->get_cancel_order_url( wc_get_page_permalink( 'myaccount' ) ),
                                                    'name' => __( 'Cancel', 'woocommerce' ),
                                                ),
                                            );

                                            if ( ! $order->needs_payment() ) {
                                                unset( $actions['pay'] );
                                            }

                                            if ( ! in_array( $order->get_status(), apply_filters( 'woocommerce_valid_order_statuses_for_cancel', array( 'pending', 'failed' ), $order ) ) ) {
                                                unset( $actions['cancel'] );
                                            }

                                            if ( $actions = apply_filters( 'woocommerce_my_account_my_orders_actions', $actions, $order ) ) {
                                                foreach ( $actions as $key => $action ) {
                                                    echo '<a href="' . esc_url( $action['url'] ) . '" class="woocommerce-button button el-btn mod-look ' . sanitize_html_class( $key ) . '">' . get_field( "посмотреть", 'option' ) . '</a>';
                                                }
                                            }
                                            ?>
                                        <?php endif; ?>
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    </div>
                </div>
                    <!-- end hist -->
                    <?php if ( count( $customer_orders ) == 3 ) : ?>
                    <div class="userdata-stats-more">
                        <a href="<?php echo wc_get_endpoint_url('orders'); ?>">
                            <?php echo get_field( "вся_история_покупок", 'option' ); ?> <i class="icon-right-arrow"></i>
                        </a>
                    </div>
                        <?php endif; ?>

                    <?php do_action( 'woocommerce_before_account_orders_pagination' ); ?>

                    <?php if ( 1 < $customer_orders->max_num_pages ) : ?>
                        <div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
                            <?php if ( 1 !== $current_page ) : ?>
                                <a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url( wc_get_endpoint_url( 'orders', $current_page - 1 ) ); ?>"><?php _e( 'Previous', 'woocommerce' ); ?></a>
                            <?php endif; ?>

                            <?php if ( intval( $customer_orders->max_num_pages ) !== $current_page ) : ?>
                                <a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url( wc_get_endpoint_url( 'orders', $current_page + 1 ) ); ?>"><?php _e( 'Next', 'woocommerce' ); ?></a>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                <?php else : ?>
                    <!-- hist -->
                    <div class="hist">
                        <div class="hist-nothing">
                            <div class="col-xs-12 col-md-7 valign">
                                <div class="hist-nothing-pic">
                                    <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/monster.svg" alt="">
                                </div>
                                <div class="hist-nothing-txt">
                                    <?php echo get_field( "вы_еще_ничего_не_заказали", 'option' ); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-5 valign">
                                <a href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>" class="el-btn mod-grad mod-arr">
                                    <?php echo get_field( "в_магазин", 'option' ); ?>
                                </a>
                            </div>
                        </div>
                    </div>

                <?php endif; ?>
            </div>
            <div class="userdata-stats-block">
                <div class="userdata-stats-h">
                    <?php echo get_field( "blocks_otzivov", 'option' ); ?>
                </div>
                <div class="userdata-stats-descr">
                    <?php echo get_field( "надпись_поделиться", 'option' ); ?>
                </div>

                <?php
                $customer_orders = get_posts( array(
                    'numberposts' => 1,
                    'meta_key'    => '_customer_user',
                    'meta_value'  => get_current_user_id(),
                    'post_type'   => wc_get_order_types(),
                    'post_status' => array_keys( wc_get_order_statuses() ),
                ) );
                $customer = wp_get_current_user();
                //var_dump($customer_orders);
                if ( count( $customer_orders ) >= 1 ) : ?>

                <!-- userfeed -->
                <div class="row ">
                    <div class="userfeed">

                        <div class="userfeed-row js-rowToDelete">
                            <div class="col-xs-12 col-md-3 valign">
                                <div class="userfeed-row-pic">
                                    <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/ct.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4 valign">
                                <a href="#" class="userfeed-row-title">
                                    Крем-стик с эфирными маслами для детей
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-4 valign">
                                <a href="#sendfeed" class="el-btn mod-grad">
                                    <?php echo get_field( "napisat_otziv_block", 'option' ); ?>
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-1 valign">
                                <a href="#" class="table-row-delete">
                                    <i class="icon-delete"></i>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end userfeed -->
                    <?php endif; ?>
                <div class="userdata-stats-more">
                    <a href="<?php echo wc_get_endpoint_url('my-otzivi'); ?>">
                        <?php echo get_field( "еще_отзывы", 'option' ); ?> <i class="icon-right-arrow"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
//	/**
//	 * My Account dashboard.
//	 *
//	 * @since 2.6.0
//	 */
//	do_action( 'woocommerce_account_dashboard' );
//
//	/**
//	 * Deprecated woocommerce_before_my_account action.
//	 *
//	 * @deprecated 2.6.0
//	 */
//	do_action( 'woocommerce_before_my_account' );
//
//	/**
//	 * Deprecated woocommerce_after_my_account action.
//	 *
//	 * @deprecated 2.6.0
//	 */
//	do_action( 'woocommerce_after_my_account' );
//
///* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */

