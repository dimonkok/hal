<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="container">
    <div class="cabcontent-title">
    <div class="cabcontent-title-h">
        <?php echo get_field( "istoriya_pokupok", 'option' ); ?>
    </div>
</div>
<!-- history-block -->
<div class="history-block">

    <div class="history-block-date">
        <?php echo get_field( "zakaz_podrob_history", 'option' ); ?> #<?php echo $order->get_order_number(); ?> <?php echo get_field( "ot_podrob_history", 'option' ); ?> <?php echo wc_format_datetime( $order->get_date_created() ); ?>
    </div>
<!--<p>--><?php
//	/* translators: 1: order number 2: order date 3: order status */
//	printf(
//		__( 'Order #%1$s was placed on %2$s and is currently %3$s.', 'woocommerce' ),
//		'<mark class="order-number">' . $order->get_order_number() . '</mark>',
//		'<mark class="order-date">' . wc_format_datetime( $order->get_date_created() ) . '</mark>',
//		'<mark class="order-status">' . wc_get_order_status_name( $order->get_status() ) . '</mark>'
//	);
//?><!--</p>-->

<?php //if ( $notes = $order->get_customer_order_notes() ) : ?>
<!--	<h2>--><?php //_e( 'Order updates', 'woocommerce' ); ?><!--</h2>-->
<!--	<ol class="woocommerce-OrderUpdates commentlist notes">-->
<!--		--><?php //foreach ( $notes as $note ) : ?>
<!--		<li class="woocommerce-OrderUpdate comment note">-->
<!--			<div class="woocommerce-OrderUpdate-inner comment_container">-->
<!--				<div class="woocommerce-OrderUpdate-text comment-text">-->
<!--					<p class="woocommerce-OrderUpdate-meta meta">--><?php //echo date_i18n( __( 'l jS \o\f F Y, h:ia', 'woocommerce' ), strtotime( $note->comment_date ) ); ?><!--</p>-->
<!--					<div class="woocommerce-OrderUpdate-description description">-->
<!--						--><?php //echo wpautop( wptexturize( $note->comment_content ) ); ?>
<!--					</div>-->
<!--	  				<div class="clear"></div>-->
<!--	  			</div>-->
<!--				<div class="clear"></div>-->
<!--			</div>-->
<!--		</li>-->
<!--		--><?php //endforeach; ?>
<!--	</ol>-->
<?php //endif; ?>
    <?php $order = wc_get_order( $order_id );
    $show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
    $show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
    ?>

    <table class="cart-table">
        <tbody>
        <tr>
            <th>
                <?php echo get_field( "name_podrob_history", 'option' ); ?>
            </th>
            <th></th>
            <th>
                <?php echo get_field( "kol_podrob_history", 'option' ); ?>
            </th>
            <th>
                <?php echo get_field( "sum_podrob_history", 'option' ); ?>
            </th>
            <th>
                <?php echo get_field( "bye_podrob_history", 'option' ); ?>
            </th>
        </tr>

            <?php foreach ( $order->get_items() as $item_id => $item ) :
            $product = apply_filters( 'woocommerce_order_item_product', $item->get_product(), $item ); ?>
        <tr class="js-basketRow">
                <td class="fav-pic">
                    <?php
                        $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $item->get_product_id() ), 'single-post-thumbnail' );
                    ?>
                    <img src="<?php echo $image_url[0]; ?>">
                </td>
                <td class="fav-info">
                    <div class="fav-item">
                        <?php echo $item->get_name(); ?>
                    </div>
                    <a href="#" class="fav-add js-add-to-fav" data-id="1">
                  <span>
                    <i class="icon-like"></i>
                  </span>
                        <p>
                            <?php echo get_field( "in_spis_podrob_history", 'option' ); ?>
                        </p>
                    </a>
                </td>
                <td class="fav-num">
                    <?php echo $item->get_quantity(); ?> шт
                </td>
                <td class="fav-price">
                    <?php
                     echo $order->get_formatted_line_subtotal( $item );
                    // echo $item->get_subtotal();
                    //var_dump($item); ?>
                    <p>
                        <?php echo get_field( "stoimost_podrob_history", 'option' ); ?>
                    </p>
                </td>
                <td class="fav-buy">
                    <?php  echo $order->get_formatted_line_subtotal( $item ); ?>
                    <p>
                        <?php echo get_field( "thcena_za_podrob_history", 'option' ); ?>
                    </p>
                    <a href="#" data-product_id="<?php echo $product->get_id(); ?>" class="el-btn mod-grad tocart add_to_cart_button ajax_add_to_cart btn btn-hg hg-product-add-to-cart-button">
                    <span>
                      <i class="icon-shopping-cart"></i>
                    </span>
                        <span><?php echo get_field( "cart_podrob_history", 'option' ); ?></span>
                    </a>
                </td>
        </tr>
          <?php  endforeach;
            ?>

        </tbody>
    </table>

<?php// do_action( 'woocommerce_view_order', $order_id ); ?>
