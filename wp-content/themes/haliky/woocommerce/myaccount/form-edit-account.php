<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if (!defined('ABSPATH')) {
    exit;
}

do_action('woocommerce_before_edit_account_form'); ?>
<div class="avatar_change remodal" data-remodal-id="avatar_change">
    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
    <?php echo do_shortcode('[avatar_upload]'); ?>
</div>
<div class="container">
    <div class="cabcontent-title">
        <div class="cabcontent-title-h">
            <?php echo get_field("detali_profily_text", 'option'); ?>
        </div>
    </div>
    <!-- form -->
    <form class="form-edit-account" action="" method="post">
        <div class="orderform">
            <?php do_action('woocommerce_edit_account_form_start'); ?>
            <div class="orderform-half col-xs-12 col-md-6 ">
                <div class="orderform-title">
                    <?php echo get_field("личные_данные", 'option'); ?>
                </div>
                <div>
                    <div class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text"
                               name="account_first_name" placeholder="Ф.И.О." id="account_first_name"
                               value="<?php echo esc_attr($user->first_name); ?>"/>
                    </div>
                    <div class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                        <input type="text" name="address"
                               value="<?php echo get_user_meta($user->ID, 'billing_address_1', true); ?>"
                               placeholder="ГОРОД / НАСЕЛЕННыЙ ПУНКТ">
                    </div>
                    <div class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
                        <input type="tel" name="tel"
                               value="<?php echo get_user_meta($user->ID, 'billing_phone', true); ?>"
                               placeholder="контактный телефон">
                    </div>
                    <div class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                        <input type="email" class="woocommerce-Input woocommerce-Input--email input-text"
                               name="account_email" id="account_email" placeholder="email"
                               value="<?php echo get_user_meta($user->ID, 'billing_email', true); ?>"/>
                    </div>
                </div>

                <div class="orderform-ava">
                    <div class="orderform-ava-title"><?php echo get_field("аватар", 'option'); ?></div>
                    <?php $user = wp_get_current_user();
                    $avtarimg = scrapeImage(get_avatar($user->ID));
                    ?>
                    <a href="#" class="userdata-inner-ava js-bg-contain" data-img="<?php echo $avtarimg; ?>"></a>
                    <div class="orderform-ava-actions">
                        <a href="#avatar_change">
                            <i class="icon-right-arrow"></i><?php echo get_field("обновить_фотографию", 'option'); ?>
                        </a>
                        <a href="#" id="wpua-remove-existing">
                            <i class="icon-cross"></i><?php echo get_field("удалить_фотографию", 'option'); ?>
                        </a>
                    </div>
                </div>
            </div>

            <div class="orderform-half col-xs-12 col-md-6 ">
                <div class="orderform-title">
                    <?php echo get_field("адрес_доставки", 'option'); ?>
                </div>
                <div>
                    <div class="form-row">
                        <input type="text" name="address-in"
                               value="<?php echo get_user_meta($user->ID, 'shipping_city', true); ?>"
                               placeholder="ГОРОД / НАСЕЛЕННыЙ ПУНКТ">
                    </div>
                    <div class="form-row">
                        <input type="text" name="street"
                               value="<?php echo get_user_meta($user->ID, 'shipping_address_1', true); ?>"
                               placeholder="Улица">
                    </div>
                    <div class="form-row">
                        <input type="text" name="dom"
                               value="<?php echo get_user_meta($user->ID, 'shipping_address_2', true); ?>"
                               placeholder="Дом / Корпус">
                    </div>
                    <div class="form-row">
                        <input type="text" name="flat"
                               value="<?php echo get_user_meta($user->ID, 'shipping_state', true); ?>"
                               placeholder="Квартира">
                    </div>
                    <div class="form-row">
                        <input type="text" name="dopinfo"
                               value="<?php echo get_user_meta($user->ID, 'order_comments', true); ?>"
                               placeholder="Дополнительная информация">
                    </div>
                    <div class="form-row">
                        <input type="password" class="woocommerce-Input woocommerce-Input--password input-text"
                               placeholder="Текущий пароль" name="password_current" id="password_current"/>
                    </div>
                    <div class="form-row">
                        <input type="password" class="woocommerce-Input woocommerce-Input--password input-text"
                               placeholder="Новый пароль" name="password_1" id="password_1"/>
                    </div>
                    <div class="form-row">
                        <input type="password" class="woocommerce-Input woocommerce-Input--password input-text"
                               placeholder="Повторите пароль" name="password_2" id="password_2"/>
                    </div>
                </div>

            </div>
        </div>
</div>
<div class="clear"></div>

<?php do_action('woocommerce_edit_account_form'); ?>

<p>
    <?php //wp_nonce_field( 'save_account_details' ); ?>
<div class="orderform-btn text-center col-xs-12">
    <button type="submit" role="submit" id="formeditsubmit" class="el-btn mod-grad">
        <?php echo get_field("сохранить", 'option'); ?>
    </button>
</div>

</p>

<?php do_action('woocommerce_edit_account_form_end'); ?>
</form>
</div>
<?php do_action('woocommerce_after_edit_account_form'); ?>
