<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $order_idsik;
$order_idsik = $order->id;
$order_meta = get_post_meta($order_idsik);

if ($order_meta['wpml_language'][0] == "ua") {
    $thenck_processing_orderss = "ДЯКУЄМО ЗА ЗАМОВЛЕННЯ";
    $text_processing_order_firstss = "Ми прийняли ваше замовлення і обробляємо його.";
    $text_processing_order_secondss = "Деталі замовлення наведено нижче:";
} else {
    $thenck_processing_orderss = "СПАСИБО ЗА ЗАКАЗ";
    $text_processing_order_firstss = "Мы приняли ваш заказ и обрабатываем его.";
    $text_processing_order_secondss = "Детали заказа приведены ниже:";
}
/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
    <table border="0" cellpadding="0" cellspacing="0" width="610" id="template_header">
        <tr>
            <td id="header_wrapper_process">
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/haliky/img/logo_leter.png">
                <h3 id="welcome_textss"><?php echo $thenck_processing_orderss; ?></h3>
            </td>
        </tr>
    </table>
    <!-- End Header -->
    </td>
    </tr>
    <tr>
    <td align="center" valign="top">
    <!-- Body -->
    <table border="0" cellpadding="0" cellspacing="0" width="610" id="template_body">
    <tr>
    <td valign="top" id="body_content">
    <!-- Content -->
    <table border="0" cellpadding="20" cellspacing="0" width="100%">
    <tr>
    <td valign="top" id="text_padding_process">
    <div id="body_content_inner">

    <p><?php echo $text_processing_order_firstss; ?></p>
    <p><?php echo $text_processing_order_secondss; ?></p>

<?php

/**
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
