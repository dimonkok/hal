<?php
/**
 * Email Styles
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-styles.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Load colors
$bg              = get_option( 'woocommerce_email_background_color' );
$body            = get_option( 'woocommerce_email_body_background_color' );
$base            = get_option( 'woocommerce_email_base_color' );
$base_text       = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text            = get_option( 'woocommerce_email_text_color' );

$bg_darker_10    = wc_hex_darker( $bg, 10 );
$body_darker_10  = wc_hex_darker( $body, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$base_lighter_40 = wc_hex_lighter( $base, 40 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );

// !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
?>

#wrapper {
	background-color: <?php echo esc_attr( $bg ); ?>;
	margin: 0;
	padding: 70px 0 70px 0;
	-webkit-text-size-adjust: none !important;
	width: 100%;
}

#template_container {
	box-shadow: 0 1px 4px rgba(0,0,0,0.1) !important;
	background-color: <?php echo esc_attr( $body ); ?>;
	border: none;
	border-radius: 3px !important;
}

#template_header {
    background-color: #ffffff;
	border-radius: 3px 3px 0 0 !important;
	color: <?php echo esc_attr( $base_text ); ?>;
	border-bottom: 0;
	font-weight: bold;
	line-height: 100%;
	vertical-align: middle;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

#template_header h1,
#template_header h1 a {
	color: <?php echo esc_attr( $base_text ); ?>;
}

#template_footer td {
	padding: 0;
	-webkit-border-radius: 6px;
}

#template_footer #credit {
	border:0;
	color: <?php echo esc_attr( $base_lighter_40 ); ?>;
	font-family: Arial;
	font-size:12px;
	line-height:125%;
	text-align:center;
    padding: 40px 48px 48px 48px;
}

#body_content {
	background-color: <?php echo esc_attr( $body ); ?>;
}

#body_content table td {
    padding: 20px 60px;
}

#body_content table td td {
	padding: 12px;
}

#body_content table td th {
	padding: 12px;
}

#body_content td ul.wc-item-meta {
	font-size: small;
	margin: 1em 0 0;
	padding: 0;
	list-style: none;
}

#body_content td ul.wc-item-meta li {
	margin: 0.5em 0 0;
	padding: 0;
}

#body_content td ul.wc-item-meta li p {
	margin: 0;
}

#body_content p {
	margin: 0 0 16px;
}

#body_content_inner {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
    font-family: "Open Sans", Arial, sans-serif;
	font-size: 14px;
	line-height: 150%;
	text-align: center;
}

.td {
	color: <?php echo esc_attr( $text_lighter_20 ); ?>;
	border: none;
    font-family: "Open Sans", Arial, sans-serif;
}

.text {
	color: <?php echo esc_attr( $text ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
}

.link {
	color: <?php echo esc_attr( $base ); ?>;
}

#header_wrapper {
    padding: 36px 48px 12px;
	display: block;
    text-align: center;
}

h1 {
	color: <?php echo esc_attr( $base ); ?>;
	font-family: "Helvetica Neue", Helvetica, Roboto, Arial, sans-serif;
	font-size: 30px;
	font-weight: 300;
	line-height: 150%;
	margin: 0;
	text-align: <?php echo is_rtl() ? 'right' : 'left'; ?>;
	text-shadow: 0 1px 0 <?php echo esc_attr( $base_lighter_20 ); ?>;
	-webkit-font-smoothing: antialiased;
}

h2 {
    color: #1f58b6;
    display: block;
    font-family: "Open Sans Bold", Arial, sans-serif;
    font-size: 19px;
    font-weight: 700;
    line-height: 130%;
    margin: 16px 0 22px;
    text-transform: uppercase;
    text-align: center;
}

h3 {
    color: #1f58b6;
    display: block;
    font-family: "Open Sans Bold", Arial, sans-serif;
    font-size: 18px;
    font-weight: bold;
    margin: 15px 0 8px;
    letter-spacing: 0.9px;
}

a {
    color: #1f58b6;
    font-weight: normal;
    text-decoration: underline;
    font-size: 15px;
}

p {
    margin: 0 0 16px;
    font-weight: normal;
    font-size: 15px;
    letter-spacing: -0.1px;
    line-height: 1.6;
    font-family: "Open Sans", Arial, sans-serif;
}

img {
	border: none;
	display: inline;
	font-size: 14px;
	font-weight: bold;
	height: auto;
	line-height: 100%;
	outline: none;
	text-decoration: none;
	text-transform: capitalize;
}
#text_padding {
    padding: 20px 65px !important;
}
#firs_block {
    margin: 26px 0 27px !important;
    font-size: 14.5px !important;
    letter-spacing: -0.5px !important;
    line-height: 1.6 !important;
    font-family: "Open Sans", Arial, sans-serif !important;
}
#credit p {
    margin: 0px !important;
    font-weight: normal !important;
    font-size: 15px !important;
    letter-spacing: -0.4px !important;
    line-height: 1.6 !important;
    font-family: "Open Sans", Arial, sans-serif;
    color: #0671cf !important;
}

#header_wrapper_process {
    padding: 36px 48px 0px;
    display: block;
    text-align: center;
}

#header_wrapper_process #welcome_textss {
    color: #1f58b6;
    display: block;
    font-family: "Open Sans Bold", Arial, sans-serif;
    font-size: 19px;
    font-weight: 600;
    margin: 22px 0 8px;
    letter-spacing: 0.9px;
}

#body_content_inner p {
    margin: 0;
    font-weight: normal;
    font-size: 15px;
    letter-spacing: -0.1px;
    line-height: 1.6;
    font-family: "Open Sans", Arial, sans-serif;
}


#text_padding_process {
    padding: 13px 45px !important;
}

#beack_color {
    background: #f3f4f5;
}

.order_item .td {
    text-align: left;
    vertical-align: middle;
    border: none;
    word-wrap: break-word;
    font-family: "Open Sans", Arial, sans-serif;
    line-height: 1.5;
    font-size: 14px;
    padding: 8px 0px 28px 18px;
}

#beack_color #product {
    text-align: left;
    color: #636363;
    border: none;
    font-family: "Open Sans", Arial, sans-serif;
    padding: 8px 20px 18px;
    font-size: 15px;
    white-space: nowrap;
}

#beack_color #kolich {
    text-align: left;
    color: #636363;
    border: none;
    font-family: "Open Sans", Arial, sans-serif;
    padding: 8px 34px 18px;
    font-size: 15px;
    white-space: nowrap;
}

#beack_color #price {
    text-align: left;
    color: #636363;
    border: none;
    font-family: "Open Sans", Arial, sans-serif;
    padding: 8px 25px 18px;
    font-size: 15px;
    white-space: nowrap;
}

.order_item .td:nth-child(1) {
    border-width: 0 0 2px 0;
    border-bottom-color: #f3f4f5;
    border-bottom-style: solid;
    text-align: left !important;
    vertical-align: middle !important;
    word-wrap: break-word !important;
    color: #5e5e5e !important;
    font-family: "Open Sans", Arial, sans-serif !important;
    line-height: 1.5 !important;
    font-size: 14px !important;
    padding: 12px 0px 20px 20px !important;
}

.order_item .td:nth-child(2) {
    text-align: center !important;
    vertical-align: top !important;
    border-width: 0 0 2px 0;
    border-bottom-color: #f3f4f5;
    border-bottom-style: solid;
    color: #5e5e5e !important;
    font-family: "Open Sans", Arial, sans-serif !important;
    word-wrap: break-word !important;
    line-height: 1.5 !important;
    font-size: 14px !important;
    padding: 14px 0px 0px 0px !important;
}

.order_item .td:nth-child(3) {
    text-align: left !important;
    vertical-align: top !important;
    border-width: 0 0 2px 0;
    border-bottom-color: #f3f4f5;
    border-bottom-style: solid;
    color: #5e5e5e !important;
    font-family: "Open Sans", Arial, sans-serif !important;
    word-wrap: break-word !important;
    line-height: 1.5 !important;
    font-size: 14px !important;
    padding: 14px 0px 0px 14px !important;
}

#order_details tfoot tr:nth-child(1) th{
    font-size: 15px;
    padding: 20px;
    border-width: 0 0 2px 0;
    border-bottom-color: #f3f4f5;
    border-bottom-style: solid;
}

#order_details tfoot tr td {
    border-width: 0 0 2px 0;
    border-bottom-color: #f3f4f5;
    border-bottom-style: solid;
    font-size: 14px !important;
}

#order_details tfoot tr:nth-child(2) th {
    padding: 16px 20px;
    font-size: 15px;
    border-width: 0 0 2px 0;
    border-bottom-color: #f3f4f5;
    border-bottom-style: solid;
}

#order_details tfoot tr th {
    color: #5e5e5e;
    font-family: "Open Sans Bold", Arial, sans-serif;
    font-size: 14.4px;
}

#order_details tfoot tr:nth-child(3) th {
    text-align: left;
    color: #5e5e5e;
    border: none;
    font-family: "Open Sans Bold", Arial, sans-serif;
    padding: 23px 20px 42px;
    font-size: 14.4px;
}

#order_details tfoot tr:nth-child(4) th {
    text-align: left;
    color: #5e5e5e;
    border: none;
    font-family: "Open Sans Bold", Arial, sans-serif;
    padding: 13px 21px 8px;
    font-size: 14.4px;
    background: #f3f4f5;
}

#order_details tfoot tr:nth-child(3) td {
    border-width: 0;
    border-bottom-style: none;
}

#order_details tfoot tr:nth-child(4) td {
    text-align: left !important;
    color: #5e5e5e !important;
    border: none !important;
    font-family: "Open Sans", Arial, sans-serif !important;
    padding: 12px !important;
    font-size: 14px;
    font-weight: 800;
    background: #f3f4f5 !important;
}

#customer_details {
    color: #1f58b6;
    display: block;
    font-family: "Open Sans Bold", Arial, sans-serif;
    font-size: 19px;
    font-weight: 700;
    line-height: 130%;
    letter-spacing: 1.5px;
    margin: 36px 2px 20px;
    text-transform: none;
    text-align: left;
}

#styler_list {
    text-align: left;
    padding: 0px 0px 0px 45px;
    line-height: 1.7;
}

#styler_list li strong {
    font-family: "Open Sans Bold", Arial, sans-serif;
}

#addresses .td {
    padding: 16px 3px;
}

#addresses h3 {
    font-family: "Open Sans Bold", sans-serif;
    font-size: 19px;
}

#body_content_inners {
    text-align: left;
}

#welcome_text_completed {
    color: #1f58b6;
    display: block;
    font-family: "Open Sans Bold", Arial, sans-serif;
    font-size: 18px;
    font-weight: bold;
    line-height: 1.4;
    margin: 18px 0 0px;
    letter-spacing: 0.9px;
}

#text_padding_order_completed {
    padding: 0px 60px !important;
}

#body_content_inners_completed_order p {
    margin: 0 0 0px !important;
}

#body_content_inners_completed_order h2 {
    margin: 21px 0 30px;
}
<?php
