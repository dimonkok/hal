<?php
/**
 * Customer on-hold order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-on-hold-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$order_id = $order->id;
$order_meta = get_post_meta($order_id);

if ($order_meta['wpml_language'][0] == "ua") {
    $thenck_processing_order_hold = "ДЯКУЄМО ЗА ЗАМОВЛЕННЯ";
    $weiting_payment = "Ми прийняли ваше замовлення і очікуємо підтвердженя оплати.";
    $text_processing_order_second = "Деталі замовлення наведено нижче:";
} else {
    $thenck_processing_order_hold = "СПАСИБО ЗА ЗАКАЗ";
    $weiting_payment = "Мы приняли ваш заказ и ожидаем подтверждения оплаты.";
    $text_processing_order_second = "Детали заказа приведены ниже:";
}
/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

    <table border="0" cellpadding="0" cellspacing="0" width="610" id="template_header">
        <tr>
            <td id="header_wrapper_process">
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/haliky/img/logo_leter.png">
                <h3 id="welcome_textss"><?php echo $thenck_processing_order_hold; ?></h3>
            </td>
        </tr>
    </table>
    <!-- End Header -->
    </td>
    </tr>
    <tr>
    <td align="center" valign="top">
    <!-- Body -->
    <table border="0" cellpadding="0" cellspacing="0" width="610" id="template_body">
    <tr>
    <td valign="top" id="body_content">
    <!-- Content -->
    <table border="0" cellpadding="20" cellspacing="0" width="100%">
    <tr>
    <td valign="top" id="text_padding_process">
    <div id="body_content_inner">

    <p><?php echo $weiting_payment; ?></p>
    <p><?php echo $text_processing_order_second; ?></p>
<?php

/**
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
