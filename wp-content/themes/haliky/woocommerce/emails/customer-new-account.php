<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$order_id = $order->id;
$order_meta = get_post_meta($order_id);

if (ICL_LANGUAGE_CODE == "ua" || $order_meta['wpml_language'][0] == "ua") {
    $welcome_all_user = "Вітаємо в інтернет-магазині Халіку";
    $welcome_text1 = "Дякуємо за створення акаунту в інтернет-магазині";
    $first_name = "Ваше ім’я користувача";
    $password_user = "Ваш пароль було автоматично згенеровано";
    $linck_user = "Ви можете змінити пароль та іншу особисту інформацію, а також переглянути свої замовлення в своєму обліковому записі за посиланням";
} else {
    $welcome_all_user = "Добро пожаловать в интерет-магазин Халику";
    $welcome_text1 = "Спасибо за создание аккаунта в магазине";
    $first_name = "Ваше имя пользователя";
    $password_user = "Ваш пароль был автоматически сгенерирован";
    $linck_user = "Вы можете изменить пароль и другую личную информацию, а так же просматривать свои заказы в своей учетной записи по ссылке";
}

 do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
    <table border="0" cellpadding="0" cellspacing="0" width="610" id="template_header">
        <tr>
            <td id="header_wrapper">
                <img src="<?php echo get_site_url(); ?>/wp-content/themes/haliky/img/logo_leter.png">
                <h3 id="welcome_text"><?php echo $welcome_all_user; ?></h3>
            </td>
        </tr>
    </table>
    <!-- End Header -->
    </td>
    </tr>
    <tr>
    <td align="center" valign="top">
    <!-- Body -->
    <table border="0" cellpadding="0" cellspacing="0" width="610" id="template_body">
    <tr>
    <td valign="top" id="body_content">
    <!-- Content -->
    <table border="0" cellpadding="20" cellspacing="0" width="100%">
    <tr>
    <td valign="top" id="text_padding">

    <div id="body_content_inners">

    <p><?php echo $welcome_text1; ?> <?php echo esc_html( $blogname );?>.</p></br>

    <p id="firs_block"><?php echo $first_name; ?>: <strong><?php echo esc_html( $user_login );?></strong><br>
    <?php echo $password_user; ?>: <strong><?php echo esc_html( $user_pass );?></strong></p>

    <p><?php echo $linck_user; ?>:
        <?php echo make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) )); ?></p>

<?php do_action( 'woocommerce_email_footer', $email );
