<?php
/**
 * Additional Customer Details
 *
 * This is extra customer data which can be filtered by plugins. It outputs below the order item table.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-customer-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $order_idsik;
$order_metas = get_post_meta($order_idsik);

if ($order_metas['wpml_language'][0] == "ua") {
    $poshta_name = "Відділення нової пошти";
} else {
    $poshta_name = "Отделение новой почты";
}
?>
<?php if ( ! empty( $fields ) ) : ?>
	<h2 id="customer_details"><?php _e( 'Customer details', 'woocommerce' ); ?>:</h2>
	<ul id="styler_list">
		<?php foreach ( $fields as $field ) : ?>
			<li><strong><?php echo wp_kses_post( $field['label'] ); ?>:</strong> <span class="text"><?php echo wp_kses_post( $field['value'] ); ?></span></li>
		<?php endforeach; ?>
        <?php if(isset($order_metas[$poshta_name][0])) : ?>
        <li><strong><?php echo $poshta_name; ?>:</strong> <?php echo $order_metas[$poshta_name][0]; ?></li>
        <?php endif; ?>
	</ul>
<?php endif; ?>
