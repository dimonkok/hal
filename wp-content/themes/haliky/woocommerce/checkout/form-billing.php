<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */

?>
<div class="woocommerce-billing-fields">
    <div class="orderform-title">
        <?php
            $dannie_zakaz = get_field( "данные_для_оформления_заказа:", 'option' );
            echo $dannie_zakaz;
        ?>
    </div>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<div class="woocommerce-billing-fields__field-wrapper">
        <?php if(!is_user_logged_in()) :?>
            <?php if ( ICL_LANGUAGE_CODE=='ua' ) : ?>
                <div class="orderform-descr">
                    <a href="<?php echo get_permalink(woocommerce_get_page_id('myaccount')); ?>">Авторизуйтесь</a>, щоб не заповнювати поля
                </div>
            <?php else : ?>
                <div class="orderform-descr">
                    <a href="<?php echo get_permalink(woocommerce_get_page_id('myaccount')); ?>">Авторизируйтесь</a>, чтобы не заполнять поля
                </div>
            <?php endif; ?>
        <?php endif; ?>
            <div class="first-step">
		<?php
			$fields = $checkout->get_checkout_fields( 'billing' );

			foreach ( $fields as $key => $field ) {
				if ( isset( $field['country_field'], $fields[ $field['country_field'] ] ) ) {
					$field['country'] = $checkout->get_value( $field['country_field'] );
				}
				woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
			}
		?>
                <?php if ( ICL_LANGUAGE_CODE=='ua' ) : ?>
                    <div class="orderform-btn" id="submit_first_step">
                        <button role="submit" class="el-btn mod-grad mod-arr">Відправити</button>
                    </div>
                <?php else : ?>
                    <div class="orderform-btn" id="submit_first_step">
                        <button role="submit" class="el-btn mod-grad mod-arr">Отправить</button>
                    </div>
                <?php endif; ?>
            </div>

	</div>
<div class="second-step">
	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
    <?php if (ICL_LANGUAGE_CODE == "ua") : ?>
        <div class="form-row">
            <?php $i = 1; ?>
            <select name="hero" required="" id="change_dost" aria-required="true">
                <option value="1" hidden data-id="<?php echo $i++; ?>">Спосіб доставки</option>
                <option value="2" data-id="<?php echo $i++; ?>">Кур'єр</option>
                <option value="3" data-id="<?php echo $i++; ?>">Нова Пошта</option>
                <option value="4" data-id="<?php echo $i++; ?>">Самовивіз</option>
            </select>
        </div>
        <div class="form-row oplata">
            <?php $m = 1; ?>
            <select required="" name="payment" aria-required="true" id="change_oplata">
                <option value="1" hidden data-id="<?php echo $m++; ?>">Спосіб оплати</option>
                <option value="2" data-id="<?php echo $m++; ?>">Готівка Кур'єру</option>
                <option value="3" data-id="<?php echo $m++; ?>">Платіж з післяплатою</option>
                <option value="4" data-id="<?php echo $m++; ?>">Оплата Картою</option>
                <option value="5" data-id="<?php echo $m++; ?>">Готівка</option>
            </select>
        </div>
    <?php else : ?>
    <div class="form-row">
        <?php $i = 1; ?>
        <select name="hero" required="" id="change_dost" aria-required="true">
            <option value="1" hidden data-id="<?php echo $i++; ?>">Способ доставки</option>
            <option value="2" data-id="<?php echo $i++; ?>">Курьер</option>
            <option value="3" data-id="<?php echo $i++; ?>">Новой почтой</option>
            <option value="4" data-id="<?php echo $i++; ?>">Самовывоз</option>
        </select>
    </div>
    <div class="form-row oplata">
        <?php $m = 1; ?>
        <select required="" name="payment" aria-required="true" id="change_oplata">
            <option value="1" hidden data-id="<?php echo $m++; ?>">Способ оплаты</option>
            <option value="2" data-id="<?php echo $m++; ?>">Наличными курьеру</option>
            <option value="3" data-id="<?php echo $m++; ?>">Наложенный платёж</option>
            <option value="4" data-id="<?php echo $m++; ?>">Оплата картой</option>
            <option value="5" data-id="<?php echo $m++; ?>">Наличкой</option>
        </select>
    </div>
    <?php endif; ?>
    
    </div>
</div>

<?php if ( ! is_user_logged_in() && $checkout->is_registration_enabled() ) : ?>
	<div class="woocommerce-account-fields">
		<?php if ( ! $checkout->is_registration_required() ) : ?>

			<p class="form-row form-row-wide create-account">
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true ) ?> type="checkbox" name="createaccount" value="1" /> <span><?php _e( 'Create an account?', 'woocommerce' ); ?></span>
				</label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( $checkout->get_checkout_fields( 'account' ) ) : ?>

			<div class="create-account">
				<?php foreach ( $checkout->get_checkout_fields( 'account' )  as $key => $field ) : ?>
					<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
				<?php endforeach; ?>
				<div class="clear"></div>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
	</div>
<?php endif; ?>
