<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}
?>

<div class="orderform-stats-row">
    <div class="orderform-stats-cell">
        <?php echo get_field("товар", 'option'); ?>
    </div>
    <div class="orderform-stats-cell">
        <?php echo get_field("сумма", 'option'); ?>
    </div>
</div>

<?php
do_action('woocommerce_review_order_before_cart_contents');

foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
    $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);

    if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key)) {
        ?>
        <div class="orderform-stats-row" data-id="<?php echo $cart_item[product_id]; ?>">
            <div class="orderform-stats-cell">
                <?php echo apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;'; ?>
                <?php echo apply_filters('woocommerce_checkout_cart_item_quantity', ' <span class="product-quantity">' . sprintf('&times; %s', $cart_item['quantity']) . '</span>', $cart_item, $cart_item_key); ?>
            </div>
            <div class="orderform-stats-cell">
                <?php echo WC()->cart->get_item_data($cart_item); ?>
                <?php echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); ?>
            </div>
        </div>

        <?php
    }
} ?>
<div class="orderform-stats-row" id="dost">
    <div class="orderform-stats-cell">
        <p>Доставка</p>
        <span id="insert">()</span>
    </div>
    <div class="orderform-stats-cell dostavkachena">

    </div>
</div>
<div class="orderform-stats-row">
    <?php foreach (WC()->cart->get_coupons() as $code => $coupon) : ?>

        <div class="orderform-stats-cell">
            <?php wc_cart_totals_coupon_label($coupon); ?>
        </div>
        <div class="orderform-stats-cell">
            <?php wc_cart_totals_coupon_html($coupon); ?>
        </div>

    <?php endforeach; ?>
</div>
<div class="orderform-stats-row mod-result">
    <div class="orderform-stats-cell">
        <?php echo get_field("к_оплате", 'option'); ?>
        <?php // _e( 'Subtotal', 'woocommerce' ); ?>
    </div>
    <div class="orderform-stats-cell" id="replases">
        <?php // wc_cart_totals_order_total_html(); ?>

        <?php wc_cart_totals_subtotal_html(); ?>
    </div>
</div>
</div>

<?php do_action('woocommerce_review_order_after_cart_contents'); ?>
<?php
if (ICL_LANGUAGE_CODE == "ua") {
    $id_agree_page = '813';
} else {
    $id_agree_page = '811';
}
$url = get_permalink($id_agree_page); ?>
<div class="orderform-agree">
    <input id="agree" type="checkbox" class="input-checkbox" required name="agree_order">
    <label for="agree">
        <?php echo get_field("я_ознакомлен_и_принимаю", 'option'); ?>
        <a href="<?php echo $url; ?>" target="_blank"><?php echo get_field("условия_магазина", 'option'); ?></a>
    </label>
    <input type="hidden" id="valid_check" name="terms-field" value="0"/>
</div>


