<?php
/**
 * Single Product Rating
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/rating.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.3.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( get_option( 'woocommerce_enable_review_rating' ) === 'no' ) {
	return;
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();
$idss = $product->id;
global $wpdb;
$count = $wpdb->get_var("SELECT COUNT(*) FROM `rating` WHERE `product_id` = 65");
$result = $wpdb->get_results('SELECT sum(rate) as result_value FROM `rating` WHERE `product_id` = 65');
$end_result = $result[0]->result_value;
$ratess = $end_result/$count;

 ?>
<script>
 var tovar_ids = "<?php echo $idss; ?>";
</script>
 <div class="tovar-slider-info-top">
	<div class="rate">
		<div class="rate_row">
            <?php
            for($i=1; $i <= 5; $i++) {
                ((int) $ratess >= $i) ? $active="active" : $active="";
                echo '<span class="rate_star '.$active.'" data-value="'.$i.'"></span>';
            }
            ?>
		</div>
    </div>


