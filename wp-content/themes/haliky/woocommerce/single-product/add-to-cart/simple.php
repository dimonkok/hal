<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.0.0
 */
if (!defined('ABSPATH')) {
    exit;
}

global $product;

if (!$product->is_purchasable()) {
    return;
}

echo wc_get_stock_html($product);

if ($product->is_in_stock()) : ?>

    <?php do_action('woocommerce_before_add_to_cart_form'); ?>
    <div class="tovar-slider-btns">
        <form class="cart" method="post" enctype='multipart/form-data'>
            <?php
            /**
             * @since 2.1.0.
             */
            do_action('woocommerce_before_add_to_cart_button');

            /**
             * @since 3.0.0.
             */
            do_action('woocommerce_before_add_to_cart_quantity');
            woocommerce_quantity_input(array(
                'min_value' => apply_filters('woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product),
                'max_value' => apply_filters('woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product),
                'input_value' => isset($_POST['quantity']) ? wc_stock_amount($_POST['quantity']) : $product->get_min_purchase_quantity(),
            ));
            /**
             * @since 3.0.0.
             */
            do_action('woocommerce_after_add_to_cart_quantity');
            ?>

            <button type="submit" name="add-to-cart" id="get_qty_to_cart" data-quantity="1" data-product_id="<?php echo esc_attr($product->get_id()); ?>" value="<?php echo esc_attr($product->get_id()); ?>"
                    class="el-btn mod-grad tocart button product_type_simple add_to_cart_button ajax_add_to_cart item-addbasket"> <span>
                    <i class="icon-shopping-cart"></i>
                  </span><span><?php echo esc_html($product->single_add_to_cart_text()); ?></span></button>
            <a href="#" class="like ajax" data-id="<?php echo $product->get_id(); ?>">
                <i class="icon-like"></i>
            </a>
            <?php
            /**
             * @since 2.1.0.
             */
            do_action('woocommerce_after_add_to_cart_button');
            ?>
        </form>
    </div>
    <div class="list_button">
        <?php // echo previous_post_link( '%link', '< %title', true ); ?>
        <?php previous_post_link('%link', '', true, '', 'product_cat'); ?>
        <?php next_post_link('%link', '', true, '', 'product_cat'); ?>
    </div>
    <?php do_action('woocommerce_after_add_to_cart_form'); ?>

<?php endif; ?>
