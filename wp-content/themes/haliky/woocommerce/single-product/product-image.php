<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;
$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
$image_title       = get_post_field( 'post_excerpt', $post_thumbnail_id );
$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'valign',
    'text-right'
) );
?>
<?php if(is_product()) : ?>
    <div class="tovar-slider">
<?php endif; ?>
<div class="valign text-right">
    <?php
    $beack_images = get_field('fon_kartinka_category', 'product_cat_'.$product->category_ids[0]);
            do_action( 'woocommerce_product_thumbnails' );
            echo '<div class="tovar-slider-main js-bg-cover" data-img="' .$beack_images. '">';
    $attachment_ids = $product->get_gallery_image_ids();
    $i = -1;
    if ( $attachment_ids && has_post_thumbnail() ) {
        foreach ( $attachment_ids as $attachment_id ) {
            $i = $i + 1;
            $full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
            $thumbnail       = wp_get_attachment_image_src( $attachment_id, 'full' );
            $image_title     = get_post_field( 'post_excerpt', $attachment_id );

            $attributes = array(
                'title'                   => $image_title,
                'data-src'                => $full_size_image[0],
            );

            $html  = '<div class="tovar-slider-main-slide" data-index="' .$i. '">';
            $html .= wp_get_attachment_image( $attachment_id, 'full', false, $attributes );
            $html .= '</div>';

            echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
        }
        echo '</div>';
    }
    ?>
</div>
