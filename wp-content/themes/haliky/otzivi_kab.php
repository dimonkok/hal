<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
global $wpdb;
global $woocommerce ;
global $order;
global $product;
do_action( 'woocommerce_before_account_navigation' );
?>
<div class="container ">
    <div class="page-heading">
        <div class="page-heading-title">
            <?php echo get_field( "мой_кабинет", 'option' ); ?>
        </div>
        <div class="cabinet-menu-wrap">
            <div class="cabinet-menu">
                <ul>
                    <?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
                        <li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
                            <a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
                        </li>
                    <?php endforeach; ?>
                    <li><a href="<?php echo wp_logout_url(home_url()); ?>" title="Logout"><?php echo get_field( "выход", 'option' ); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php do_action( 'woocommerce_after_account_navigation' ); ?>

<script>
    var ajaxurls = "<?php echo get_template_directory_uri(); ?>/delete.php";
</script>
<div class="cabcontent">
    <div class="container">
        <div class="cabcontent-title">
            <div class="cabcontent-title-h">
                <?php echo get_field( "otziv_ostavte", 'option' ); ?>
            </div>
            <div class="cabcontent-title-txt">
                <?php echo get_field( "text_otziv_ostavte", 'option' ); ?>
            </div>
        </div>
        <!-- userfeed -->
        <div class="row">
            <div class="userfeed">
                <?php
                $customer_orders = get_posts( array(
                    'numberposts' => 10,
                    'meta_key'    => '_customer_user',
                    'meta_value'  => get_current_user_id(),
                    'post_type'   => wc_get_order_types(),
                    'post_status' => 'wc-completed',
                ) );
                $ist = array();
                foreach ($customer_orders as $ordersik) :
                   // var_dump($ordersik);

                    $order = new WC_Order( $ordersik->ID );
                    $items = $order->get_items();
                    foreach ( $items as $item ) : ?>
                        <?php
                        $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $item['product_id'] ), 'full' );
                        //var_dump($item); ?>
                        <div class="userfeed-row">
                            <div class="col-xs-12 col-md-6 valign">
                                <div class="userfeed-row-pic">
                                    <img src="<?php echo $image_url[0]; ?>" alt="">
                                </div>
                                <a href="<?php echo get_permalink($item['product_id']); ?>" class="userfeed-row-title">
                                    <?php echo $item['name']; ?>
                                </a>
                            </div>
                            <div class="col-xs-12 col-md-6 valign">
                                <a href="#sendfeed-<?php echo $item['product_id']; ?>" class="el-btn mod-grad" data-id="<?php echo $item['product_id']; ?>">
                                    <?php echo get_field( "napisat_otziv_ostavte", 'option' ); ?>
                                </a>
                            </div>
                            <div class="sendfeed remodal" data-remodal-id="sendfeed-<?php echo $item['product_id']; ?>">
                                <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
                                <div id="sendfeeds-form">
                                    <div class="sendfeed-h">
                                        Оставьте отзыв
                                    </div>
                                    <a href="<?php echo get_permalink($item['product_id']); ?>">
                                        к товару
                                    </a>
                                    <div class="sendfeed-pic">
                                        <img src="<?php echo $image_url[0]; ?>" alt="">
                                    </div>
                                    <div class="sendfeed-title">
                                        <?php echo $item['name']; ?>
                                    </div>
                                    <div class="sendfeed-rate rate">
                                        <span class="rate_star" data-value="1"></span>
                                        <span class="rate_star" data-value="2"></span>
                                        <span class="rate_star" data-value="3"></span>
                                        <span class="rate_star" data-value="4"></span>
                                        <span class="rate_star" data-value="5"></span>
                                        <input type="hidden" class="get_star" value="">
                                    </div>
                                    <div class="sendfeed-txtarea">
                                        <div class="sendfeed-txtarea-h">
                                            ТЕКСТ ОТЗЫВА*
                                        </div>
                                        <textarea name="message" id="text_comments"></textarea>
                                    </div>
                                    <div class="sendfeed-btn">
                                        <button role="submit" id="fins_but" class="el-btn mod-grad" data-id="<?php echo $item['product_id']; ?>">
                                            Отправить
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                  <?php endforeach; endforeach; ?>

            </div>
        </div>
        <!-- end userfeed -->
        <!-- newfeed -->
        <div class="row">

            <div class="newfeed">

                <div class="cabcontent-title">
                    <div class="cabcontent-title-h">
                        <?php echo get_field( "vashi_napisat_otziv_ostavte", 'option' ); ?>
                    </div>
                    <div class="cabcontent-title-txt">
                        <?php echo get_field( "opiblick_napisat_otziv_ostavte", 'option' ); ?>
                    </div>
                </div>

                <?php
                $user_id = get_current_user_id();
                $args = array(
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'offset' => 0,
                    'number' => 2,
                    'user_id' => $user_id, // ID пользователя
                    'status' => 'approve',
                );

$comments = get_comments($args);
foreach($comments as $comment) :
    $image_urls = wp_get_attachment_image_src( get_post_thumbnail_id( $comment->comment_post_ID ), 'full' );
?>
                    <div class="newfeed-row" data-comment="<?php echo $comment->comment_ID; ?>">
                        <div class="col-xs-12 col-md-3 valign text-center">
                            <div class="newfeed-row-pic">
                                <img src="<?php echo $image_urls[0]; ?>" alt="">
                            </div>
                            <a href="#" class="userfeed-row-title">
                                <?php echo get_the_title( $comment->comment_post_ID ); ?>
                            </a>
                        </div>
                        <div class="col-xs-12 col-md-9 valign newfeed-content">
                            <div class="newfeed-row-top">

                                <div class="newfeed-rate rate">
                                    <?php
                                    $idss = $comment->comment_post_ID;
                                    $count = $wpdb->get_var("SELECT COUNT(*) FROM `rating` WHERE `product_id` = 65");
                                    $result = $wpdb->get_results('SELECT sum(rate) as result_value FROM `rating` WHERE `product_id` = 65');
                                    $end_result = $result[0]->result_value;
                                    $ratess = $end_result/$count;
                                    ?>
                                    <script>
                                        var tovar_ids = "<?php echo $idss; ?>";
                                    </script>
                                    <div class="rate_row fav-rate rate">
                                        <?php
                                        for($i=1; $i <= 5; $i++) {
                                            ((int) $ratess >= $i) ? $active="active" : $active="";
                                            echo '<span class="rate_star '.$active.'" data-value="'.$i.'"></span>';
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="newfeed-date">
                                    <?php echo get_comment_date('d F Y', $comment->comment_ID); ?>
                                </div>
                            </div>

                            <div class="newfeed-comment">
                                <?php echo $comment->comment_content; ?>
                            </div>
                            <a class="newfeed-delete" data-id="<?php echo $comment->comment_ID; ?>" href="#">
                                <i class="icon-close"></i>
                                <?php echo get_field( "delete_otziv_napisat_otziv_ostavte", 'option' ); ?>
                            </a>
                        </div>
                    </div>
            <?php endforeach;
                global $wpdb;
                $count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_comments` WHERE `user_id` = ".$user_id." AND `comment_approved` = 1");
                ?>
<!--                <a href="#" class="btn btn-red large show-more seecomments" data-count="--><?php //echo $count - 2; ?><!--" data-limit="2" data-id="--><?php //echo $user_id; ?><!--">Show more</a>-->
            </div>


        </div>
        <!-- end newfeed -->


    </div>

