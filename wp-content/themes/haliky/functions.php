<?php
/**
 * Функции шаблона (function.php)
 * @package WordPress
 * @subpackage your-clean-template
 */

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title'    => 'Опции темы',
        'menu_title'    => 'Опции темы',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Надписи шапки',
        'menu_title'    => 'Шапка',
        'menu_slug'     => 'header',
        'parent_slug'   => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Надписи футера',
        'menu_title'    => 'Футер',
        'menu_slug'     => 'footer',
        'parent_slug'   => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Корзина',
        'menu_title'    => 'Корзина',
        'menu_slug'     => 'cart',
        'parent_slug'   => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Меню Кабинета',
        'menu_title'    => 'Меню Кабинета',
        'menu_slug'     => 'cabinet',
        'parent_slug'   => 'theme-general-settings',
    ));

}

function typical_title() { // функция вывода тайтла
    global $page, $paged; // переменные пагинации должны быть глобыльными
    wp_title('|', true, 'right'); // вывод стандартного заголовка с разделителем "|"
    //bloginfo('name'); // вывод названия сайта
    $site_description = get_bloginfo('description', 'display'); // получаем описание сайта
    if ($site_description && (is_home() || is_front_page())) //если описание сайта есть и мы на главной
        echo " | $site_description"; // выводим описание сайта с "|" разделителем
    if ($paged >= 2 || $page >= 2) // если пагинация была использована
        echo ' | '.sprintf(__( 'Страница %s'), max($paged, $page)); // покажем номер страницы с "|" разделителем
}

register_nav_menus(array( // Регистрируем 2 меню
    'top' => 'Верхнее', // Верхнее
    'bottom' => 'Внизу' // Внизу
));

add_theme_support('post-thumbnails'); // включаем поддержку миниатюр
set_post_thumbnail_size(250, 150); // задаем размер миниатюрам 250x150
add_image_size('big-thumb', 400, 400, true); // добавляем еще один размер картинкам 400x400 с обрезкой

register_sidebar(array( // регистрируем левую колонку, этот кусок можно повторять для добавления новых областей для виджитов
    'name' => 'Колонка слева', // Название в админке
    'id' => "left-sidebar", // идентификатор для вызова в шаблонах
    'description' => 'Обычная колонка в сайдбаре', // Описалово в админке
    'before_widget' => '<li id="%1$s" class="widget %2$s">', // разметка до вывода каждого виджета
    'after_widget' => "</li>\n", // разметка после вывода каждого виджета
    'before_title' => '<label class="widgettitle">', //  разметка до вывода заголовка виджета
    'after_title' => "</label>\n", //  разметка после вывода заголовка виджета
));

class clean_comments_constructor extends Walker_Comment { // класс, который собирает всю структуру комментов
    public function start_lvl( &$output, $depth = 0, $args = array()) { // что выводим перед дочерними комментариями
        $output .= '<ul class="children">' . "\n";
    }
    public function end_lvl( &$output, $depth = 0, $args = array()) { // что выводим после дочерних комментариев
        $output .= "</ul><!-- .children -->\n";
    }
    protected function comment( $comment, $depth, $args ) { // разметка каждого комментария, без закрывающего </li>!
        $classes = implode(' ', get_comment_class()).($comment->comment_author_email == get_the_author_meta('email') ? ' author-comment' : ''); // берем стандартные классы комментария и если коммент пренадлежит автору поста добавляем класс author-comment
        echo '<li id="li-comment-'.get_comment_ID().'" class="'.$classes.'">'."\n"; // родительский тэг комментария с классами выше и уникальным id
        echo '<div id="comment-'.get_comment_ID().'">'."\n"; // элемент с таким id нужен для якорных ссылок на коммент
        echo get_avatar($comment, 64)."\n"; // покажем аватар с размером 64х64
        echo '<p class="meta">Автор: '.get_comment_author()."\n"; // имя автора коммента
        echo ' '.get_comment_author_email(); // email автора коммента
        echo ' '.get_comment_author_url(); // url автора коммента
        echo ' Добавлено '.get_comment_date('F j, Y').' в '.get_comment_time()."\n"; // дата и время комментирования
        if ( '0' == $comment->comment_approved ) echo '<em class="comment-awaiting-moderation">Ваш комментарий будет опубликован после проверки модератором.</em>'."\n"; // если комментарий должен пройти проверку
        comment_text()."\n"; // текст коммента
        $reply_link_args = array( // опции ссылки "ответить"
            'depth' => $depth, // текущая вложенность
            'reply_text' => 'Ответить', // текст
            'login_text' => 'Вы должны быть залогинены' // текст если юзер должен залогинеться
        );
        echo get_comment_reply_link(array_merge($args, $reply_link_args)); // выводим ссылку ответить
        echo '</div>'."\n"; // закрываем див
    }
    public function end_el( &$output, $comment, $depth = 0, $args = array() ) { // конец каждого коммента
        $output .= "</li><!-- #comment-## -->\n";
    }
}

function pagination() { // функция вывода пагинации
    global $wp_query; // текущая выборка должна быть глобальной
    $big = 999999999; // число для замены
    echo paginate_links(array( // вывод пагинации с опциями ниже
        'base' => str_replace($big,'%#%',esc_url(get_pagenum_link($big))), // что заменяем в формате ниже
        'format' => '?paged=%#%', // формат, %#% будет заменено
        'current' => max(1, get_query_var('paged')), // текущая страница, 1, если $_GET['page'] не определено
        'type' => 'list', // ссылки в ul
        'prev_text'    => '<i class="icon-right-arrow"></i>', // текст назад
        'next_text'    => '<i class="icon-right-arrow"></i>', // текст вперед
        'total' => $wp_query->max_num_pages, // общие кол-во страниц в пагинации
        'show_all'     => false, // не показывать ссылки на все страницы, иначе end_size и mid_size будут проигнорированны
        'end_size'     => 15, //  сколько страниц показать в начале и конце списка (12 ... 4 ... 89)
        'mid_size'     => 15, // сколько страниц показать вокруг текущей страницы (... 123 5 678 ...).
        'add_args'     => false, // массив GET параметров для добавления в ссылку страницы
        'add_fragment' => '',   // строка для добавления в конец ссылки на страницу
        'before_page_number' => '', // строка перед цифрой
        'after_page_number' => '' // строка после цифры
    ));
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
//function add_last($items,$args) {
//    $items[count($items)]->classes[] = 'mod-discard';
//    return $items;
//}
//add_filter('wp_nav_menu_objects', 'add_last');

add_filter( 'woocommerce_currencies', 'add_my_currency' );

function add_my_currency( $currencies ) {

    $currencies['UAH'] = __( 'Українська гривня', 'woocommerce' );

    return $currencies;

}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {

    switch( $currency ) {

        case 'UAH': $currency_symbol = 'грн'; break;

    }

    return $currency_symbol;

}
/**
 * woocommerce_single_product_summary hook.
 *
 * @hooked woocommerce_template_single_title - 5
 * @hooked woocommerce_template_single_rating - 10
 * @hooked woocommerce_template_single_price - 10
 * @hooked woocommerce_template_single_excerpt - 20
 * @hooked woocommerce_template_single_add_to_cart - 30
 * @hooked woocommerce_template_single_meta - 40
 * @hooked woocommerce_template_single_sharing - 50
 * @hooked WC_Structured_Data::generate_product_data() - 60
 */

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );


remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 60 );

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 5);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 20);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 30);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 50);


add_filter('woocommerce_product_description_heading',
    'sam_product_description_heading');

function sam_product_description_heading() {
    return '';
}


// Remove each style one by one
add_filter( 'woocommerce_enqueue_styles', 'jk_dequeue_styles' );
function jk_dequeue_styles( $enqueue_styles ) {
    unset( $enqueue_styles['woocommerce-general'] );    // Remove the gloss
    unset( $enqueue_styles['woocommerce-layout'] );     // Remove the layout
    unset( $enqueue_styles['woocommerce-smallscreen'] );    // Remove the smallscreen optimisation
    return $enqueue_styles;
}

// Or just remove them all in one line
add_filter( 'woocommerce_enqueue_styles', '__return_false' );

if ( ! function_exists( 'woocommerce_template_loop_product_title' ) ) {
    /**
     * Show the product title in the product loop. By default this is an H2.
     */
    function woocommerce_template_loop_product_title() {
        echo get_the_title();
    }
}


/**
 * Insert the opening anchor tag for products in the loop.
 */
// remove the hook to the default function
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );

//remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

add_action ( 'woocommerce_before_shop_loop_item', 'my_function', 10 );
function my_function() {
    echo '<a href="' . get_the_permalink() . '" class="item-title">';
}

add_filter( 'woocommerce_get_price_html', 'wpa83368_price_html', 100, 2 );

function wpa83368_price_html( $price,$product ){
    // return $product->price;
    if ( $product->price > 0 ) {
        if ($product->is_on_sale()) {
            $from = $product->regular_price;
            $to = $product->sale_price;
            return '<span class="new">'.( ( is_numeric( $to ) ) ? woocommerce_price( $to ) : $to ) .'</span> <span class="old">'. ( ( is_numeric( $from ) ) ? woocommerce_price( $from ) : $from ) .'</span>';
        } else {
            $to = $product->regular_price;
            return  ( ( is_numeric( $to ) ) ? woocommerce_price( $to ) : $to );
        }
    } else {
        return 0;
    }
}

add_action( 'woocommerce_archive_description', 'woocommerce_category_image', 2 );
function woocommerce_category_image() {
    if ( is_product_category() ){
        global $wp_query;
        $cat = $wp_query->get_queried_object();
        $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
        $image = wp_get_attachment_url( $thumbnail_id );
        if ( $image ) {
            echo '<img src="' . $image . '" alt="' . $cat->name . '" />';
        }
    }
}


function my_acf_google_map_api( $api ){

    $api['key'] = 'AIzaSyBV7j8tYBO_DEWmvaNQC929_qEaRxFGNVo';

    return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

// WooCommerce Checkout Fields Hook
add_filter('woocommerce_checkout_fields','custom_wc_checkout_fields_no_label');

// Our hooked in function - $fields is passed via the filter!
// Action: remove label from $fields
function custom_wc_checkout_fields_no_label($fields) {
    // loop by category
    foreach ($fields as $category => $value) {
        // loop by fields
        foreach ($fields[$category] as $field => $property) {
            // remove label property
            unset($fields[$category][$field]['label']);
        }
    }
    return $fields;
}

//// Hook in
//add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );
//
//// Our hooked in function - $address_fields is passed via the filter!
//function custom_override_default_address_fields( $address_fields ) {
//    $address_fields['address_1']['required'] = false;
//
//    return $address_fields;
//}

add_action('woocommerce_checkout_process', 'misha_check_if_selected');

function misha_check_if_selected() {

    // you can add any custom validations here
    if ( empty( $_POST['billing_first_name'] ) ) {
        wc_add_notice('', 'error');
    }
    if ( empty( $_POST['billing_address_1'] ) ) {
        wc_add_notice('', 'error');
    }
    if ( empty( $_POST['billing_phone'] ) ) {
        wc_add_notice('', 'error');
    }
    if ( empty( $_POST['billing_email'] ) ) {
        wc_add_notice('', 'error');
    }
    if ( empty( $_POST['hero'] ) || ($_POST['hero'] == '1') ) {
        wc_add_notice('', 'error');
    }
    if ( empty( $_POST['payment'] ) || ($_POST['payment'] == '1') ) {
        wc_add_notice('', 'error');
    }
    if ( empty( $_POST['terms-field'] ) || ($_POST['terms-field'] == '0')) {
        wc_add_notice('', 'error');
    }

}

add_filter( 'woocommerce_checkout_fields' , 'x_custom_override_default_address_fields', 100 );
function x_custom_override_default_address_fields( $fields ) {
    $fields['billing']['billing_phone']['required']    = 1;
    return $fields;
}

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fieldsss' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fieldsss( $fields ) {
    $fields['billing']['billing_phone']['placeholder'] = get_field( "контактный_телефон", 'option' );
    $fields['billing']['billing_first_name']['placeholder'] = get_field( "фио", 'option' );
    $fields['billing']['billing_address_1']['placeholder'] = get_field( "город_населенный_пункт", 'option' );
    $fields['billing']['billing_email']['placeholder'] = 'email';
    $fields['order']['order_comments']['placeholder'] = get_field( "комментарий_к_заказу", 'option' );

    return $fields;
}


add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {

    unset($fields['billing']['billing_last_name']);
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_city']);
    unset($fields['billing']['billing_postcode']);
 //   unset($fields['billing']['billing_country']);
    unset($fields['billing']['billing_state']);
    unset($fields['shipping']['shipping_last_name']);


    return $fields;
}


//// hide coupon field on checkout page
//function hide_coupon_field_on_checkout( $enabled ) {
//    if ( is_checkout() ) {
//        $enabled = false;
//    }
//    return $enabled;
//}
//add_filter( 'woocommerce_coupons_enabled', 'hide_coupon_field_on_checkout' );


//remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20,0);
/*
** Отключение вкладок на странице товара
*/

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

unset( $tabs['additional_information'] ); // Убираем вкладку "Свойства"
unset( $tabs['reviews'] );          // Remove the reviews tab

return $tabs;
}
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

add_filter( 'woocommerce_product_tabs', 'sb_woo_new_test_tab');


function sb_woo_new_test_tab($tabs) {
    global $product;
    $names = "<span>({$product->get_review_count()})</span>";
    if (ICL_LANGUAGE_CODE == "ua") {
        $name_otziv = 'Відгуки';
    } else {
        $name_otziv = 'Отзывы';
    }
    $tabs['test_tab'] = array(
        'title' => $name_otziv.$names,
        'priority' => 50,
        'callback' => 'comments_template'
    );

    return $tabs;
}

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */

function do_excerpt($string, $word_limit) {
    $words = explode(' ', $string, ($word_limit + 1));
    if (count($words) > $word_limit)
        array_pop($words);
    echo implode(' ', $words).' ...';
}

function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


add_filter('woocommerce_short_description', 'reigel_woocommerce_short_description', 10, 1);
function reigel_woocommerce_short_description($post_excerpt){
    if (is_product()) {
        $pieces = explode(" ", $post_excerpt);
        $post_excerpt = implode(" ", array_splice($pieces, 0, 36));
    }
    return $post_excerpt;
}


function my_custom_my_account_menu_items( $items ) {
    $dannie = get_field( "мои_данные", 'option' );
    $lubim_tovaru = get_field( "любимые_товары", 'option' );
    $history_tovaru = get_field( "история_покупок", 'option' );
    $detali_profila = get_field( "детали_профиля", 'option' );
    $otziv = get_field( "оставьте_отзыв", 'option' );
    $items = array(
        'dashboard' => $dannie,
        'my-likes' =>$lubim_tovaru,
        'orders' => $history_tovaru,
        'edit-account' => $detali_profila,
        'my-otzivi' => $otziv
    );

    return $items;
}

add_filter( 'woocommerce_account_menu_items', 'my_custom_my_account_menu_items' );


add_action( 'init', 'add_endpoint' );
function add_endpoint(){
    add_rewrite_endpoint( 'my-likes', EP_ROOT | EP_PAGES );
}


add_filter( 'wc_get_template', 'custom_endpoint', 10, 5 );
function custom_endpoint($located, $template_name, $args, $template_path, $default_path){

    if( $template_name == 'myaccount/my-account.php' ){
        global $wp_query;
        if(isset($wp_query->query['my-likes'])){
            $located = get_template_directory() . '/my-likes.php';
        }
    }

    return $located;
}

add_action( 'init', 'add_endpointsik' );
function add_endpointsik(){
    add_rewrite_endpoint( 'my-otzivi', EP_ROOT | EP_PAGES );
}


add_filter( 'wc_get_template', 'custom_endpointik', 10, 5 );
function custom_endpointik($located, $template_name, $args, $template_path, $default_path){

    if( $template_name == 'myaccount/my-account.php' ){
        global $wp_query;
        if(isset($wp_query->query['my-otzivi'])){
            $located = get_template_directory() . '/otzivi_kab.php';
        }
    }

    return $located;
}

add_action('init', 'my_custom_init');
function my_custom_init(){
    register_post_type('comment', array(
        'labels'             => array(
            'name'               => 'Комментарии пользователей', // Основное название типа записи
            'singular_name'      => 'Комментарии пользователей', // отдельное название записи типа Book
            'add_new'            => 'Добавить новий',
            'add_new_item'       => 'Добавить новий комментарий',
            'edit_item'          => 'Редактировать комментарий',
            'new_item'           => 'Новий комментарий',
            'view_item'          => 'Посмотреть комментарий',
            'search_items'       => 'Найти комментарий',
            'not_found'          =>  'Комментариев не найдено',
            'not_found_in_trash' => 'В корзине комментариев не найдено',
            'parent_item_colon'  => '',
            'menu_name'          => 'Комментарии пользователей'

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 4,
        'menu_icon'          => 'dashicons-format-chat',
        'supports'           => array('title','editor','author','thumbnail','excerpt')
    ) );
}


add_action('init', 'my_custom_init_sale');
function my_custom_init_sale(){
    register_post_type('sale', array(
        'labels'             => array(
            'name'               => 'Акции', // Основное название типа записи
            'singular_name'      => 'Акции', // отдельное название записи типа Book
            'add_new'            => 'Добавить акцию',
            'add_new_item'       => 'Добавить акцию',
            'edit_item'          => 'Редактировать акцию',
            'new_item'           => 'Добавить акцию',
            'view_item'          => 'Посмотреть акцию',
            'search_items'       => 'Найти акцию',
            'not_found'          =>  'Акции не найдено',
            'not_found_in_trash' => 'В корзине акции не найдено',
            'parent_item_colon'  => '',
            'menu_name'          => 'Акции'

        ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'menu_icon'          => 'dashicons-tickets',
        'supports'           => array('title','editor','author','thumbnail','excerpt')
    ) );
}

add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment( $fragments ) {
    global $woocommerce;

    ob_start();

    ?>
    <span class="cart-count"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
    <?php

    $fragments['span.cart-count'] = ob_get_clean();

    return $fragments;

}

add_filter( 'wc_add_to_cart_message', 'custom_add_to_cart_message' );
function custom_add_to_cart_message() {
    global $woocommerce;

    $return_to  = get_permalink(woocommerce_get_page_id('shop'));
    $message    = "Товар добавлен в корзину";
    return $message;
}


// ajax post

function ajax_filter_posts_scripts()
{
    // Enqueue script
    wp_register_script('afp_script', get_stylesheet_directory_uri() . '/js/load-ajax.js', false, null, false);
    wp_enqueue_script('afp_script');

    wp_localize_script('afp_script', 'afp_vars', array(
            'afp_nonce' => wp_create_nonce('afp_nonce'), // Create nonce which we later will use to verify AJAX request
            'afp_ajax_url' => admin_url('admin-ajax.php'),
        )
    );
}

add_action('wp_enqueue_scripts', 'ajax_filter_posts_scripts', 100);


/**
 * @param $limit
 */
function comm($limit){
    $p_id = ( $_POST['spd'] );
    $limit =( $_POST['limit'] );
    $args = array(
        'orderby' => 'date',
        'order' => 'DESC',
        'number' => 2,
        'post_type' => 'product',
        'offset' => $limit,
        'post_id' => $p_id
    );
    $comments_query = new WP_Comment_Query;
    $comments = $comments_query->query( $args );

    foreach ($comments as $comment){
        wp_list_comments( woocommerce_comments( $comment ));
    }
    die();
}
add_action('wp_ajax_mycomm', 'comm');
add_action('wp_ajax_nopriv_mycomm', 'comm');

function scrapeImage($text) {
    $pattern = '/src=[\'"]?([^\'" >]+)[\'" >]/';
    preg_match($pattern, $text, $link);
    $link = $link[1];
    $link = urldecode($link);
    return $link;

}

//remove_action( 'load-update-core.php', 'wp_update_plugins' );
//add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
//wp_clear_scheduled_hook( 'wp_update_plugins' );
//add_filter('pre_site_transient_update_core',create_function('$a', "return null;"));
//wp_clear_scheduled_hook('wp_version_check');
//remove_action('load-update-core.php','wp_update_themes');
//add_filter('pre_site_transient_update_themes',create_function('$a', "return null;"));
//wp_clear_scheduled_hook('wp_update_themes');

add_filter('woocommerce_create_account_default_checked' , function ($checked){
    return true;
});

add_action( 'template_redirect', 'wc_custom_redirect_after_purchase' );
function wc_custom_redirect_after_purchase() {
    global $wp;
    $domain = get_home_url();
    if ( is_checkout() && ! empty( $wp->query_vars['order-received'] ) ) {
        if (ICL_LANGUAGE_CODE == "ua") {
            wp_redirect($domain . '/ua/podyaka/');
        } else {
            wp_redirect($domain . '/blagodarnost/');
        }
        exit;
    }
}


add_filter('woocommerce_email_from_name', 'new_mail_from_name');

function new_mail_from_name($old) {
    if (ICL_LANGUAGE_CODE == "ua") {
        return 'Інтернет-магазин HALYKOO';
    } else {
        return 'Интернет-магазин HALYKOO';
    }

}


function filter_woocommerce_email_headingorder( $email_heading , $email ) {
    if (ICL_LANGUAGE_CODE == "ua") {
        $email_headings = 'Ваше замовлення переведено в статус "Оплачено"';
    }else{
        $email_headings = 'Ваш заказ переведен в статус "Оплачено"';
    }


    return $email_headings;
};
add_filter( "woocommerce_email_heading_customer_completed_order", 'filter_woocommerce_email_headingorder', 10, 2 );

function filter_woocommerce_email_headingordernew( $email_heading , $email ) {
    if (ICL_LANGUAGE_CODE == "ua") {
        $email_headingsiks = 'Нове клієнтське замовлення';
    }else{
        $email_headingsiks = 'Новый клиентский заказ';
    }


    return $email_headingsiks;
};
add_filter( "woocommerce_email_heading_new_order", 'filter_woocommerce_email_headingordernew', 10, 2 );

function filter_woocommerce_email_headingordercancel( $email_heading , $email ) {
    if (ICL_LANGUAGE_CODE == "ua") {
        $email_headingsik = 'Замовлення в статусі "Закрито"';
    }else{
        $email_headingsik = 'Заказ в статусе "Закрыто"';
    }


    return $email_headingsik;
};
add_filter( "woocommerce_email_heading_cancelled_order", 'filter_woocommerce_email_headingordercancel', 10, 2 );


add_filter('woocommerce_email_subject_new_order', 'change_admin_email_subject', 1, 2);

function change_admin_email_subject( $subject, $order ) {
    global $woocommerce;

    if (ICL_LANGUAGE_CODE == "ua") {
        $emailhead = 'Нове замовлення';
    }else{
        $emailhead = 'Новый заказ';
    }


    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    // Change subject according to your requirement
    $subject = sprintf( '[%s]  %s (%s) — %s', $blogname, $emailhead, $order->id, date_i18n( wc_date_format(), strtotime( $order->order_date ) ) );


    return $subject;
}

add_filter('woocommerce_email_subject_customer_processing_order', 'change_admin_email_subject_order', 1, 2);

function change_admin_email_subject_order( $subjects, $order ) {
    global $woocommerce;

    if (ICL_LANGUAGE_CODE == "ua") {
        $emailheads = 'Квитанція вашого замовлення на';
        $emailneed = 'від';
    }else{
        $emailheads = 'Квитанция вашего заказа на';
        $emailneed = 'от';
    }


    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    // Change subject according to your requirement
    $subjects = sprintf( '%s %s %s %s', $emailheads, $blogname, $emailneed, date_i18n( wc_date_format(), strtotime( $order->order_date ) ) );


    return $subjects;
}



add_filter('woocommerce_email_subject_customer_completed_order', 'change_admin_email_subject_completed_order', 1, 2);

function change_admin_email_subject_completed_order( $subjectsi, $order ) {
    global $woocommerce;

    if (ICL_LANGUAGE_CODE == "ua") {
        $emailheadsi = 'Ваше замовлення на';
        $emaildone = 'в статусі "Оплачено"';
        $emailneedi = 'від';
    }else{
        $emailheadsi = 'Ваш заказ на';
        $emaildone = 'в статусе "Оплачено"';
        $emailneedi = 'от';
    }


    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    // Change subject according to your requirement
    $subjectsi = sprintf( '%s %s %s %s %s', $emailheadsi, $blogname, $emailneedi, date_i18n( wc_date_format(), strtotime( $order->order_date ) ), $emaildone );


    return $subjectsi;
}

function filter_woocommerce_email_headingss( $email_heading , $email ) {
    if (ICL_LANGUAGE_CODE == "ua") {
        $email_heading = 'Ласкаво просимо на HALYKOO';
    }else{
        $email_heading = 'Добро пожаловать на HALYKOO';
    }


    return $email_heading;
};
add_filter('woocommerce_email_subject_customer_new_account', 'filter_woocommerce_email_headingss', 1, 2);

// add the filter
add_filter( "woocommerce_email_heading_customer_new_account", 'filter_woocommerce_email_headingss', 10, 2 );


function filter_woocommerce_email_heading( $email_heading , $email ) {
    if (ICL_LANGUAGE_CODE == "ua") {
        $email_heading = 'Дякуємо за Ваше замовлення';
    }else{
        $email_heading = 'Спасибо за ваш заказ';
    }


    return $email_heading;
};

// add the filter
/*add_filter( "woocommerce_email_heading_customer_new_account", 'filter_woocommerce_email_heading', 10, 2 ); */
add_filter( "woocommerce_email_heading_customer_processing_order", 'filter_woocommerce_email_heading', 10, 2 );

function footer_enqueue_scripts(){
    remove_action('wp_head','wp_print_scripts');
    remove_action('wp_head','wp_print_head_scripts',9);
    remove_action('wp_head','wp_enqueue_scripts',1);
    add_action('wp_footer','wp_print_scripts',5);
    add_action('wp_footer','wp_enqueue_scripts',5);
    add_action('wp_footer','wp_print_head_scripts',5);
}
add_action('after_setup_theme','footer_enqueue_scripts');


/**
 * Обновляем метаданные заказа со значением поля
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['otdilen_nova_poshta'] ) ) {
        if (ICL_LANGUAGE_CODE == "ua") {
            $otdilenie_poshta = 'Відділення нової пошти';
        }else{
            $otdilenie_poshta = 'Отделение новой почты';
        }
        update_post_meta( $order_id, $otdilenie_poshta, sanitize_text_field( $_POST['otdilen_nova_poshta'] ) );
    }
}

/**
 * Выводим значение поля на странице редактирования заказа
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    if (ICL_LANGUAGE_CODE == "ua") {
        $otdilenie_poshta = 'Відділення нової пошти';
    }else{
        $otdilenie_poshta = 'Отделение новой почты';
    }
    echo '<p><strong>'.__($otdilenie_poshta).':</strong> ' . get_post_meta( $order->id, $otdilenie_poshta, true ) . '</p>';
}


// add_action( 'woocommerce_email_after_order_table', 'order_custom_field_in_item_meta_end', 10, 4 );
// function order_custom_field_in_item_meta_end( $order ) {
//     if (ICL_LANGUAGE_CODE == "ua") {
//         $otdilenie_poshta = 'Відділення нової пошти';
//     }else{
//         $otdilenie_poshta = 'Отделение новой почты';
//     }
//     $_text_field = get_post_meta( $order->id, $otdilenie_poshta, true );
//     if (! empty($_text_field)) {
//         echo '<br/>' . $otdilenie_poshta . ': ' . $_text_field;
//     }
// }
//
//


//add_filter('wp_nav_menu_items','add_todaysdate_in_menu', 10, 2);
//function add_todaysdate_in_menu( $items, $args ) {
//
//
//
//    return $items;
//}




function my_nav_wrap() {
    // default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'

    // open the <ul>, set 'menu_class' and 'menu_id' values
    $wrap  = '<ul id="%1$s" class="%2$s">';
if (ICL_LANGUAGE_CODE == 'ua') {
    $hot_line = '<span class="txt1">гаряча</span><span class="txt2">лінія</span>';
} else {
    $hot_line = '<span class="txt1">горячая</span><span class="txt2">линия</span>';
}
    // the static link
    $wrap .= '<li class="my-static-link hides"><label>
'.get_field("page_phone", "option").'</label><a href="#" class="el-btn mod-call">
                          <span>
                            <i class="icon-phones"></i>
                          </span>
                            <span>
                          '.$hot_line.'
                          </span>
                            <span class="number">
                            '.get_field("namber_phonee", "option").'
                          </span>
                        </a></li>';
    // get nav items as configured in /wp-admin/
    $wrap .= '%3$s';



    // close the <ul>
    $wrap .= '</ul>';
    // return the result
    return $wrap;
}

add_filter( 'wp_nav_menu_items', 'add_search_to_nav', 10, 2 );

function add_search_to_nav( $items, $args )
{
    if( $args->theme_location == 'top' ) {
        $items .= '<li class="hides"><div class="tools-row">
<form class="dgwt-wcas-search-form search_menu" role="search" action="' . get_home_url() . '" method="get">
<input type="search" id="dgwt-wcas-search" class="dgwt-wcas-search-input" name="s" value="" placeholder="Введите слово для поиска...">
                       <button type="submit" class="dgwt-wcas-search-submit">
                            <i class="icon-lup"></i>
                        </button>   
                        </form>
                    </div></li>';
        ob_start();
        do_action('icl_language_selector');
        $contents = ob_get_contents();
        ob_end_clean();

        $items .= '<li class="hides"><div class="tools-row langs"><div class="text_before">Выберите язык</div>' . $contents . '</div></li>';

        if (is_user_logged_in()) {
            $user = wp_get_current_user();
            $avtarimg = scrapeImage(get_avatar($user->ID, 34));
            if ($user) {
                $avtarimgsi = $avtarimg;
            } else {
                $avtarimgsi = get_template_directory_uri() . '/img/cab.png';
            }
            $items .= '<li class="hides"><div class="tools-row"><a href="' . get_permalink(get_option("woocommerce_myaccount_page_id")) . '" class="tools-ava">
<img src="' . $avtarimgsi . '" alt=""/>
</a></div></li>';
            $items .= '<li class="hides"><div class="tools-row"><a href="' . wp_logout_url(home_url()) . '" class="tools-logout">
' . get_field("global_exit", 'option') . '
</a></div></li>';
        } else {
            $items .= '<li class="hides borders"><div class="tools-row"><div class="text_before">Вход в личный кабинет</div><a href="' . get_permalink(get_option('woocommerce_myaccount_page_id')) . '" class="tools-logout">
' . get_field("global_in", 'option') . ' >>
</a></div></li>';
        }
        return $items;
    }
}



add_action( 'woocommerce_checkout_order_processed', 'custom_email_notification', 10, 1 );
function custom_email_notification( $order_id ) {

    if ( ! $order_id ) return;

    ## THE ORDER DATA ##

    // Get an instance of the WC_Order object
    $order = wc_get_order( $order_id );

    // Targetting Order status 'pending' or 'on-hold'
    if( $order->has_status( 'pending' ) ) {

       // Getting all WC_emails objects
        $wc_email_notifications = WC()->mailer()->get_emails();

        // New Email notification (admin)
        $email_object = $wc_email_notifications['WC_Email_New_Order'];

        // Customizing Heading, subject, recipients, email type …
        $email_object->settings = array(
            'enabled' => 'yes',
            'recipient' => 'order@halykoo.store', // Set here the recipients emails (separated by a coma)
            'subject' => '[{site_title}] Новый зака ({order_number}) - {order_date}',
            'heading' => 'Новый заказ',
            'email_type' => 'html'
        );

        // Sending the email
        $email_object->trigger( $order_id );
    }
}