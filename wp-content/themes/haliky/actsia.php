<?php
/**
 * Страница архивов записей (archive.php)
 * @package WordPress
 * @subpackage your-clean-template
 * Template Name: Акции
 */
get_header(); // подключаем header.php ?>
    <div id="page-body" class="page-body article-wrapper">
    <div class="page-heading">
        <div class="page-heading-title mod-black">
            <?php the_title() /* заголовок */ ?>
        </div>
    </div>
    <div class="article-list">
    <div class="container">
    <div class="row">

<?php
//if (ICL_LANGUAGE_CODE == "ua") {
//    $cats = 39;
//} else {
//    $cats = 38;
//}
       $args = array(
           'post_type' => 'sale',
           'order' => 'DESC',
           'posts_per_page' => get_option('posts_per_page')
       );
       $query = new WP_Query( $args );
   while ($query->have_posts()) : $query->the_post();  // launching a series of blog bypass materials ?>
       <!-- item -->
       <div class="article clear-fix sales">
           <div class="article-inner ">
               <div class="col-xs-12 col-md-6 js-bg-cover article-pic" data-img="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"></div>
               <div class="col-xs-12 col-md-6 article-info">
                   <a href="<?php the_permalink(); ?>" class="article-title">
                       <?php the_title(); ?>
                   </a>
                   <div class="article-caption">
                       <?php //the_content(''); // пост превью, до more ?>
                       <?php the_excerpt(); ?>
                   </div>
                   <div class="article-bottom">
                       <div class="article-bottom-date">
                           <?php echo get_the_date('d.m.Y'); ?>
                       </div>
                       <a href="<?php the_permalink(); ?>" class="article-bottom-link">
                           <i class="icon-circular-down-arrow-button"></i>
                       </a>
                   </div>
               </div>
           </div>
       </div>
       <!-- end item -->
   <?php endwhile; // Complete the cycle.
wp_reset_query();
?>
    </div>
    </div>
    </div>

    <div class="container">
        <div class="pagination">
            <?php
            $links = paginate_links( array(
                'show_all'           => false,
                'end_size'           => 2,
                'mid_size'           => 2,
                'prev_next'          => false,
                'type'               => 'array'
            ) );

            $links = preg_replace('@\<span([^>]*)>(.*?)\<\/span>@i', '<a href="#" $1>$2', $links);

            if ( $links ) :

                echo '<ul>';

                // $prev_posts_link = str_replace('</span>', '', $prev_posts_link);
                // get_previous_posts_link will return a string or void if no link is set.
                if ( $prev_posts_link = get_previous_posts_link( __( 'Previous Page' ) ) ) :
                    $prev_posts_link = str_replace("Previous Page", "<i class=\"icon-right-arrow\"></i>", $prev_posts_link);
                    echo '<li class="pagination-back">';
                    echo $prev_posts_link;
                    echo '</li>';
                endif;

                echo '<li>';

                echo join( '</li><li>', $links );
                echo '</li>';

                // get_next_posts_link will return a string or void if no link is set.
                if ( $next_posts_link = get_next_posts_link( __( 'Next Page' ) ) ) :
                    $next_posts_link = str_replace("Next Page", "<i class=\"icon-right-arrow\"></i>", $next_posts_link);
                    echo '<li class="pagination-next">';
                    echo $next_posts_link;
                    echo '</li>';
                endif;
                echo '</ul>';
            endif; ?>
        </div>
    </div>
<?php  get_footer(); // подключаем footer.php ?>