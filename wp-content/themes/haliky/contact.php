<?php
/*
Template Name: Контакты
*/
get_header();
if (ICL_LANGUAGE_CODE == "ua") {
    $hot_line = "гаряча";
    $hot_line_hot = "лінія";
    $free_cals_ukrain = "Безкоштовні дзвінки по Україні";
    $telephone_cont = "ТЕЛЕФОНИ:";
    $adress_cont = "АДРЕСА:";
    $regim_robotu_cont = "РЕЖИМ РОБОТИ:";
    $napischite_nam_cont = "Напишіть нам:";
} else {
    $hot_line = "горячая";
    $hot_line_hot = "линия";
    $free_cals_ukrain = "Бесплатные звонки по Украине";
    $telephone_cont = "ТЕЛЕФОНЫ:";
    $adress_cont = "АДРЕС:";
    $regim_robotu_cont = "РЕЖИМ РАБОТЫ:";
    $napischite_nam_cont = "Напишите нам:";
}

?>
<div id="page-body" class="page-body contacts-wrapper">
    <div class="contacts">
        <div class="contacts-inner mod-form js-bg-cover" data-img="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/thanx.jpg">
            <div class="contacts-inner-callbtn">
                <a href="#" class="el-btn mod-call">
            <span>
              <i class="icon-phones"></i>
            </span>
                    <span>
              <span class="txt1"><?php echo $hot_line; ?></span>
              <span class="txt2"><?php echo $hot_line_hot; ?></span>
            </span>
                    <span class="number"><?php $value = get_field( "cals" ); echo $value;?></span>
                </a>
                <label>
                    <?php echo $free_cals_ukrain; ?>
                </label>
            </div>

            <div class="contacts-list">
                <div class="contacts-item">
                    <div class="contacts-item-ico">
                        <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/ci1.png" alt="">
                    </div>
                    <label>
                        <?php echo $telephone_cont; ?>
                    </label>
                    <?php if( have_rows('phons') ): ?>

                        <ul>

                            <?php while( have_rows('phons') ): the_row();
                                // vars
                                $phone = get_sub_field('phone');
                            ?>
                                <li>
                                    <?php echo $phone; ?>
                                </li>

                            <?php endwhile; ?>

                        </ul>

                    <?php endif; ?>

                </div>
                <div class="contacts-item">
                    <div class="contacts-item-ico">
                        <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/ci2.png" alt="">
                    </div>
                    <label>
                        <?php echo $adress_cont; ?>
                    </label>
                    <?php if( have_rows('adressik') ): ?>

                        <ul>

                            <?php while( have_rows('adressik') ): the_row();
                                // vars
                                $aress = get_sub_field('adressik_kk');
                                ?>
                                <li>
                                    <?php echo $aress; ?>
                                </li>

                            <?php endwhile; ?>

                        </ul>

                    <?php endif; ?>
                </div>
                <div class="contacts-item">
                    <div class="contacts-item-ico">
                        <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/ci3.png" alt="">
                    </div>
                    <label>
                        EMAIL:
                    </label>
                    <?php if( have_rows('emails') ): ?>

                        <ul>

                            <?php while( have_rows('emails') ): the_row();
                                // vars
                                $mails = get_sub_field('email');
                                ?>
                                <li>
                                    <a href="mailto:<?php echo $mails; ?>"><?php echo $mails; ?></a>
                                </li>

                            <?php endwhile; ?>

                        </ul>

                    <?php endif; ?>
                </div>
            </div>

            <div class="contacts-schedule">
                <div class="contacts-schedule-h">
                    <?php echo $regim_robotu_cont; ?>
                </div>
                <div>
                    <?php if( have_rows('rabota_regim') ): ?>

                        <?php while( have_rows('rabota_regim') ): the_row();
                                // vars
                                $dni = get_sub_field('dni_rabotii');
                                $time = get_sub_field('rabota_time');
                                ?>
                            <div class="contacts-schedule-item">
                                <label><?php echo $dni; ?></label>
                                <?php echo $time; ?>
                            </div>
                        <?php endwhile; ?>

                     <?php endif; ?>

                </div>
            </div>

            <div class="contacts-form">
                <div class="contacts-form-h">
                    <?php echo $napischite_nam_cont; ?>
                </div>
                <?php if ( ICL_LANGUAGE_CODE=='ua' ) {
                    echo do_shortcode('[contact-form-7 id="334" title="Контактная форма 1_copy"]');
                } else {
                    echo do_shortcode('[contact-form-7 id="48" title="Контактная форма 1"]');
                } ?>
            </div>

        </div>
        <div class="contacts-inner" id="hide_map">
            <div id="contacts-map" class="contacts-map"></div>
        </div>
    </div>
</div>
<?php get_footer();?>
