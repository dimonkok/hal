<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?>
<div id="page-body" class="page-body single-wrapper">
    <div class="container">

      <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
          <?php $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');  ?>
          <div class="single-pic">
              <img src="<?php echo $featured_img_url; ?>" alt="">
          </div>
          <div class="single-date">
              <?php the_date('d.m.Y'); ?>
          </div>
          <div class="single-prev ">
              <h1>
                  <?php the_title(); // заголовок поста ?>
              </h1>
		<div class="single-content ctext">
              		<?php $descriptionss = get_field( "discriptionnn" ); echo $descriptionss; ?>
		</div>
              <?php if (ICL_LANGUAGE_CODE == "ua") {
                $go_ty_vatalog = "Перейти в каталог";
                $previos_news = "Попередня новина";
                $next_news = "Наступна новина";
              } else {
                $go_ty_vatalog = "Перейти в каталог";
                $previos_news = "Предыдущая новость";
                $next_news = "Следующая новость";
              } ?>
          </div>
              <div class="single-content ctext">
                  <?php //the_content(); // контент ?>
                  <div class="text-center">
                    <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>" class="el-btn mod-grad mod-arr">
                      <?php echo $go_ty_vatalog; ?> 
                    </a>
                  </div>
              </div>
              <div class="single-content-after">
                  <?php $prev_post = get_previous_post();
                  if (!empty( $prev_post )): ?>
                    <a class="single-back" href="<?php echo esc_url( get_permalink( $prev_post->ID ) ); ?>">
                        <i class="icon-right-arrow"></i>
                        <span><?php echo $previos_news; ?></span>
                    </a>
                  <?php endif; ?>
                  <?php
                  $next_post = get_next_post();
                  if (!empty( $next_post )): ?>
                    <a class="single-forw" href="<?php echo esc_url( get_permalink( $next_post->ID ) ); ?>">
                      <span><?php echo $next_news; ?></span>
                      <i class="icon-right-arrow"></i>
                    </a>
                  <?php endif; ?>
              </div>
      <?php endwhile; // конец цикла ?>

    </div>
</div>
<?php get_footer(); // подключаем footer.php ?>
