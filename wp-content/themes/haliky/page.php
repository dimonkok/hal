<?php
/**
 * Шаблон обычной страницы (page.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?>
<?php if(is_cart()): ?>
    <div class="container cart-content">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
     <?php
    if (ICL_LANGUAGE_CODE == "ua") {
        $breack_glav = 'Головна';
        $breack_cart = 'Кошик';
    } else {
        $breack_glav = 'Главная';
        $breack_cart = 'Корзина';
    }
    ?>
    <div class="page-heading">
    <div class="page-heading-title">
        <?php echo $breack_cart; ?>
    </div>
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="<?php echo get_home_url(); ?>">
                <?php echo $breack_glav; ?>
                </a>
            </li>
            <li>
                <a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>">
                    Каталог
                </a>
            </li>
            <li>
              <span>
                <?php echo $breack_cart; ?>
              </span>
            </li>
        </ul>
    </div>
</div>

		    <?php the_content(); // контент ?>

<?php endwhile; // конец цикла ?>

    </div>
    <?php if( is_shop() ) : ?>
        <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>

                <h1><?php the_title(); // заголовок ?></h1>
                <?php the_content(); // контент ?>

        <?php endwhile; // конец цикла ?>

        <?php endif; ?>
    <?php endif; ?>
<?php if (is_account_page()) : ?>
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
        <div id="page-body" class="page-body cabinet-wrapper usercab-wrap">
            <?php the_content(); ?>
        </div>
    <?php endwhile; // конец цикла ?>
<?php endif; ?>

<?php get_footer(); // подключаем footer.php ?>
