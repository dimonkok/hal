<?php
/**
 * Template Name: подяка
 *
 */
 get_header();?>
<script>
  fbq('track', 'Purchase');
</script>
<?php if (ICL_LANGUAGE_CODE == "ua") : ?>
<div id="page-body" class="page-body thanx-wrapper js-bg-cover" data-img="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/thanx.jpg">
    <div class="container text-center">
        <div class="thanx-logo">
            <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/thanx.png" alt="">
        </div>
        <div class="thanx-h">
            ДЯКУЄМО ЗА ПОКУПКУ
        </div>
        <span>Вся інформація стосовно Вашого замовлення надіслана Вам на пошту.</span>
        <span>Наші менеджери найближчим часом зв'яжуться з Вами.</span>
        <span>Халіку бажає здоров'я вашому малюку</span>
        <div class="thanx-btn">
            <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/thanx-heart.png" alt="">
            <a href="/" class="">Повернутися в магазин</a>
        </div>
    </div>
</div>
<?php else : ?>

    <div id="page-body" class="page-body thanx-wrapper js-bg-cover" data-img="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/thanx.jpg">
        <div class="container text-center">
            <div class="thanx-logo">
                <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/thanx.png" alt="">
            </div>
            <div class="thanx-h">
                СПАСИБО ЗА ПОКУПКУ
            </div>
            <span>Вся информация касательно Вашего заказа отправлена Вам на почту.</span>
            <span>Наши менеджеры в ближайшее время свяжутся с Вами.</span>
            <span>Халику желает здоровья вашему малышу</span>
            <div class="thanx-btn">
                <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/thanx-heart.png" alt="">
                <a href="/" class="">Вернуться в магазин</a>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php get_footer();?>
