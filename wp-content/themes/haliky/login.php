<?php
/*
Template Name: Login
*/
$path = $_SERVER['DOCUMENT_ROOT'];
$site_url = $_SERVER['SERVER_NAME'];
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
if($_POST) {

    global $wpdb;

    //We shall SQL escape all inputs
    $username = $wpdb->escape($_REQUEST['username']);
    $password = $wpdb->escape($_REQUEST['password']);
    $remember = $wpdb->escape($_REQUEST['rememberme']);

    if($remember) $remember = "true";
    else $remember = "false";

    $login_data = array();
    $login_data['user_login'] = $username;
    $login_data['user_password'] = $password;
    $login_data['remember'] = $remember;

    $user_verify = wp_signon( $login_data, false );

    if ( is_wp_error($user_verify) )
    {
        $error = $user_verify->get_error_message();
        $out = json_encode(["success" => false, "error" => $error]);
    } else {

        $out = json_encode(["success" => true]);

    }

    echo $out;

}

?>
