<?php
/**
 * Template Name: подяка укр
 *
 */
get_header();?>

    <div id="page-body" class="page-body thanx-wrapper js-bg-cover" data-img="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/thanx.jpg">
        <div class="container text-center">
            <div class="thanx-logo">
                <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/thanx.png" alt="">
            </div>
            <div class="thanx-h">
                ДЯКУЄМО ЗА ПОКУПКУ
            </div>
            <span>Вся інформація стосовно Вашого замовлення надіслана Вам на пошту.</span>
            <span>Наші менеджери найближчим часом зв'яжуться з Вами.</span>
            <span>Халіку бажає здоров'я вашому малюку</span>
            <div class="thanx-btn">
                <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/thanx-heart.png" alt="">
                <a href="/" class="">Повернутися в магазин</a>
            </div>
        </div>
    </div>

<?php get_footer();?>
