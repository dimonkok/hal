<?php
if ( ! defined( 'ABSPATH' ) ) {
exit; // Exit if accessed directly
}
global $wpdb;
global $woocommerce ;
global $order;
global $product;
do_action( 'woocommerce_before_account_navigation' );
?>
    <div class="container ">
        <div class="page-heading">
            <div class="page-heading-title">
                <?php echo get_field( "мой_кабинет", 'option' ); ?>
            </div>
            <div class="cabinet-menu-wrap">
                <div class="cabinet-menu">
                    <ul>
                        <?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
                            <li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?>">
                                <a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
                            </li>
                        <?php endforeach; ?>
                        <li><a href="<?php echo wp_logout_url(home_url()); ?>" title="Logout"><?php echo get_field( "выход", 'option' ); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
<?php do_action( 'woocommerce_after_account_navigation' ); ?>

<script>
    var ajaxurls = "<?php echo get_template_directory_uri(); ?>/delete.php";
</script>
<div class="cabcontent">
    <div class="container">
        <div class="cabcontent-title">
            <div class="cabcontent-title-h">
                <?php echo get_field( "любимые_товары", 'option' ); ?>
            </div>
            <div class="cabcontent-title-txt">
                <?php $user = wp_get_current_user(); ?>
                <b> <?php echo $user->user_nicename; ?>!</b>
                <br>
                <?php echo get_field( "block_opisanies", 'option' ); ?>
            </div>
        </div>
        <?php
        $user_id = get_current_user_id();
        $product_id = $wpdb->get_results('SELECT `product_id` FROM `likes` WHERE `user_id` ='.$user_id, ARRAY_A);

        if (!empty($product_id)):
        $var_product = array();
        foreach ($product_id as $key => $value) {
            $var_product[] = $value['product_id'];
        }
    $args = array(
        'post__in' => $var_product,
        'post_type' => 'product',
        'posts_per_page' => 2
    );
    $loop = new WP_Query( $args );
    if ( $loop->have_posts() ): while ( $loop->have_posts() ): $loop->the_post(); ?>
        <?php global $product; ?>
        <!-- fav -->
        <div class="fav">
            <table class="cart-table">
                <tr class="js-basketRow">
                    <td class="fav-pic">
                        <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="">
                    </td>
                    <td class="fav-info">
                        <?php//  wc_get_template( 'single-product/rating.php' ); ?>
                        <?php
                        $idss = $product->get_id();
                        $count = $wpdb->get_var("SELECT COUNT(*) FROM `rating` WHERE `product_id` = 65");
                        $result = $wpdb->get_results('SELECT sum(rate) as result_value FROM `rating` WHERE `product_id` = 65');
                        $end_result = $result[0]->result_value;
                        $ratess = $end_result/$count;
                        ?>
                        <script>
                            var tovar_ids = "<?php echo $idss; ?>";
                        </script>
                                <div class="rate_row fav-rate rate">
                                    <?php
                                    for($i=1; $i <= 5; $i++) {
                                        ((int) $ratess >= $i) ? $active="active" : $active="";
                                        echo '<span class="rate_star '.$active.'" data-value="'.$i.'"></span>';
                                    }
                                    ?>
                                </div>
                        <div class="fav-item">
                            <?php the_title(); echo "&nbsp;".get_field( "texts", $product->get_id() ); ?>
                        </div>
                        <ul class="fav-stat">
                            <?php global $product; ?>
                            <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

                                <li><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></li>

                            <?php endif; ?>

                            <?php echo wc_get_product_category_list( $product->get_id(), ', ', '<li>' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</li>' ); ?>

                            <?php
                            $inst = $product->list_attributes();
                            ?>

                            <?php echo wc_get_product_tag_list( $product->get_id(), ', ', '<li>' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</li>' ); ?>

                        </ul>

                    </td>
                    <td class="fav-price">
                        <p>
                            <?php echo get_field( "tchena_v_cabinete", 'option' ); ?>:
                        </p>
                        <div class="fav-price-num">
                           <?php $price = $product->get_price_html();
//                           $price = str_replace("%body%", "black", $price);
                           $price = preg_replace('@\<span([^>]*)>(.*?)\<\/span>@i', '$2', $price);
                           echo $price;
                           ?>
                        </div>
                    </td>
                    <td>
                        <div class="numinput">
                            <input type="number" size="3" name="num" min="1" max="999" value="1">
                        </div>
                    </td>
                    <td class="fav-buy">
                        <a href="#" data-product_id="<?php echo $product->get_id(); ?>" class="el-btn mod-grad tocart add_to_cart_button ajax_add_to_cart btn btn-hg hg-product-add-to-cart-button">
                <span>
                  <i class="icon-shopping-cart"></i>
                </span>
                      <span><?php echo get_field( "v_corzinu_but", 'option' ); ?></span>
                        </a>
                    </td>
                    <td class="fav-close">
                        <a href="#" data-id="<?php echo $product->get_id(); ?>" class="js-killrow cart-table-delete">
                            <i class="icon-delete"></i>
                        </a>
                    </td>
                </tr>

            </table>
        </div>
        <!-- end fav -->

  <?php
    endwhile; endif; wp_reset_postdata(); else :
?>
<div class="cabcontent-title">
<p><?php echo get_field( "пока_нет_товаров", 'option' ); ?></p>
</div>
<?php endif; ?>
</div>

