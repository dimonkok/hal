<?php
/**
 * Template Name: Доставка и оплата
 *
 */
get_header();?>
    <div id="page-body" class="page-body single-wrapper">
        <div class="container">
            <div class="single-prev "><h1><?php// the_title(); ?></h1></div>
            <div class="single-content ctext">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
<?php get_footer();?>