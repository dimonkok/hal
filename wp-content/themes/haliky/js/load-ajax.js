jQuery(document).ready(function($) {
    $('.seecomments').click( function(e) {
        e.preventDefault();
        var limit = parseInt($(this).attr('data-limit'));
        var conut = parseInt($(this).attr('data-count'));
        var send_post_id = parseInt($(this).attr('data-id'));
        data_ajax(limit, send_post_id);
        limit = limit + 2;
        $(this).attr('data-limit', limit);
        conut = conut - 2;
        $(this).attr('data-count', conut);
    });

    function data_ajax(limit, send_post_id){
        data = {
            action: 'mycomm',
            spd: send_post_id,
            afp_nonce: afp_vars.afp_nonce,
            limit: limit
        };

        $.post(
            afp_vars.afp_ajax_url,
            data,
            function(response) {
                console.info(response);
                if( response ) {
                    $('.commentlist li:last-child').after(response);
                    if (parseInt($('.seecomments').attr('data-count')) <= 0) {
                        $('.seecomments').fadeOut();
                    }
                }
            })
    }
});
