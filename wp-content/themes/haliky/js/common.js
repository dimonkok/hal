'use strict';



// sticky footer
//-----------------------------------------------------------------------------------
if(!Modernizr.flexbox) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      noFlexboxStickyFooter = function() {
        $pageBody.height('auto');
        if($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
        } else {
          $pageWrapper.height('auto');
        }
      };
    $(window).on('load resize', noFlexboxStickyFooter);
  })();
}
if(ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
  (function(){
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      ieFlexboxFix = function() {
        if($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageWrapper.height($(window).height());
          $pageBody.removeClass('flex-none');
        } else {
          $pageWrapper.height('auto');
        }
      };
    ieFlexboxFix();
    $(window).on('load resize', ieFlexboxFix);
  })();
}

// placeholder
//-----------------------------------------------------------------------------------
$(function() {
  $('input[placeholder], textarea[placeholder]').placeholder();
});

// fixed svg show
//-----------------------------------------------------------------------------
var baseUrl = window.location.protocol + '//' + window.location.host + window.location.pathname + window.location.search;
function fixedSvg() {
  $('use').filter(function () {
    return ( $(this).attr("xlink:href").indexOf("#") > -1);
  }).each(function () {
    $(this).attr("xlink:href", baseUrl + $(this).attr("xlink:href").split('/').slice(-1)[0]);
  });
}

fixedSvg();

// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
  return ($(selector).length) ? $(selector) : false;
}

// stretch bg's
//-----------------------------------------------------------------------------------


function setMaxHeight(selector) {
	selector.css("height", 'auto');
	var heightsArray = [];
	var maxHeight;
	selector.each(function() {
		heightsArray.push($(this).outerHeight());
	});

	maxHeight = Math.floor(Math.max.apply(null, heightsArray));

	selector.each(function() {
		$(this).height(maxHeight);
	});

}

$(window).on("load debounce", function(){

  $(".tools").css({
    "top" :  $("header").outerHeight() + 20 + "px",
    "opacity": 1
  });

  if (!$(".notfound-wrapper").length) {
      $(".page-wrapper").css("margin-top", $("header").outerHeight() + "px");
  } else {
    $(".page-wrapper").css({"margin-top": '0', "padding-top": '0'});
  }


  setMaxHeight($(".contacts-item-ico, .itemlist .item-title"));

  setMaxHeight($(".catitem"));
  if ($(window).width() >= 992) {

    setMaxHeight($(".goods-slide-half"));

    setTimeout(function(){
      $('.about-slide-half').css("height", 'auto');
      setMaxHeight($(".about-slide-half"));
    }, 100);

  } else {
    $(' .goods-slide-half, .about-slide-half ').css("height", 'auto');
  }

  if ($(window).width() >= 768) {
    setMaxHeight($('.features-label-title'));
    setMaxHeight($('.features-label-descr'));
    setMaxHeight($(".doods-tile-title"));
    setMaxHeight($(".item-title"));
    setMaxHeight($(".cat-article-title"));
    //setMaxHeight($('.goods-tile'));
  } else {
    $('.goods-tile, .item-title, .cat-article-title, .features-label-title, .features-label-descr, .doods-tile-title, .itemlist .item-title').css("height", 'auto');
  }

  // if ($(window).width() < 992 && $(window).width() >= 768) {
  //   setMaxHeight($('.goods-tile'));
  // } else {
  //   $('.goods-tile').css("height", 'auto');
  // }

});

$(".goods-slider").slick({
  adaptiveHeight: true
});

$(".js-bg-cover").each(function(){
	var img = $(this).attr("data-img");
	if (img) {
		$(this).css("background", "url(" + img + ")")
			.css("background-repeat", "no-repeat")
			.css("background-position", "center")
			.css("background-size", "cover");
	}
});

$(".js-bg-contain").each(function(){
	var img = $(this).attr("data-img");
	if (img) {
		$(this).css("background-image", "url(" + img + ")")
			.css("background-repeat", "no-repeat")
			.css("background-position", "center")
			.css("background-size", "contain");
	}
});



$(".about-slide-rotator").slick({
  arrows: false,
  infinite: true
});

$(".action-slider").slick({
  arrows: true,
  dots: true
});



$(".about-slider-btn").on("click", function(e){
  e.preventDefault();
  if ($(this).hasClass("next")) {
    $(".about-slide-rotator").slick('slickNext');
  } else {
    $(".about-slide-rotator").slick('slickPrev');
  }
});

$(".xpander-title").on("click",function(){
  $(this).closest($('.xpander')).toggleClass("active");
});

setTimeout(function() {
  $('select, input[type="checkbox"], input[type="number"]').styler();
}, 100);

$(".js-h-menutrigger").on("click", function(e){
  e.preventDefault();
  $(this).toggleClass("active");
  $(".header-menu").toggleClass("active");
});

$(".js-scrolldown").on('click', function(e){
  e.preventDefault();
  var target = $(this).attr("href");

  $('html, body').animate({
      scrollTop: $(target).offset().top - $('header').outerHeight()
  }, 300);
});

$(".orderform select").on("change", function(){
  $(this).closest(".jq-selectbox").addClass("mod-changed");
})

$(".js-killrow").on("click", function(e){
  e.preventDefault();
  $(this).closest("tr").remove();
});


$(window).scroll(function() {
    if ($(this).scrollTop() >= $('header').outerHeight()) {
        $('header').addClass("mod-scrolled");
    } else {
      $('header').removeClass("mod-scrolled");
    }
});


$('.js-onlynum').on('keypress', function(ev) {
    var keyCode = window.event ? ev.keyCode : ev.which;
    //codes for 0-9
    if (keyCode < 48 || keyCode > 57) {
        //codes for backspace, delete, enter
        if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
            ev.preventDefault();
        }
    }
});

$(".js-cahngeNum").on("keyup", function(){

  var res = $(this).val() * $(this).attr("data-price");
  $(this).closest(".js-basketRow").find(".js-result").text(res);
});

$( "#orderform, #sendfeed-form" ).validate({
  errorPlacement: function(error, element) {
    //$(element).closest(".jq-checkbox").addClass("error")
  },
  highlight: function(element) {
    $(element).closest('.jq-checkbox').addClass('error');
    $(element).addClass('error');
  }
});



$(".tovar-slider-main").slick({
    asNavFor: '.tovar-slider-nav',
    arrows: false,
    infinite: false
  }
);

$(".js-cat-articles-inner-slider").slick({
    arrows: true,
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
       {
         breakpoint: 992,
         settings: {
           slidesToShow: 3
         }
       },
       {
         breakpoint: 768,
         settings: {
           slidesToShow: 2
         }
       },
       {
         breakpoint: 480,
         settings: {
           slidesToShow: 1
         }
       }
     ]
  }
);

$(".tovar-slider-nav").slick({
    asNavFor: '.tovar-slider-main',
    vertical: true,
    arrows: false,
    focusOnSelect: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: false,
    responsive: [
       {
         breakpoint: 768,
         settings: {
           vertical: false
         }
       }
     ]
  }
);

$(".rate").starwarsjs();

// tabs
if ($(".tovar-tabs").length) {
  $(".js-tab").on("click", function(e){
    e.preventDefault();
    var index = $(this).index();
    $(".js-tab.active, .tovar-tabs-tabcont.active").removeClass('active');
    $(this).addClass("active");
    $('.tovar-tabs-tabcont').eq(index).addClass("active");
  });
}

// search
$(".js-tools-search").on("click", function(e){
  e.preventDefault();
  $(".searchresults").toggleClass("active");
  $("body").toggleClass("mod-ov-hidden");
});


// contact map

var markersJson = [
  {
    "title": "Украина, Киев 1",
    "lat": -32.709,
    "lng": 150.919,
    "price": '13.444',
    "pic": 'img/mapmarker.png',
    "pic_logo": "img/maplogo.png",
    "addr": 'Ул. Крещатик, 11 офис, 101 корпус 1'
  },
  {
    "title": "Украина, Киев 2",
    "lat": -32.706,
    "lng": 150.918,
    "price": '16.444',
    "pic": 'img/mapmarker.png',
    "pic_logo": "img/maplogo.png",
    "addr": 'Ул. Крещатик, 121 офис, 101 корпус 2'
  }
];

// init map =================

function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("://") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

var map;
function initMap(){
  map = new google.maps.Map(document.getElementById('contacts-map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 5,
    disableDefaultUI: true
  });

  // map fit bounds ================
  var bounds = new google.maps.LatLngBounds();
  var infoWindow = new google.maps.InfoWindow();

  var markers = markersJson.map(function(marker, i) {
    var latLng = new google.maps.LatLng(marker.lat, marker.lng);
    bounds.extend(latLng);


    var full = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');

    var infoWindowContent =
      '<div class="infoWindow">'+
        '<div class="infoWindow-top">'+
            '<img src="' + full + "/" + marker.pic_logo + '"/>' +
            '<div class="infoWindow-txt">' +
              '<div class="infoWindow-txt-top">Халіку Україна</div>' +
              '<div class="infoWindow-txt-bot">Товари для здоров’я дітей</div>' +
            '</div>' +
        '</div>'+
        '<div class="infoWindow-bottom">'+
          '<div class="infoWindow-title">' + marker.title + '</div>'+
          '<div class="infoWindow-addr">' + marker.addr + '</div>'+
        '</div>'
      '</div>';


    var markerUrl = full  + "/" + marker.pic;

    var image = {
      url:  markerUrl,
      size: new google.maps.Size(58, 58),
    };

    var marker = new google.maps.Marker({
      position: latLng,
      icon: markerUrl,
      map: map
      // shape: shape
    });

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
          infoWindow.setContent(infoWindowContent);
          infoWindow.open(map, marker);
      }
    })(marker, i));
    return marker;
  });

  // map fit/center bounds ================
  map.setCenter(bounds.getCenter());
  map.fitBounds(bounds);

}

$(".table-row-delete").on("click", function(e){
  e.preventDefault();
  $(this).closest(".js-rowToDelete").remove();
});

if ($('#contacts-map').length) {
    initMap();
}

$(".js-add-to-fav").on("click", function(e){
  e.preventDefault();
  alert("Item id:" + $(this).attr("data-id") + " added to favourites");
  $(this).toggleClass("active");
});
