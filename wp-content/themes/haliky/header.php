<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); // вывод атрибутов языка ?>>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/favicon.ico"/>
    <link rel="stylesheet"
          href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/css/style.min.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/style.css"/>
     
    <!--[if lte IE 9]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/><![endif]-->

    <!--
      mobile define colors for elements of the browser

      <meta name="theme-color" content="HEX_COLOR">
      <meta name="msapplication-navbutton-color" content="HEX_COLOR">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="HEX_COLOR">

    -->

    <!--
      <link rel="shortcut icon" href="favicon.ico"/>
      <meta name="keywords" content=""/>
      <meta name="description" content=""/>
      <meta name="author" content="">
      <meta name="robots" content="index,follow">
      <meta name="copyright" content="">
    -->
    <meta charset="<?php bloginfo('charset'); // кодировка ?>">
    <?php /* RSS и всякое */ ?>
    <link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
    <link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
    <link rel="alternate" type="application/rss+xml" title="Comments RSS"
          href="<?php bloginfo('comments_rss2_url'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <?php if(is_cart()):
        if (ICL_LANGUAGE_CODE == "ua") {
            $breack_carts = 'Кошик';
        } else {
            $breack_carts = 'Корзина';
        }
        ?>
        <title><?php echo $breack_carts; ?></title>
        <?php elseif(is_shop()) :
        if (ICL_LANGUAGE_CODE == "ua") {
            $breack_catalogs = 'Каталог товарів';
        } else {
            $breack_catalogs = 'Каталог товаров';
        }
        ?>
        <title><?php echo $breack_catalogs; ?></title>
        <?php else : ?>
        <title><?php typical_title(); // выводи тайтл, функция лежит в function.php ?></title>
    <?php endif; ?>

    <?php wp_head(); // необходимо для работы плагинов и функционала ?>
    <!--    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBV7j8tYBO_DEWmvaNQC929_qEaRxFGNVo"></script>-->
    <?php
    if (ICL_LANGUAGE_CODE == "ua") {
        $page_id_contact = 168;
        $namesicks = get_field( "nadpis_karta");
        $podzagolovock = get_field( "подзаголовок_на_карте_товары_для_здоровья_детей");
        $titles_maps = get_field( "первый_адрес_украина_киев");
        $adr_podrob = get_field( "улица_ул_мазепы_16_офис_5");
        $lat_map = get_field( "cart")['lat'];
        $lng_map = get_field( "cart")['lng'];
    } else {
        $page_id_contact = 49;
        $namesicks = get_field( "nadpis_karta");
        $podzagolovock = get_field( "подзаголовок_на_карте_товары_для_здоровья_детей");
        $titles_maps = get_field( "первый_адрес_украина_киев");
        $adr_podrob = get_field( "улица_ул_мазепы_16_офис_5");
        $lat_map = get_field( "cart")['lat'];
        $lng_map = get_field( "cart")['lng'];
    }
     if( is_page( $page_id_contact ) ) :  ?>        
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBV7j8tYBO_DEWmvaNQC929_qEaRxFGNVo&v=3.exp&amp;sensor=false"></script>
    <?php endif; ?>    
 <script>
             var namesicks = "<?php echo $namesicks; ?>";
             var podzagolovock = "<?php echo $podzagolovock; ?>";
             var titles_maps = "<?php echo $titles_maps; ?>";
             var adr_podrob = "<?php echo $adr_podrob; ?>";
             var lat_map = "<?php echo $lat_map; ?>";
             var lng_map = "<?php echo $lng_map; ?>";
         </script>

    <?php
    global $woocommerce;
    $cart_url = $woocommerce->cart->get_cart_url();
    if (ICL_LANGUAGE_CODE == "ua") {
        $notise_add_to_cart = 'Товар доданий у кошик';
        $pereytu_v_korziny = 'Перейти в кошик?';
        $prodolgit_pokupki = 'Продовжити покупки';
        $besplatna_dostavka = 'Безкоштовна доставка';
    } else {
        $notise_add_to_cart = 'Товар добавлен в корзину';
        $pereytu_v_korziny = 'Перейти в корзину?';
        $prodolgit_pokupki = 'Продолжить покупки';
        $besplatna_dostavka = 'Бесплатная доставка';
    }
    ?>

    <div class="cart_add_popoap remodal">
        <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
        <div class="sendfeed-h">
            <?php echo $notise_add_to_cart; ?>
        </div>
        <p></p></br>
            <a href="#" id="closeremodal"><?php echo $prodolgit_pokupki; ?></a>
            <a href="<?php echo $cart_url; ?>"><?php echo $pereytu_v_korziny; ?></a>
    </div>

    <script>
        var messages_add_to_cart = "<?php echo $notise_add_to_cart; ?>";
        var test = "<?php echo get_template_directory_uri(); ?>/test.php";
        var validate_form_cart = "<?php echo get_template_directory_uri(); ?>/validate.php";
        var cart_qty_ajaxs = "<?php echo get_template_directory_uri(); ?>/qty.php";
        var comment_ajaxs = "<?php echo get_template_directory_uri(); ?>/coment_add.php";
        var besplat_dost = "<?php echo $besplatna_dostavka; ?>";
    </script>
    <?php if (is_user_logged_in()) : $user_id = get_current_user_id(); ?>
        <script>
            var comment_delete_ajaxs = "<?php echo get_template_directory_uri(); ?>/delete_comment.php";
            var login = "yes";
            var ajaxurls = "<?php echo get_template_directory_uri(); ?>/likes.php";
            var user_id = "<?php echo $user_id; ?>";
            var ajaxurlrate = "<?php echo get_template_directory_uri(); ?>/rating.php";
            var deleteRows = "<?php echo get_template_directory_uri(); ?>/delete.php";
        </script>
    <?php else : ?>
    <?php
    if (ICL_LANGUAGE_CODE == "ua") {
        $notise_user = 'Лише зареєстровані користувачі можуть додати товар в обрані';
        $notise_user_like = 'Лише зареєстровані користувачі можуть голосувати за продукт';
    } else {
        $notise_user = 'Только зарегистрированные пользователи могут добавлять товар в избранное';
        $notise_user_like = 'Только зарегистрированные пользователи могут голосовать за продукт';
    }
    ?>
        <script>
            var login = "no";
            var nitisess = "<?php echo $notise_user; ?>";
            var nitisess_дшлу = "<?php echo $notise_user_like; ?>";
        </script>
    <?php endif; ?>

<!-- Global Site Tag (gtag.js) - Google Analytics -->

<script>  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');  ga('create', 'UA-105990239-1', 'auto');  ga('send', 'pageview');</script>
<!-- Hotjar Tracking Code for http://halykoo.store -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:726116,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
</head>
<body <?php body_class(); // все классы для body ?>>
<?php
    if (ICL_LANGUAGE_CODE == "ua") { 
        $asotiation = "Ми входимо в асоціацію <br> неонатологів України";
    } else {
        $asotiation = "Мы входим в ассоциацию<br> неонатологов Украины";
    } ?>
<div id="callback"></div>
            <div id="callback_panel">
                <p class="text"><?php echo $asotiation; ?></p>
            </div>
<?php if (!is_404()) : ?>
<div id="page-wrapper" class="page-wrapper ">
    <svg xmlns="http://www.w3.org/2000/svg" style="width: 0; height: 0; float: left;">

        <symbol id="ico-close-modal" viewBox="0 0 17.59999999999991 17.600000000000364">
            <path d="M1238 5646L1245.91 5638L1238 5630 " fill-opacity="0" fill="#ffffff" stroke-dasharray="0"
                  stroke-linejoin="round" stroke-linecap="round" stroke-opacity="1" stroke-miterlimit="50"
                  stroke-width="1.6" transform="matrix(1,0,0,1,-1237.2,-5629.2)"></path>
            <path d="M1253.91 5630L1246 5638L1253.91 5646 " fill-opacity="0" fill="#ffffff" stroke-dasharray="0"
                  stroke-linejoin="round" stroke-linecap="round" stroke-opacity="1" stroke-miterlimit="50"
                  stroke-width="1.6" transform="matrix(1,0,0,1,-1237.2,-5629.2)"></path>
        </symbol>

        <symbol id="ripply-scott" viewBox="0 0 100 100">
            <circle id="ripple-shape" cx="1" cy="1" r="1"/>
        </symbol>

    </svg>

    <!-- <svg><use xlink:href="#ico-close-modal"></use></svg> -->

    <!-- searchresults -->
    <div class="searchresults">
        <div class="container">
            <?php echo do_shortcode('[wcas-search-form]'); ?>
        </div>
    </div>

    <header id="header" class="header">
        <a href="<?php echo get_home_url(); ?>" class="header-logo">
            <img src="<?php echo get_field("logotip_site", 'option'); ?>" alt="">
        </a>

        <div class="container">

            <div class="row">

                <div class="menu-trigger js-h-menutrigger">
                    <span></span>
                </div>
                <div>sdfgh</div>
                <?php $args = array( // опции для вывода верхнего меню, чтобы они работали, меню должно быть создано в админке
                    'theme_location' => 'top', // идентификатор меню, определен в register_nav_menus() в function.php
                    'container' => 'nav', // обертка списка
                    'container_class' => 'header-menu col-xs-12 col-md-8',
                    'items_wrap' => my_nav_wrap(),
                    'menu_class' => 'bottom-menu', // класс для ul
                    'menu_id' => 'bottom-nav', // id для ul
                   // 'walker'=> new True_Walker_Nav_Menu()
                );
                wp_nav_menu($args); // выводим верхнее меню
                ?>

                <div class="col-xs-12 col-md-4 header-right ">
                    <div class="header-right-call">
                        <label>
                            <?php
                            $value = get_field("page_phone", 'option');
                            echo $value;
                            ?>
                        </label>
                        <a href="#" class="el-btn mod-call">
                          <span>
                            <i class="icon-phones"></i>
                          </span>
                            <span>
                          <?php if (ICL_LANGUAGE_CODE == 'ua') : ?>
                              <span class="txt1">гаряча</span>
                              <span class="txt2">лінія</span>
                          <?php else : ?>
                              <span class="txt1">горячая</span>
                              <span class="txt2">линия</span>
                          <?php endif; ?>
                          </span>
                            <span class="number">
                            <?php echo get_field("namber_phonee", "option"); ?>
                          </span>
                        </a>
                        <a href="<?php echo wc_get_cart_url(); ?>" class="basket">
                            <span class="cart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                            <i class="icon-shopping-cart"></i>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </header>

    <div id="notise"></div>


    <div class="tools visible desctope">
        <div class="tools-row">
            <span class="dot"></span>
            <?php do_action('icl_language_selector'); ?>
        </div>
        <div class="tools-row">
            <span class="dot"></span>
            <a class="tools-search js-tools-search" href="#">
                <i class="icon-lup"></i>
            </a>
        </div>
        <?php if (is_user_logged_in()) : ?>
            <div class="tools-row">
                <span class="dot"></span>
                <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>" class="tools-ava">
                    <?php
                    $user = wp_get_current_user();
                    $avtarimg = scrapeImage(get_avatar($user->ID, 34));
                    if ($user) :
                        ?>
                        <img src="<?php echo $avtarimg ?>" alt=""/>
                    <?php else : ?>
                        <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/cab.png"
                             alt="">
                    <?php endif; ?>


                </a>
            </div>
            <div class="tools-row">
                <span class="dot"></span>
                <a href="<?php echo wp_logout_url(home_url()); ?>" class="tools-logout">
                    <?php echo get_field("global_exit", 'option'); ?> >>
                </a>
            </div>
        <?php else : ?>
            <div class="tools-row">
                <span class="dot"></span>
                <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                   class="tools-logout">
                    <?php echo get_field("global_in", 'option'); ?> >>
                </a>
            </div>
        <?php endif; ?>
    </div>
    <?php if (is_cart()): ?>
    <div id="page-body" class="page-body cart-wrapper">
        <?php endif; ?>
        <?php if (is_shop()): ?>
        <div id="page-body" class="page-body catalog-wrapper">
            <div class="container">
                <?php endif; ?>
                <?php if (is_product()) : ?>
                <div id="page-body" class="page-body tovar-wrapper">
                    <?php endif; ?>
                    <?php if (is_product_category()) : ?>
                    <div id="page-body" class="page-body cat-wrapper">
                        <?php endif; ?>
                        <?php endif; ?>
