<?php
/**
 * Страница 404 ошибки (404.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // Подключаем header.php ?>
    <div id="page-wrapper" class="page-wrapper notfound-wrapper">
        <div id="page-body" class="page-body  js-bg-cover" data-img="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/404bg.jpg">
            <div class="container text-center">
                <a href="/" class="notfound-logo">
                    <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/404logo.png" alt="">
                </a>
                <div class="notfound-pic">
                    <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/404pic.png" alt="">
                </div>
                <div class="notfound-title">
                    К сожалению,<br>
                    нам не удалось найти эту страницу
                </div>
                <div class="notfound-txt">
                    Не отчаивайтесь!<br>
                    Перейдите <a href="/">на главную</a> и попробуйте еще раз.
                </div>
                <div class="notfound-btn">
                    <a href="/" class="el-btn mod-main">ВЕРНУТЬСЯ НА ГЛАВНУЮ</a>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); // подключаем footer.php ?>
