<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$site_url = $_SERVER['SERVER_NAME'];
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);
global $wpdb;
include_once $path . '/wp-load.php';
if($_POST) {
    $out = json_encode(["success" => true]);
    $errors= array();
    $email_input = $_POST['billing_email'];
    if ( email_exists( $email_input ) && !is_user_logged_in() ) {
        if (ICL_LANGUAGE_CODE == "ua") {
            $error = "Користувача с такою e-mail адресою вже зареєстровано";
        } else {
            $error = "Пользователь с таким е-мейл уже зарегестрирован";
        }
        $out = json_encode(["success" => false, "error" => $error]);
    } else {
// you can add any custom validations here
    if (empty($_POST['billing_first_name'])) {
        $errors[] = 'Имя';
    }
    if (empty($_POST['billing_address_1'])) {
        $errors[] = 'Адрес';
    }
    if (empty($_POST['billing_phone'])) {
        $errors[] = 'Телефон';
    }
    if (empty($_POST['billing_email'])) {
        $errors[] = 'Email';
     }
    if (empty($_POST['hero']) || ($_POST['hero'] == '1')) {
        $errors[] = 'Способ доставки';
    }
    if (empty($_POST['payment']) || ($_POST['payment'] == '1')) {
         $errors[] = 'Способо оплаты';
    }
    if ( empty( $_POST['terms-field'] ) || ($_POST['terms-field'] == '0')) {
        $errors[] = 'Надо согласиться';
    }
    //var_dump($errors);
    if (!empty($errors)) {
        $error = "Заполните красные поля";
        $out = json_encode(["success" => false, "error" => $error]);
    }
    }
    echo $out;
}
?>