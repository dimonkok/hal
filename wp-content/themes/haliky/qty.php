<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$site_url = $_SERVER['SERVER_NAME'];
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);

include_once $path . '/wp-load.php';
global $woocommerce;

if($_POST) {
    // Set item key as the hash found in input.qty's name
    $cart_item_key = $_POST['hash'];

    $prod_id = $_POST['product_id'];
    $product = new WC_Product($prod_id);
    // Get the array of values owned by the product we're updating
    $threeball_product_values = WC()->cart->get_cart_item( $cart_item_key );

    // Get the quantity of the item in the cart
    $threeball_product_quantity = apply_filters( 'woocommerce_stock_amount_cart_item', apply_filters( 'woocommerce_stock_amount', preg_replace( "/[^0-9\.]/", '', filter_var($_POST['quantity'], FILTER_SANITIZE_NUMBER_INT)) ), $cart_item_key );

    // Update cart validation
    $passed_validation  = apply_filters( 'woocommerce_update_cart_validation', true, $cart_item_key, $threeball_product_values, $threeball_product_quantity );

    // Update the quantity of the item in the cart
    if ( $passed_validation ) {
       WC()->cart->set_quantity( $cart_item_key, $threeball_product_quantity, true );
    }
    $prise = $threeball_product_quantity * $product->price;
    echo wc_price($prise);

}