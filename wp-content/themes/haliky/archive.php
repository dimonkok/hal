<?php
/**
 * Страница архивов записей (archive.php)
 * @package WordPress
 * @subpackage your-clean-template
 * Template Name: Новости
 */
get_header(); // подключаем header.php
    $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1; // определяем текущую страницу блога
    $args = array(
    'posts_per_page' => get_option('posts_per_page'), // значение по умолчанию берётся из настроек, но вы можете использовать и собственное
    'paged'          => $current_page // текущая страница
    );
    query_posts( $args );

    $wp_query->is_archive = true;
    $wp_query->is_home = false;?>
<div id="page-body" class="page-body article-wrapper">
    <div class="page-heading">
        <div class="page-heading-title mod-black">
            <?php the_title() /* заголовок */ ?>
        </div>
    </div>
        <div class="article-list">
      <div class="container">
        <div class="row">
            <?php while(have_posts()): the_post(); ?>
                <!-- item -->
                <div class="article clear-fix">
                    <div class="article-inner ">
                        <div class="col-xs-12 col-md-6 js-bg-cover article-pic" data-img="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"></div>
                        <div class="col-xs-12 col-md-6 article-info">
                            <?php if( get_field('est_li_video') ): ?>
                            <!-- vid mark -->
                            <div class="article-inner-vid">
                                <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/vid.png" alt="">
                                <span>
                                    видео
                                </span>
                            </div>
                            <!-- end vid mark -->
                            <?php endif; ?>
                            <a href="<?php the_permalink(); ?>" class="article-title">
                                <?php the_title(); ?>
                            </a>
                            <div class="article-caption">
                                <?php //the_content(''); // пост превью, до more ?>
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="article-bottom">
                                <div class="article-bottom-date">
                                    <?php echo get_the_date('d.m.Y'); ?>
                                </div>
                                <a href="<?php the_permalink(); ?>" class="article-bottom-link">
                                    <i class="icon-circular-down-arrow-button"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end item -->
                <?php
            endwhile;?>
        </div>
      </div>
</div>

    <div class="container">
    <div class="pagination">
        <?php
        $links = paginate_links( array(
            'show_all'           => false,
            'end_size'           => 2,
            'mid_size'           => 2,
            'prev_next'          => false,
            'type'               => 'array'
        ) );

        $links = preg_replace('@\<span([^>]*)>(.*?)\<\/span>@i', '<a href="#" $1>$2', $links);

        if ( $links ) :

            echo '<ul>';

           // $prev_posts_link = str_replace('</span>', '', $prev_posts_link);
            // get_previous_posts_link will return a string or void if no link is set.
            if ( $prev_posts_link = get_previous_posts_link( __( 'Previous Page' ) ) ) :
                $prev_posts_link = str_replace("Previous Page", "<i class=\"icon-right-arrow\"></i>", $prev_posts_link);
                echo '<li class="pagination-back">';
                echo $prev_posts_link;
                echo '</li>';
            endif;

            echo '<li>';

            echo join( '</li><li>', $links );
            echo '</li>';

            // get_next_posts_link will return a string or void if no link is set.
            if ( $next_posts_link = get_next_posts_link( __( 'Next Page' ) ) ) :
                $next_posts_link = str_replace("Next Page", "<i class=\"icon-right-arrow\"></i>", $next_posts_link);
                echo '<li class="pagination-next">';
                echo $next_posts_link;
                echo '</li>';
            endif;
            echo '</ul>';
        endif; ?>
        <?php //pagination(); ?>
    </div>
    </div>
<?php  get_footer(); // подключаем footer.php ?>