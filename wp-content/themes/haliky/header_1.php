<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); // вывод атрибутов языка ?>>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


    <script src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/js/ie-detector.js"></script>
    <script src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/js/modernizr.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i" rel="stylesheet">
    <!--[if lte IE 9]><meta http-equiv="X-UA-Compatible" content="IE=edge"/><![endif]-->

    <!--
      mobile define colors for elements of the browser

      <meta name="theme-color" content="HEX_COLOR">
      <meta name="msapplication-navbutton-color" content="HEX_COLOR">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="HEX_COLOR">

    -->

    <!--
      <link rel="shortcut icon" href="favicon.ico"/>
      <meta name="keywords" content=""/>
      <meta name="description" content=""/>
      <meta name="author" content="">
      <meta name="robots" content="index,follow">
      <meta name="copyright" content="">
    -->
	<meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<title><?php typical_title(); // выводи тайтл, функция лежит в function.php ?></title>
	<?php wp_head(); // необходимо для работы плагинов и функционала ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/css//jquery.modal.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/css/style.min.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/style.css"/>
    <?php if (is_page('Контакты')): ?>
        <style type="text/css">
            .acf-map {
                width: 100%;
                height: 100%;
            }
            /* fixes potential theme css conflict */
            .acf-map img {
                max-width: inherit !important;
            }

        </style>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBV7j8tYBO_DEWmvaNQC929_qEaRxFGNVo"></script>
        <script type="text/javascript">
            (function($) {

                /*
                 *  new_map
                 *
                 *  This function will render a Google Map onto the selected jQuery element
                 *
                 *  @type	function
                 *  @date	8/11/2013
                 *  @since	4.3.0
                 *
                 *  @param	$el (jQuery element)
                 *  @return	n/a
                 */

                function new_map( $el ) {

                    // var
                    var $markers = $el.find('.marker');


                    // vars
                    var args = {
                        zoom		: 16,
                        center		: new google.maps.LatLng(0, 0),
                        mapTypeId	: google.maps.MapTypeId.ROADMAP
                    };


                    // create map
                    var map = new google.maps.Map( $el[0], args);


                    // add a markers reference
                    map.markers = [];


                    // add markers
                    $markers.each(function(){

                        add_marker( $(this), map );

                    });


                    // center map
                    center_map( map );


                    // return
                    return map;

                }

                /*
                 *  add_marker
                 *
                 *  This function will add a marker to the selected Google Map
                 *
                 *  @type	function
                 *  @date	8/11/2013
                 *  @since	4.3.0
                 *
                 *  @param	$marker (jQuery element)
                 *  @param	map (Google Map object)
                 *  @return	n/a
                 */

                function add_marker( $marker, map ) {

                    // var
                    var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

                    // create marker
                    var marker = new google.maps.Marker({
                        position	: latlng,
                        map			: map
                    });

                    // add to array
                    map.markers.push( marker );

                    // if marker contains HTML, add it to an infoWindow
                    if( $marker.html() )
                    {
                        // create info window
                        var infowindow = new google.maps.InfoWindow({
                            content		: $marker.html()
                        });

                        // show info window when marker is clicked
                        google.maps.event.addListener(marker, 'click', function() {

                            infowindow.open( map, marker );

                        });
                    }

                }

                /*
                 *  center_map
                 *
                 *  This function will center the map, showing all markers attached to this map
                 *
                 *  @type	function
                 *  @date	8/11/2013
                 *  @since	4.3.0
                 *
                 *  @param	map (Google Map object)
                 *  @return	n/a
                 */

                function center_map( map ) {

                    // vars
                    var bounds = new google.maps.LatLngBounds();

                    // loop through all markers and create bounds
                    $.each( map.markers, function( i, marker ){

                        var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

                        bounds.extend( latlng );

                    });

                    // only 1 marker?
                    if( map.markers.length == 1 )
                    {
                        // set center of map
                        map.setCenter( bounds.getCenter() );
                        map.setZoom( 16 );
                    }
                    else
                    {
                        // fit to bounds
                        map.fitBounds( bounds );
                    }

                }

                /*
                 *  document ready
                 *
                 *  This function will render each map when the document is ready (page has loaded)
                 *
                 *  @type	function
                 *  @date	8/11/2013
                 *  @since	5.0.0
                 *
                 *  @param	n/a
                 *  @return	n/a
                 */
// global var
                var map = null;

                $(document).ready(function(){

                    $('.acf-map').each(function(){

                        // create map
                        map = new_map( $(this) );

                    });

                });

            })(jQuery);
        </script>
    <?php endif; ?>
</head>
<body <?php body_class(); // все классы для body ?>>
<?php if(!is_404()) :?>
<div id="page-wrapper" class="page-wrapper ">
    <svg xmlns="http://www.w3.org/2000/svg" style="width: 0; height: 0; float: left;">

        <symbol id="ico-close-modal" viewBox="0 0 17.59999999999991 17.600000000000364">
            <path d="M1238 5646L1245.91 5638L1238 5630 " fill-opacity="0" fill="#ffffff" stroke-dasharray="0" stroke-linejoin="round" stroke-linecap="round" stroke-opacity="1" stroke-miterlimit="50" stroke-width="1.6" transform="matrix(1,0,0,1,-1237.2,-5629.2)"></path>
            <path d="M1253.91 5630L1246 5638L1253.91 5646 " fill-opacity="0" fill="#ffffff" stroke-dasharray="0" stroke-linejoin="round" stroke-linecap="round" stroke-opacity="1" stroke-miterlimit="50" stroke-width="1.6" transform="matrix(1,0,0,1,-1237.2,-5629.2)"></path>
        </symbol>

        <symbol id="ripply-scott" viewBox="0 0 100 100">
            <circle id="ripple-shape" cx="1" cy="1" r="1" />
        </symbol>

    </svg>

    <!-- <svg><use xlink:href="#ico-close-modal"></use></svg> -->

    <!-- searchresults -->
    <div class="searchresults">
        <div class="container">
           <?php echo do_shortcode('[wcas-search-form]');?>
        </div>
    </div>

    <header id="header" class="header">
        <a href="<?php echo get_home_url(); ?>" class="header-logo">
            <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/logo.png" alt="">
        </a>

        <div class="container">

            <div class="row">

                <div class="menu-trigger js-h-menutrigger">
                    <span></span>
                </div>
                    <?php $args = array( // опции для вывода верхнего меню, чтобы они работали, меню должно быть создано в админке
                        'theme_location' => 'top', // идентификатор меню, определен в register_nav_menus() в function.php
                        'container'=> 'nav', // обертка списка
                        'container_class' => 'header-menu col-xs-12 col-md-8',
                        'menu_class' => 'bottom-menu', // класс для ul
                        'menu_id' => 'bottom-nav', // id для ul
                    );
                    wp_nav_menu($args); // выводим верхнее меню
                    ?>
                <div class="col-xs-12 col-md-4 header-right ">
                    <div class="header-right-call">
                        <label>
                            <?php if ( ICL_LANGUAGE_CODE=='ua' ) {
                                $value = get_field( "page_phone_ua", 'option' );
                            } else {
                                $value = get_field( "page_phone", 'option' );
                            }
                            echo $value;
                            ?>
                        </label>
                        <a href="#" class="el-btn mod-call">
            <span>
              <i class="icon-phones"></i>
            </span>
            <span>
            <?php if ( ICL_LANGUAGE_CODE=='ua' ) : ?>
                <span class="txt1">гаряча</span>
                <span class="txt2">лінія</span>
            <?php else : ?>
                <span class="txt1">горячая</span>
                <span class="txt2">линия</span>
            <?php endif; ?>
            </span>
            <span class="number">
              <?php  $valueesss = get_field( "namber_phonee", 'option' );
                echo $valueesss;
              ?>
            </span>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </header>

    <div class="tools">
        <div class="tools-row">
            <span class="dot"></span>
            <a href="<?php echo wc_get_cart_url(); ?>" class="basket">
                <span><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                <i class="icon-shopping-cart"></i>
            </a>
        </div>
        <div class="tools-row">
            <span class="dot"></span>
            <?php do_action('icl_language_selector'); ?>
        </div>
        <div class="tools-row">
            <span class="dot"></span>
            <a class="tools-search js-tools-search" href="#">
                <i class="icon-lup"></i>
            </a>
        </div>
        <?php if(is_user_logged_in()) : ?>
        <div class="tools-row">
            <span class="dot"></span>
            <a href="#" class="tools-ava">
                <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/cab.png" alt="">
            </a>
        </div>
        <div class="tools-row">
            <span class="dot"></span>
            <a href="<?php echo wp_logout_url( home_url() ); ?>" class="tools-logout">
                выход >>
            </a>
        </div>
        <?php else : ?>
            <div class="tools-row">
                <span class="dot"></span>
                <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="tools-logout">
                    вход >>
                </a>
            </div>
        <?php endif; ?>
    </div>

    <?php if(is_cart()): ?>
        <div id="page-body" class="page-body cart-wrapper">
    <?php endif; ?>
    <?php if(is_shop()): ?>
        <div id="page-body" class="page-body catalog-wrapper">
    <div class="container">
    <?php endif; ?>
    <?php if(is_product()) : ?>
    <div id="page-body" class="page-body tovar-wrapper">
<?php endif; ?>
    <?php if(is_product_category()) : ?>
    <div id="page-body" class="page-body cat-wrapper">
<?php endif; ?>
    <?php endif; ?>
