<?php
/**
 * Template Name: аккаунт
 *
 */
get_header();?>

<div id="page-body" class="page-body usercab-wrap">
  <div class="container ">

    <div class="page-heading">
      <div class="page-heading-title">
        Мой кабинет
      </div>
      <div class="cabinet-menu-wrap">
        <div class="cabinet-menu">
          <ul>
            <li><a  href="#">Мои данные</a></li>
            <li class="active"><a  href="#">любимые товары </a></li>
            <li><a href="#">История покупок</a></li>
            <li><a href="#">Детали</a></li>
            <li><a href="#">профиля</a></li>
            <li><a href="#">Оставьте отзыв</a></li>
            <li><a href="#">Выход</a></li>
          </ul>
        </div>
      </div>

    </div>

  </div>


  <div class="cabcontent">

    <div class="container">
      <div class="cabcontent-title">
        <div class="cabcontent-title-h">
          Детали профиля
        </div>
      </div>

      <!-- form -->
      <form id="cabcontentform" class="cabcontentform">
        <div class="orderform">
          <div class="orderform-half col-xs-12 col-md-6 ">
            <div class="orderform-title">
              Личные данные
            </div>
            <div>
              <div class="form-row">
                <input required type="text" name="fio" value="Иванов Иван" placeholder="Ф.И.О.">
              </div>
              <div class="form-row">
                <input required type="text" name="address" value="" placeholder="ГОРОД / НАСЕЛЕННыЙ ПУНКТ">
              </div>
              <div class="form-row">
                <input required type="text" name="tel" value="" placeholder="контактный телефон">
              </div>
              <div class="form-row">
                <input required type="email" name="email" value="" placeholder="email">
              </div>

            </div>
            <div class="orderform-ava">
              <div class="orderform-ava-title">Аватар</div>
              <a href="#" class="userdata-inner-ava js-bg-contain" data-img="http://haliku.ga-alex.com/wp-content/uploads/2017/07/miniony_kartinki_na_rabochi_stol_1920x1080-3-150x150.jpg"></a>
              <div class="orderform-ava-actions">
                <a href="#">
                  <i class="icon-right-arrow"></i>Обновить фотографию
                </a>
                <a href="#">
                  <i class="icon-cross"></i>Удалить фотографию
                </a>
              </div>
            </div>
          </div>

          <div class="orderform-half col-xs-12 col-md-6 ">
            <div class="orderform-title">
              Адрес доставки
            </div>
            <div>
              <div class="form-row">
                <input required type="text" name="address-in" value="" placeholder="ГОРОД / НАСЕЛЕННыЙ ПУНКТ">
              </div>
              <div class="form-row">
                <input required type="text" name="street" value="" placeholder="Улица">
              </div>
              <div class="form-row">
                <input required type="text" name="house" value="" placeholder="Дом \ Корпус">
              </div>
              <div class="form-row">
                <input required type="text" name="flat" value="" placeholder="Квартира">
              </div>
              <div class="form-row">
                <input required type="text" name="aditional" value="" placeholder="дополнительная информация">
              </div>
            </div>
          </div>

        </div>




        <div class="orderform-btn text-center col-xs-12">
          <button role="submit" class="el-btn mod-grad">
            Сохранить
          </button>
        </div>

      </form>
      <!-- end form -->

    </div>

  </div>
</div>


<?php get_footer();?>
