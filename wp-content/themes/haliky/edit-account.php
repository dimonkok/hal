<?php
$path = $_SERVER['DOCUMENT_ROOT'];
$site_url = $_SERVER['SERVER_NAME'];
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);

include_once $path . '/wp-load.php';

if($_POST) {
    $user_id = get_current_user_id();
//var_dump($_POST);
    if ( isset( $_POST['account_first_name'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $user_id, 'first_name', sanitize_text_field( $_POST['account_first_name'] ) );
        $success = "Данные обновлены!!!";
        $out = json_encode(["success" => true, "success" => $success]);
    }
    if ( isset( $_POST['address'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $user_id, 'billing_address_1', sanitize_text_field( $_POST['address'] ) );
        $success = "Данные обновлены!!!";
        $out = json_encode(["success" => true, "success" => $success]);
    }
    if ( isset( $_POST['tel'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $user_id, 'billing_phone', sanitize_text_field( $_POST['tel'] ) );
        $success = "Данные обновлены!!!";
        $out = json_encode(["success" => true, "success" => $success]);
    }
    if ( isset( $_POST['account_email'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $user_id, 'billing_email', sanitize_text_field( $_POST['account_email'] ) );
        $success = "Данные обновлены!!!";
        $out = json_encode(["success" => true, "success" => $success]);
    }
    if ( isset( $_POST['address-in'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $user_id, 'shipping_city', sanitize_text_field( $_POST['address-in'] ) );
        $success = "Данные обновлены!!!";
        $out = json_encode(["success" => true, "success" => $success]);
    }
    if ( isset( $_POST['street'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $user_id, 'shipping_address_1', sanitize_text_field( $_POST['street'] ) );
        $success = "Данные обновлены!!!";
        $out = json_encode(["success" => true, "success" => $success]);
    }
    if ( isset( $_POST['dom'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $user_id, 'shipping_address_2', sanitize_text_field( $_POST['dom'] ) );
        $success = "Данные обновлены!!!";
        $out = json_encode(["success" => true, "success" => $success]);
    }
    if ( isset( $_POST['flat'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $user_id, 'shipping_state', sanitize_text_field( $_POST['flat'] ) );
        $success = "Данные обновлены!!!";
        $out = json_encode(["success" => true, "success" => $success]);
    }
    if ( isset( $_POST['dopinfo'] ) ) {
        // WooCommerce billing phone
        update_user_meta( $user_id, 'order_comments', sanitize_text_field( $_POST['dopinfo'] ) );
        $success = "Данные обновлены!!!";
        $out = json_encode(["success" => true, "success" => $success]);
    }
//password_current
//password_1
//password_2
    $user = wp_get_current_user();
        if ( isset( $_POST['password_current'] ) && !empty( $_POST['password_current'] ) ) {
            if ($user && wp_check_password($_POST['password_current'], $user->data->user_pass, $user->ID)) {
                if ( !empty( $_POST['password_1']) && !empty( $_POST['password_2'] )) {
                    if ( $_POST['password_1'] == $_POST['password_2']  ){
                        wp_set_password( $_POST['password_2'], $user_id );
                        $success = "Пароль обновлен!!!";
                        $out = json_encode(["success" => true, "success" => $success]);
                    } else {
                        echo 'nesowpalo';
                    }
                   // update_user_meta( $user_id, 'password_1', sanitize_text_field($_POST['password_1']) );
                } else {
                    $error = 'Введите новый пароль!';
                    $out = json_encode(["success" => false, "error" => $error]);
                }
            } else {
                $error = 'Неверный пароль!';
                $out = json_encode(["success" => false, "error" => $error]);
            }
        }

    echo $out;

}