<?php
/*
Template Name: Соглашение
*/
get_header(); ?>
<div id="page-body" class="page-body single-wrapper">
    <div class="container">
        <?php if (have_posts()) while (have_posts()) : the_post(); // старт цикла ?>
            <?php the_content(); ?>
        <?php endwhile; // конец цикла ?>
    </div>
</div>
<?php get_footer(); ?>
