<?php
/**
 * Главная страница (index.php)
 * @package WordPress
 * @subpackage your-clean-template
 * Template Name: Головна
 */
get_header(); // подключаем header.php

$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );

if ( ICL_LANGUAGE_CODE=='ua' ) {
    $page_idd = 226;
    $magazin_loc = get_field( "internet_magazin", $page_idd );
    $zagolovock = get_field( "zagolovok_2", $page_idd );
    $opisan = get_field( "opisaniess", $page_idd );
    $knopka = get_field( "buttom_nadpis", $page_idd );
    $towari = get_field( "товары_халику", $page_idd );
    $podzagolov = get_field( "подзаголовок", $page_idd );
    $bolshe_tovarow = get_field( "больше_товаров", $page_idd );
    $baners = get_field( "банер", $page_idd );
    $glavna_banersik = get_field( "главная_картинка_банера", $page_idd );
    $baner_otzivivvv = get_field( "банер_отзывов", $page_idd );
    $baner_uznst_bolshe = get_field( "баннер_узнать_больше", $page_idd );
$ssilka_uznst_bolshe = get_field( "ссылка_банера_узнать_больше", $page_idd );
    $garmoshka = get_field( "гармошка", $page_idd );
    $tms = get_field( "tm", $page_idd );
    $roditeli_otzuvu = get_field( "родители_о_халику", $page_idd );
    $otzivi_tm = get_field( "tms_roditeli", $page_idd );
    $golovna_kartinka_index = get_field( "kartinka_glavnaya_stranithsa", $page_idd );
} else {
    $page_idd = 202;
    $magazin_loc = get_field( "internet_magazin", $page_idd );
    $zagolovock = get_field( "zagolovok_2", $page_idd );
    $opisan = get_field( "opisaniess", $page_idd );
    $knopka = get_field( "buttom_nadpis", $page_idd );
    $towari = get_field( "товары_халику", $page_idd );
    $podzagolov = get_field( "подзаголовок", $page_idd );
    $bolshe_tovarow = get_field( "больше_товаров", $page_idd );
    $baners = get_field( "банер", $page_idd );
    $glavna_banersik = get_field( "главная_картинка_банера", $page_idd );
    $baner_otzivivvv = get_field( "банер_отзывов", $page_idd );
    $baner_uznst_bolshe = get_field( "баннер_узнать_больше", $page_idd );
$ssilka_uznst_bolshe = get_field( "ссылка_банера_узнать_больше", $page_idd );
    $garmoshka = get_field( "гармошка", $page_idd );
    $tms = get_field( "tm", $page_idd );
    $roditeli_otzuvu = get_field( "родители_о_халику", $page_idd );
    $otzivi_tm = get_field( "tms_roditeli", $page_idd );
    $golovna_kartinka_index = get_field( "kartinka_glavnaya_stranithsa", $page_idd );

}?>
        <div id="page-body" class="page-body">
            <section class="topblock">
                <div class="container">
                    <div class="row">
                        <div class="topblock-left col-xs-12 col-md-6 valign">
                        <img class="image_full" src="<?php echo get_field("мобильная_картинка_на_главной",$page_idd) ?>"/>
                            <h2>
                                <?php echo $magazin_loc; ?>
                            </h2>
                            <p>
                                <?php echo $zagolovock; ?>
                            </p>
                            <span>
                                <?php echo $opisan; ?>
                            </span>
                            <a href="<?php echo $shop_page_url; ?>" class="el-btn mod-grad mod-arr">
                                <?php echo $knopka; ?>
                            </a>
                        </div>
                        <div class="topblock-right col-xs-12 col-md-6 valign">
                            <img src="<?php echo $golovna_kartinka_index; ?>" alt="">
                        </div>
                    </div>
                </div>
                <a href="#categories" class="el-scrolldown js-scrolldown"></a>
            </section>

            <section id="categories" class="categories">
                <div class="container">
                    <div class="catlist row">
                        <?php
                        $taxonomy     = 'product_cat';
                        $orderby      = 'name';
                        $show_count   = 0;      // 1 for yes, 0 for no
                        $pad_counts   = 0;      // 1 for yes, 0 for no
                        $hierarchical = 1;      // 1 for yes, 0 for no
                        $title        = '';
                        $empty        = 0;
                        $exclude      = array(43,44);

                        $args = array(
                            'taxonomy'     => $taxonomy,
                            'orderby'      => $orderby,
                            'show_count'   => $show_count,
                            'pad_counts'   => $pad_counts,
                            'hierarchical' => $hierarchical,
                            'title_li'     => $title,
                            'hide_empty'   => $empty,
                            'exclude'      => $exclude,
                        );
                        global $wp_query, $woocommerce;
                        $all_categories = get_categories( $args );
                        foreach ($all_categories as $cat) :
                            if($cat->category_parent == 0) {
                                 //var_dump($cat);
                                $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
                                // get the image URL
                                $image = wp_get_attachment_url( $thumbnail_id );
                                // print the IMG HTML
                                echo '<a href="'.esc_url(get_category_link( $cat->term_id )).'" class="catitem js-bg-cover col-xs-12 col-sm-4" data-img="'.$image.'">';
                                echo '<div class="catitem-title">'.$cat->name.'</div>';
                                echo '<div class="catitem-descr" style="white-space: pre;">'.$cat->description.'</div></a>';
                                //echo '<img src="'.$image.'" alt="" width="762" height="365" />';
                                //  echo '<br /><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';
                            }
                        ?>
                        <?php endforeach; ?>

                    </div>
                </div>
            </section>

            <section class="goods">

                <div class="container">
                    <div class="article-heading">
                        <h2><?php echo $towari; ?><sup><?php echo $tms; ?></sup></h2>
                        <p>
                           <?php echo $podzagolov; ?>
                        </p>
                    </div>



                    <?php

                    $rows = get_field('repeater_tovar', $page_idd);
                    if($rows) :  ?>
                    <!-- slider -->
                    <div class="goods-slider">
                        <?php foreach($rows as $row) : ?>
                        <!-- slide -->
                        <div class="goods-slide">
                           <!--                            <div class="goods-slide-half col-xs-12 col-md-4">-->
<!--                                <div class="goods-front js-bg-cover" data-img="--><?php //echo get_template_directory_uri(); // абсолютный путь до темы ?><!--/img/romb-bg.jpg">-->
<!--                                    <h2>-->
<!--                                        <a href="--><?php //echo get_permalink($row[1]); ?><!--">-->
<!--                                            --><?php //echo get_the_title($row[1]); ?>
<!--                                        </a>-->
<!--                                    </h2>-->
<!--                                    <div class="goods-front-img">-->
<!--                                        <a href="--><?php //echo get_permalink($row[1]); ?><!--">-->
<!--                                            <img src="--><?php //echo get_the_post_thumbnail_url($row[1], 'full'); ?><!--" alt="">-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    <div class="item-price">-->
<!--                                    --><?php
//                                        $_product = wc_get_product( $row[1] );
//                                        echo $_product->get_price_html();
//                                    ?>
<!--                                    </div>-->
<!--                                    <a href="--><?php //echo get_bloginfo('url');?><!--/?add-to-cart=--><?php //echo $row[1];?><!--" data-quantity="1" data-product_id="--><?php //echo $row[1];?><!--" class="el-btn mod-transp button product_type_simple add_to_cart_button ajax_add_to_cart item-addbasket">-->
<!--                                        Купить-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </div>-->
                            <div class="goods-slide-half col-xs-12 col-md-12">
                                <div class="goods-tile col-xs-12 col-sm-6 col-md-6">
                                    <div class="goods-tile-img">
                                    	<a href="<?php echo get_permalink($row[2]); ?>">
                                        	<img src="<?php echo get_the_post_thumbnail_url($row[2], 'full'); ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="doods-tile-title">
                                    	<a href="<?php echo get_permalink($row[2]); ?>">
                                        	<?php echo get_the_title($row[2]); ?>
                                        </a>
                                    </div>
                                    <div class="item-price">
                                        <?php
                                        $_product = wc_get_product( $row[2] );
                                        echo $_product->get_price_html();
                                        ?>
                                    </div>
                                    <a href="<?php echo get_bloginfo('url');?>/?add-to-cart=<?php echo $row[2];?>" data-quantity="1" data-product_id="<?php echo $row[2];?>" class="el-btn mod-grad button product_type_simple add_to_cart_button ajax_add_to_cart item-addbasket">
                                        КУПИТЬ
                                    </a>
                                </div>
                                <div class="goods-tile col-xs-12 col-sm-6 col-md-6">
                                    <div class="goods-tile-img">
                                    	<a href="<?php echo get_permalink($row[3]); ?>">
                                        	<img src="<?php echo get_the_post_thumbnail_url($row[3], 'full'); ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="doods-tile-title">
                                    	<a href="<?php echo get_permalink($row[3]); ?>">
                                        	<?php echo get_the_title($row[3]); ?>
                                        </a>
                                    </div>
                                    <div class="item-price">
                                        <?php
                                        $_product = wc_get_product( $row[3] );
                                        echo $_product->get_price_html();
                                        ?>
                                    </div>
                                    <a href="<?php echo get_bloginfo('url');?>/?add-to-cart=<?php echo $row[3];?>" data-quantity="1" data-product_id="<?php echo $row[3];?>" class="el-btn mod-grad button product_type_simple add_to_cart_button ajax_add_to_cart item-addbasket">
                                        КУПИТЬ
                                    </a>
                                </div>
                                <div class="goods-tile col-xs-12 col-sm-6 col-md-6">
                                    <div class="goods-tile-img">
                                    	<a href="<?php echo get_permalink($row[4]); ?>">
                                        	<img src="<?php echo get_the_post_thumbnail_url($row[4], 'full'); ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="doods-tile-title">
                                    	<a href="<?php echo get_permalink($row[4]); ?>">
                                        	<?php echo get_the_title($row[4]); ?>
                                        </a>
                                    </div>
                                    <div class="item-price">
                                        <?php
                                        $_product = wc_get_product( $row[4] );
                                        echo $_product->get_price_html();
                                        ?>
                                    </div>
                                    <a href="<?php echo get_bloginfo('url');?>/?add-to-cart=<?php echo $row[4];?>" data-quantity="1" data-product_id="<?php echo $row[4];?>" class="el-btn mod-grad button product_type_simple add_to_cart_button ajax_add_to_cart item-addbasket">
                                        КУПИТЬ
                                    </a>
                                </div>
                                <div class="goods-tile col-xs-12 col-sm-6 col-md-6">
                                    <div class="goods-tile-img">
                                    	<a href="<?php echo get_permalink($row[5]); ?>">
                                        	<img src="<?php echo get_the_post_thumbnail_url($row[5], 'full'); ?>" alt="">
                                       	</a>
                                    </div>
                                    <div class="doods-tile-title">
                                    	<a href="<?php echo get_permalink($row[5]); ?>">
                                        	<?php echo get_the_title($row[5]); ?>
                                        </a>
                                    </div>
                                    <div class="item-price">
                                        <?php
                                        $_product = wc_get_product( $row[5] );
                                        echo $_product->get_price_html();
                                        ?>
                                    </div>
                                    <a href="<?php echo get_bloginfo('url');?>/?add-to-cart=<?php echo $row[5];?>" data-quantity="1" data-product_id="<?php echo $row[5];?>" class="el-btn mod-grad button product_type_simple add_to_cart_button ajax_add_to_cart item-addbasket">
                                        КУПИТЬ
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- end slide -->
                        <?php endforeach; ?>
                        </div>
                        <a href="<?php echo $shop_page_url; ?>" class="goods-slider-more"><?php echo $bolshe_tovarow; ?><i class="icon-right-arrow"></i></a>
                        <!-- end slider -->

                    <?php endif; ?>

                    <?php //if( have_rows('repeater_tovar', $page_idd) ): ?>

                    <?php //while( have_rows('repeater_tovar', $page_idd) ): the_row(); ?>

                    <?php
//                        $mass = get_field('repeater_tovar', $page_idd);
//                        echo '<pre>', var_dump($mass[0][1]);
                    ?>
                    <?php// endwhile; ?>
                    <?php //endif; ?>


                </div>
            </section>

            <section class="features js-bg-contain" data-img="<?php echo $glavna_banersik; ?>">
                <div class="container">
                    <div class="features-labels row">
                    <div class="col-xs-12 col-md-3 col-sm-6">
                    <?php
                    if( have_rows('банер', $page_idd) ):
                        $i = 0;

                        while ( have_rows('банер', $page_idd) ) : the_row(); ?>
                            <?php $i++;
                            if( $i > 2 ):
                                 break;
                            endif; ?>


                                <div class="features-label">
                                    <div class="features-label-ico">
                                        <img src="<?php echo get_sub_field('картинка', 'full', $page_idd); ?>" alt="">
                                    </div>
                                    <div class="features-label-title"><?php echo get_sub_field('заголовок', $page_idd); ?></div>
                                    <div class="features-label-descr"><?php echo get_sub_field('описание', $page_idd); ?></div>
                                </div>


                        <?php endwhile;

                    else :

                    endif;
                    ?>
                    </div>
                    <div class="col-xs-12 col-md-3 col-md-offset-6 col-sm-6">
                    <?php

                    // check if the repeater field has rows of data
                    if( have_rows('банер', $page_idd) ):
                        $povtor = get_field('банер', $page_idd );
                        $first_row = $povtor[2];
                        $second_row = $povtor[3];
                        // loop through the rows of data
                        while ( have_rows('банер', $page_idd) ) : the_row();

                            $image = get_sub_field('картинка', $page_idd);
                            $link = get_sub_field('заголовок', $page_idd);
                            $content = get_sub_field('описание', $page_idd); ?>
                            <div class="features-label">
                                <div class="features-label-ico">
                                    <img src="<?php echo $first_row['картинка']; ?>" alt="">
                                </div>
                                <div class="features-label-title">
                                    <?php echo $first_row['заголовок']; ?>
                                </div>
                                <div class="features-label-descr">
                                    <?php echo $first_row['описание']; ?>
                                </div>
                            </div>
                            <div class="features-label">
                                <div class="features-label-ico">
                                    <img src="<?php echo $second_row['картинка']; ?>" alt="">
                                </div>
                                <div class="features-label-title">
                                    <?php echo $second_row['заголовок']; ?>
                                </div>
                                <div class="features-label-descr">
                                    <?php echo $second_row['описание']; ?>
                                </div>
                            </div>

                      <?php  endwhile;

                    else :

                        // no rows found

                    endif;

                    ?>
                    </div>
                </div>


                    <div class="text-center">
                        <a href="<?php echo $shop_page_url; ?>" class="el-btn mod-red mod-arr">
                            <?php echo $knopka; ?>
                        </a>
                    </div>

                </div>
            </section>
<img class="mobile_otzivov_about" src="<?php echo $baner_otzivivvv; ?>"/>
            <section class="about">
                <div class="container-fluid js-bg-cover" data-img="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/bgrad.jpg">

                    <div class="about-slider">
                        <div class="about-slide-half col-xs-12 col-md-6">
                            <div class="about-slide-rotator">
                                <?php
                                query_posts(array(
                                    'post_type' => 'comment',
                                    'order' => 'DESC'
                                ) );
                                ?>
                                <?php while (have_posts()) : the_post(); ?>
                                    <!-- slide -->
                                    <div class="about-slide ">
                                        <div class="about-slide-inner">
                                            <div class="about-slider-title">
                                                <?php echo $roditeli_otzuvu; ?><sup><?php echo $otzivi_tm; ?></sup>
                                            </div>
                                            <div class="about-slider-content">
                                                <?php the_content(); ?>
                                            </div>
                                            <div class="about-slider-user">
                                                <div class="about-slider-user-ava">
                                                    <img src="<?php the_post_thumbnail_url(get_the_ID(),'full'); ?>" alt="">
                                                </div>
                                                <div class="about-slider-user-name">
                                                    <?php the_title(); ?>
                                                </div>
                                            </div>
                                            <?php
                                            $inst = $wp_query->found_posts;
                                            if ($inst > 1) : ?>
                                                <div class="about-slider-btns">
                                                    <a class="about-slider-btn prev" href="#"></a>
                                                    <a class="about-slider-btn next" href="#"></a>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <!-- end slide -->
                                <?php endwhile;?>
                            </div>
                        </div>

                        <div class="about-slide-half col-xs-12 col-md-6 js-bg-cover" data-img="<?php echo $baner_otzivivvv; ?>">
                        </div>

                    </div>
                </div>
            </section>

            <section class="action-section">
                <div class="container">
                    <div class="row">
                        <div class="action-slider">
                            <?php
                            query_posts(array(
                                'post_type' => 'sale',
                                'order' => 'DESC'
                            ) );
                            ?>
                            <?php while (have_posts()) : the_post(); ?>
                            <!-- slide -->
                            <div class="action-slider-slide">
                                <div class="col-xs-12 col-sm-6 text-center vafix">
                                    <img src="<?php echo get_field( "kartinka_golovna"); ?>" alt="" class="">
                                </div>

                                <div class="col-xs-12 col-sm-6 text-center vafix">
                                    <div class="action-slider-top">
                                        <img src="<?php echo get_field( "картинка_на_акции", $page_idd); ?>" alt="">
                                    </div>
                                    <div class="action-slider-title">
                                        <?php the_title(); ?>
                                    </div>
                                    <div class="action-slider-content">
                                        <?php echo get_field( "discriptionnn"); ?>
                                    </div>
                                    <div class="action-slider-btn">
                                        <a href="<?php the_permalink(); ?>" class="el-btn mod-aqua">
                                            ПЕРЕЙТИ
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- end slide -->
                            <?php endwhile;?>
                        </div>
                    </div>
                </div>
            </section>

            <!-- <div class="el-banner banner-section mpage" >
                <a href="https://halykoo.ua/" target="_blank">
                    <img src="<?php echo $baner_uznst_bolshe; ?>" alt="">
                </a>
            </div> -->

            <div class="el-banner banner-section  mpage" >
                <!-- <a href="https://halykoo.ua/" target="_blank"> -->
                    <img src="<?php echo $baner_uznst_bolshe; ?>" alt="">
                    <a href="<?php echo $ssilka_uznst_bolshe; ?>" class="el-btn mod-red">
                      ПЕРЕЙТИ 
                    </a>
                <!-- </a> -->
            </div>


            <section class="xpander-section container">
<?php if( have_rows('гармошка', $page_idd) ): ?>

    <?php while( have_rows('гармошка', $page_idd) ): the_row();
    // vars
		$titles = get_sub_field('главная_надпись', $page_idd);
		$opus = get_sub_field('opisanieeeee', $page_idd);

		?>
        <!-- item -->
        <div class="xpander">
            <div class="xpander-title">
                <?php echo $titles; ?>
                <span class="ico-plus"></span>
            </div>
            <div class="xpander-content">
                <?php echo $opus; ?>
            </div>
        </div>
        <!-- end item -->
    <?php endwhile; ?>


<?php endif; ?>
            </section>

            <div class="text-center">
                <a href="<?php echo $shop_page_url; ?>" class="el-btn mod-grad mod-arr">
                    <?php echo $knopka; ?>
                </a>
            </div>
        </div>
<?php get_footer(); // подключаем footer.php ?>
