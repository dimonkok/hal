<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
?>
</div>
<footer id="footer" class="wrapper footer">
    <div class="container-fluid">
        <div class="col-sm-12 col-md-4 ">
            <nav class="footer-nav">
		<?php $args = array( // опции для вывода нижнего меню, чтобы они работали, меню должно быть создано в админке
			'theme_location' => 'bottom', // идентификатор меню, определен в register_nav_menus() в function.php
			'container'=> false, // обертка списка, false - это ничего
			'menu_class' => 'bottom-menu', // класс для ul
	  		'menu_id' => 'bottom-nav', // id для ul
	  	);
		wp_nav_menu($args); // выводим нижние меню
		?>
            </nav>
            <div class="footer-call">
                <a href="#" class="el-btn mod-call">
          <span>
            <i class="icon-phones"></i>
          </span>
          <span>
              <?php if ( ICL_LANGUAGE_CODE=='ua' ) : ?>
                  <span class="txt1">гаряча</span>
                  <span class="txt2">лінія</span>
              <?php else : ?>
                  <span class="txt1">горячая</span>
                  <span class="txt2">линия</span>
              <?php endif; ?>
          </span>
          <span class="number">
            <?php  $valueesss = get_field( "namber_phonee", 'option' );
                echo $valueesss;
            ?>
          </span>
                </a>
                <label>
                    <?php if ( ICL_LANGUAGE_CODE=='ua' ) {
                        $value = get_field( "page_phone_ua", 'option' );
                    } else {
                        $value = get_field( "page_phone", 'option' );
                    }
                    echo $value;
                    ?>
                </label>
            </div>
        </div>
        <div class="col-sm-12 col-md-4 text-center footer-logo">
            <a class="footer-logo-link" href="/">
                <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/logo.png" alt="">
            </a>
            <p class="footer-logo-copy">
                ©2017 halykoo.store
            </p>
        </div>
        <div class="col-sm-12 col-md-4 text-right">
            <div class="footer-soc">
                <p>
                    Мы в соцсетях:
                </p>
                <ul>
                    <li>
                        <a href="https://www.facebook.com/halykooukraine/">
                            <i class="icon-facebook-logo"></i>
                        </a>
                    </li>
                   <li>
                       <a href="https://www.instagram.com/halykooua/">
                           <!-- <i class="fa fa-instagram" aria-hidden="true"></i> -->
                           <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/instagram-logo.svg">
                       </a>
                   </li>
                </ul>
            </div>

            <a rel="nofollow" href="http://itguild.pro" class="footer-brand">
                <img src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/img/eveloped.png" alt="">
            </a>

        </div>
    </div>

    <!-- Begin Login modal -->
    <div id="loginform" style="display:none;">
              <div class="row">
                <div class="col-sm-10 col-sm-offset-1">

                    <ul class="title-list">
                        <li class="js-tab"><a class="js-login" href="#">Вход</a></li>
                        <li class="js-tab"><a class="js-regist" href="#">Регистрация</a></li>
                    </ul>

                    <div class="login-wrap active">
                        <h2 class="loginerror"></h2>
                        <form id="login_modal" name="form">
                            <input class="el-input-modal sm" id="username" type="text" placeholder="E-mail" name="username">
                            <input class="el-input-modal sm" id="password" type="password" placeholder="Пароль" name="password">
                            <button type="submit" class="el-btn-modal sm " >Войти <span class="spinner"><i></i></span></button>
                        </form>

                        <div class="link-wraper">
                            <a class="forgot" href="#" data-remodal-target="password_modal">Забыли пароль?</a>
                        </div>

                    </div>

                </div>
        </div>
    </div>


	</footer>
<?php if(is_cart()): ?>
    </div>
<?php endif; ?>
</div>
            <script src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/js/ie-detector.js"></script>
    <script src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/js/modernizr.js"></script>
    
<script src = "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.3/angular.min.js"></script>
<?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
<script type="application/javascript">
    jQuery(document).ready(function($) {
        // browser window scroll (in pixels) after which the "back to top" link is shown
        var offset = 500,
            //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
            offset_opacity = 500,
            //duration of the top scrolling animation (in ms)
            scroll_top_duration = 900,
         $callback = $('#callback');
        //hide or show the "back to top" link

        $callback.on('click', function () {
            if($(this).hasClass('open')) {
                $("#callback_panel").animate({right: '-220px'}, 1000);
                $("#callback").animate({right: '0px'}, 1000);
                $("#callback").removeClass('open');
            }else{
                $("#callback_panel").animate({right: '0px'}, 1000);
                $("#callback").animate({right: '220px'}, 1000);
                $("#callback").addClass('open');
            }
        });
    });
</script>
<script src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/js/production.min.js"></script>
<script src="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/js/my.js"></script>
<script type='text/javascript' src='<?php echo plugins_url(); ?>/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js'></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=f081c27b2750daff99852cdb2c3eacad" charset="UTF-8" async nomaps></script>
    <link rel="stylesheet"
          href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/css//jquery.modal.css"
          type="text/css" media="screen"/>
          <script src="https://use.fontawesome.com/2df73e3a92.js"></script>
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i" rel="stylesheet">
<script type="text/javascript">
    window._pt_lt = new Date().getTime();
    window._pt_sp_2 = [];
    _pt_sp_2.push('setAccount,66ae9ecf');
    var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    (function() {
        var atag = document.createElement('script'); atag.type = 'text/javascript'; atag.async = true;
        atag.src = _protocol + 'cjs.ptengine.com/pta_en.js';
        var stag = document.createElement('script'); stag.type = 'text/javascript'; stag.async = true;
        stag.src = _protocol + 'cjs.ptengine.com/pts.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(atag, s); s.parentNode.insertBefore(stag, s);
    })();
</script>
</body>
</html>