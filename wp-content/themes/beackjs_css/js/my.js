jQuery(document).ready(function () {
    jQuery('.tovar-tabs-tabs a:first-child').addClass('active');
    jQuery('.tovar-tabs-content .tovar-tabs-tabcont:first-child').addClass('active');
});


// PNotify
//--------------------------
function setNotification() {
    var $notif = jQuery(".js-notification ");
    if (isOnPage($notif)) {
        var typeString;
        if (jQuery(".js-notification ").hasClass("notify-error")) {
            typeString = "error";
        } else if ($notif.hasClass("notify-success")) {
            typeString = "success";
        } else {
            typeString = "info";
        }
        new PNotify({
            title: $notif.find(".notification-title").text(),
            text: $notif.find(".notification-content").text(),
            type: typeString,
            icon: false,
            insert_brs: true,
            buttons: {
                sticker: false
            }
        });
    }
}

jQuery(document).ready(function () {
    $(".account-tab-list li:first-child a").addClass("active");
    $(".account-tab-item .account-tab-link").on("click", function (e) {
        $(".account-tab-item .account-tab-link").removeClass("active");
        $(this).addClass("active");
    });


    var qtyChangeTimer;

// tabs
    if (jQuery(".title-list").length) {
        jQuery(".js-login").on("click", function (e) {
            e.preventDefault();
            var index = jQuery(this).index();
            jQuery(".js-login.active, .tovar-tabs-tabcont.active").removeClass('active');
            jQuery(this).addClass("active");
            jQuery('.tovar-tabs-tabcont').eq(index).addClass("active");
        });
    }
});

var $tabs = jQuery('.tabs > form'), _currhash, $currTab;

function showTab() {
    if ($currTab.length > 0) {
        $tabs.removeClass('current');
        $currTab.addClass('current');
    }
}

/* find the tabs and 'unlink' the id to prevent page jump */
$tabs.each(function () {
    var _id = jQuery(this).attr('id');
    jQuery(this).attr('id', _id + '_tab');
    /* eg we have given the tab an id of 'tab1_tab' */
});

/* set up an anchor 'watch' for the panels */
function anchorWatch() {
    if (document.location.hash.length > 0) {
        /* only run if 'hash' has changed */
        if (_currhash !== document.location.hash) {
            _currhash = document.location.hash;
            /* we only want to match the 'unlinked' id's */
            $currTab = jQuery(_currhash + '_tab');
            showTab();
        }
    }
}

setInterval(anchorWatch, 300);


jQuery(document).on("click", ".like.ajax", function (e) {
    e.preventDefault();
    var tovar_id = jQuery(this).data('id');
    if (login == "yes") {
        $.ajax({
            type: 'POST',
            url: ajaxurls,
            data: {
                'user_id': user_id,
                'tovar_id': tovar_id
            },
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-success');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + response.success + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                } else {
                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-error');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + response.error + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                }
            }
        })


    } else {
        var div = document.createElement('div');
        div.setAttribute('class', 'js-notification notify-error');
        div.setAttribute('style', 'display: none;');
        div.innerHTML = '<div class="notification-content">' + nitisess + '</div>';
        document.getElementById('notise').appendChild(div);
        setNotification();
        document.getElementById('notise').removeChild(div);
    }

});

jQuery(document).on("click", ".fav-close .js-killrow", function (e) {
    e.preventDefault();
    var tovar_id = jQuery(this).data('id');
    if (login == "yes") {
        $.ajax({
            type: 'POST',
            url: deleteRows,
            data: {
                'user_id': user_id,
                'tovar_id': tovar_id
            },
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-success');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + response.success + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                } else {
                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-error');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + response.error + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                }
            }
        })
    } else {
        var div = document.createElement('div');
        div.setAttribute('class', 'js-notification notify-error');
        div.setAttribute('style', 'display: none;');
        div.innerHTML = '<div class="notification-content">' + nitisess + '</div>';
        document.getElementById('notise').appendChild(div);
        setNotification();
        document.getElementById('notise').removeChild(div);
    }
});


jQuery(document).on("click", ".rate_row .rate_star", function (e) {
    e.preventDefault();
    var id = jQuery(this).data('value');
    if (login == "yes") {
        $.ajax({
            type: 'POST',
            url: ajaxurlrate,
            data: {
                'user_id': user_id,
                'tovar_id': tovar_ids,
                'star': id
            },
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-success');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + response.success + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                } else {
                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-error');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + response.error + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                }
            }
        })
    } else {
        var div = document.createElement('div');
        div.setAttribute('class', 'js-notification notify-error');
        div.setAttribute('style', 'display: none;');
        div.innerHTML = '<div class="notification-content">' + nitisess + '</div>';
        document.getElementById('notise').appendChild(div);
        setNotification();
        document.getElementById('notise').removeChild(div);
    }
});

jQuery(document).on("click", ".coupon-btn .mod-loadmore", function (e) {
    setNotification();
    setTimeout(setNotification, 2500);
});

jQuery(document).on("change", "input.input-text.qty", function (e) {
    var val_cart_button = $(this).val();
    $('#get_qty_to_cart').attr('data-quantity', val_cart_button);
    console.log(val_cart_button);
});

if ($('.liqpay_modal').length > 0) {
    $('.liqpay_modal').remodal().open();
}
jQuery(document).on("click", "#closeremodal", function (e) {
    e.preventDefault();
    $('.cart_add_popoap').remodal().close();
});

jQuery(document).on("click", ".add_to_cart_button", function (e) {
    e.preventDefault();

    $('.cart_add_popoap').remodal().open();
    // var div = document.createElement('div');
    // div.setAttribute('class', 'js-notification notify-success');
    // div.setAttribute('style', 'display: none;');
    // div.innerHTML = '<div class="notification-content">'+ messages_add_to_cart +'</div>';
    // document.getElementById('notise').appendChild(div);
    // setNotification();
    // document.getElementById('notise').removeChild(div);
});


jQuery(function ($) {

    $(document).on("click", ".product-remove .cart-table-delete", function (e) {
        var prod_delete_id = $(this).attr('href');
        window.location.href = prod_delete_id;
    });

    $(document).on("change", "#change_dost", function (e) {

        var instik = $(this).find(':selected').data('id');
        var name = $(this).find(':selected').html();
        $("#customer_details #novaya_potchta").slideUp();
        if (instik == 2) {
            var value3 = $("#change_oplata option[value=3]");
            var value2 = $("#change_oplata option[value=2]");
            var oplata3 = $(".woocommerce-billing-fields .oplata .jq-selectbox__dropdown [data-id=3]");
            var oplata2 = $(".woocommerce-billing-fields .oplata .jq-selectbox__dropdown [data-id=2]");
            $("#insert").html(name);
            value3.hide();
            oplata3.hide();
            value2.show();
            oplata2.show();
            value2.attr("selected", "selected");
            oplata2.attr("selected", "selected");
            $.ajax({
                type: 'POST',
                url: test,
                data: {
                    select: 'wcso_local_shippings'
                },
                dataType: 'json',
                success: function (data) {
                    $(".orderform-stats-cell.dostavkachena").html(data.chosen_method);
                    $("#replases span.woocommerce-Price-amount").html(data.finalprise);
                    $("#dost").attr("style", "display:table-row");
                    //console.log(data.chosen_method);
                }
            });
        }
        if (instik == 3) {
            $("#customer_details #novaya_potchta").slideDown();
            var value3 = $("#change_oplata option[value=3]");
            var value2 = $("#change_oplata option[value=2]");
            var oplata3 = $(".woocommerce-billing-fields .oplata .jq-selectbox__dropdown [data-id=3]");
            var oplata2 = $(".woocommerce-billing-fields .oplata .jq-selectbox__dropdown [data-id=2]");
            $("#insert").html(name);

            value3.show();
            oplata3.show();
            value2.hide();
            oplata2.hide();
            value3.attr("selected", "selected");
            oplata3.attr("selected", "selected");
            $.ajax({
                type: 'POST',
                url: test,
                data: {
                    select: 'wcso_local_shipping'
                },
                dataType: 'json',
                success: function (data) {
                    $(".orderform-stats-cell.dostavkachena").html(data.chosen_method);
                    $("#replases span.woocommerce-Price-amount").html(data.finalprise);
                    $("#dost").attr("style", "display:table-row");
                }
            });
        }
    });

    $(document).on("change", "#change_oplata", function (e) {
        var instiks = $(this).find(':selected').data('id');
        if (instiks == 2) {
            $("#payment_method_cop").prop("checked", true);
        }
        if (instiks == 3) {
            $("#payment_method_cod").prop("checked", true);
        }
        if (instiks == 4) {
            $("#payment_method_liqpay").prop("checked", true);
        }
    });


    $(document).on("click", ".coupon-btn .mod-loadmore", function (e) {
        function reload() {
            window.location.reload();
        }

        setTimeout(reload, 1000);
    });

    $(document).on("click", "#akjaxs .woocommerce-remove-coupon", function (e) {
        function reload() {
            window.location.reload();
        }

        setTimeout(reload, 1000);

    });

    $(document).on('change', '.cart_item input.qty', function () {

        var item_hash = $(this).attr('name').replace(/cart\[([\w]+)\]\[qty\]/g, "$1");
        var item_quantity = $(this).val();
        var $imput = $(this);
        var currentVal = parseFloat(item_quantity);
        var prod_id = $(this).data('id');


        function qty_cart() {

            $.ajax({
                type: 'POST',
                url: cart_qty_ajaxs,
                data: {
                    hash: item_hash,
                    quantity: currentVal,
                    product_id: prod_id
                },
                success: function (data) {

                    jQuery('.woocommerce-cart-form').block({
                        message: null,
                        overlayCSS: {
                            background: '#fff',
                            opacity: 0.6
                        }
                    });

                    var data = {
                        security: wc_checkout_params.update_order_review_nonce,
                        post_data: $('form.checkout').serialize()
                    };
                    $.ajax({
                        type: 'POST',
                        url: wc_checkout_params.wc_ajax_url.toString().replace('%%endpoint%%', 'update_order_review'),
                        data: data,
                        success: function (data) {
                            // Always update the fragments
                            if (data.fragments) {
                                var obj = data.fragments;
                                if (obj) {
                                    var text = obj[Object.keys(obj)[0]];
                                }
                                // console.log(text);
                                $("#akjaxs").html(text);
                                $('input').styler();
                            }
                        }
                    });
                    $(".shipping-calculator-form .button").click();
                    $('input').styler();
                }
            });
            var div = document.createElement('div');
            div.setAttribute('class', 'js-notification notify-success');
            div.setAttribute('style', 'display: none;');
            div.innerHTML = '<div class="notification-content">Корзина обновлена</div>';
            document.getElementById('notise').appendChild(div);
            setNotification();
            document.getElementById('notise').removeChild(div);

        }

        if (typeof qtyChangeTimer != 'undefined')
            clearTimeout(qtyChangeTimer);
        qtyChangeTimer = setTimeout(qty_cart, 1000);

        function stylerInput() {
            $('input').styler();
        }

        setTimeout(stylerInput, 5000);
    });


    $(document).on("click", ".sendfeed-rate .rate_star", function (e) {
        e.preventDefault();
        var starss = $(this).data('value');
        var dataStar = $(this).closest('#sendfeeds-form').find('#fins_but').attr('data-star', starss);
        //$("#fins_but")

    });

    $(document).on("click", "#sendfeeds-form .sendfeed-btn .mod-grad", function (e) {
        e.preventDefault();
        var prod_id = $(this).data('id');
        if ($(this).attr('data-star')) {
            var star = $(this).data('star');
        } else {
            var star = 0;
        }
        var textss = $(this).closest('#sendfeeds-form').find('#text_comments').val();

        $.ajax({
            type: 'POST',
            url: comment_ajaxs,
            data: {
                product_id: prod_id,
                star: star,
                text: textss
            },
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-success');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + response.success + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                } else {
                    var obj = response.error.errors;
                    if (obj) {
                        var text = obj[Object.keys(obj)[0]];
                    } else {
                        var text = response.error;
                    }
                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-error');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + text + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                }
            }
        });
    });

    $(document).on("click", ".newfeed-content .newfeed-delete", function (e) {
        e.preventDefault();
        var comment_id = $(this).data('id');
        var thisParent = $(this).parent().parent();
        $.ajax({
            type: 'POST',
            url: comment_delete_ajaxs,
            data: {
                comment_id: comment_id,
            },
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    //console.log(response.success);
                    // $('.sendfeed').remodal().close();
                    if (thisParent.data('comment') == response.success) {
                        var div = document.createElement('div');
                        div.setAttribute('class', 'js-notification notify-success');
                        div.setAttribute('style', 'display: none;');
                        div.innerHTML = '<div class="notification-content">Комментарий удален</div>';
                        document.getElementById('notise').appendChild(div);
                        setNotification();
                        document.getElementById('notise').removeChild(div);
                        thisParent.fadeOut().remove();
                    }
                }
            }
        });
    });

    $(document).on("click", ".userdata-inner .userdata-inner-ava", function (e) {
        $('.avatar_change').remodal().open();
    });

    $("select[name='hero']").on("change", function () {
        $("select[name='payment']").val(1).trigger("refresh");
    });

    $(document).on("change", "select[data-foo='filtersel']", function () {
        function stylers() {
            $('select').styler();
        }

        setTimeout(stylers, 2500);

    });
    $(document).on("click", "#formeditsubmit", function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/wp-content/themes/haliky/edit-account.php',
            data: $('.form-edit-account').serialize(),
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-success');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + response.success + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                    location.reload();
                } else {
                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-error');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + response.error + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                }
            }
        });
    });
});


// The toggle

jQuery(document).ready(function () {

    jQuery('#grid').click(function () {
        jQuery(this).addClass('active');
        jQuery('#list').removeClass('active');
        jQuery.cookie('gridcookie', 'grid', {path: '/'});
        jQuery('.post-type-archive-product section.itemlist').fadeOut(300, function () {
            jQuery(this).addClass('grid').removeClass('list').fadeIn(300);
        });
        return false;
    });

    jQuery('#list').click(function () {
        jQuery(this).addClass('active');
        jQuery('#grid').removeClass('active');
        jQuery.cookie('gridcookie', 'list', {path: '/'});
        jQuery('.post-type-archive-product section.itemlist').fadeOut(300, function () {
            jQuery(this).removeClass('grid').addClass('list').fadeIn(300);
        });
        return false;
    });

    if (jQuery.cookie('gridcookie')) {
        jQuery('.post-type-archive-product section.itemlist, #gridlist-toggle').addClass(jQuery.cookie('gridcookie'));
    }

    if (jQuery.cookie('gridcookie') == 'grid') {
        jQuery('.gridlist-toggle #grid').addClass('active');
        jQuery('.gridlist-toggle #list').removeClass('active');
    }

    if (jQuery.cookie('gridcookie') == 'list') {
        jQuery('.gridlist-toggle #list').addClass('active');
        jQuery('.gridlist-toggle #grid').removeClass('active');
    }

    jQuery('#gridlist-toggle a').click(function (event) {
        event.preventDefault();
    });
    $(document).on('click', '#agree-styler', function (e) {
        e.preventDefault();
        if ($(this).hasClass('checked')) {
            $(this).closest(".orderform-agree").find("#valid_check").val('1');
        } else {
            $(this).closest(".orderform-agree").find("#valid_check").val('0');
        }
        // $(this).closest('#agree').prop( "checked" );
        // console.log('asdf');

    });

    $(document).on('click', '#submit_peyment_form', function (e) {
        var data = $('#orderform').serialize();
        $("#change_dost-styler").removeClass("error");
        $("#change_oplata-styler").removeClass("error");
        $.ajax({
            type: 'POST',
            url: validate_form_cart,
            data: data,
            dataType: 'json',
            success: function (response) {
                if (response.success) {

                } else {
                    var ists = $("#change_dost").val();
                    if (ists == '1') {
                        $("#change_dost").closest("#change_dost-styler").addClass("error");
                    }
                    var ist = $("#change_oplata").val();
                    if (ist == '1') {
                        $("#change_oplata").closest("#change_oplata-styler").addClass("error");
                    }

                    var div = document.createElement('div');
                    div.setAttribute('class', 'js-notification notify-error');
                    div.setAttribute('style', 'display: none;');
                    div.innerHTML = '<div class="notification-content">' + response.error + '</div>';
                    document.getElementById('notise').appendChild(div);
                    setNotification();
                    document.getElementById('notise').removeChild(div);
                }
            }
        });

    });
});